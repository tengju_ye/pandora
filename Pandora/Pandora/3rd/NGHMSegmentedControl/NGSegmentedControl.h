
//
//  Original created by :
//  Created by Hesham Abd-Elmegid on 23/12/12.
//  Copyright (c) 2012 Hesham Abd-Elmegid. All rights reserved.
//


//  Modify by ng from HMSegmentedControl 2014-7 for background image and dash style


#import <UIKit/UIKit.h>

enum NGSelectionIndicatorMode {
    NGSelectionIndicatorResizesToStringWidth = 0, // Indicator width will only be as big as the text width
    NGSelectionIndicatorFillsSegment = 1 // Indicator width will fill the whole segment
};

enum NGSelectionIndicatorFillMode {
    NGSelectionIndicatorFillModeAll = 0, // Indicator fill the whole text
    NGSelectionIndicatorFillModeDash = 1 // Indicator under the text
};

@interface NGSegmentedControl : UIControl

@property (nonatomic, strong) NSArray *sectionTitles;
@property (nonatomic, copy) void (^indexChangeBlock)(NSUInteger index); // you can also use addTarget:action:forControlEvents:

@property (nonatomic, strong) UIFont *font; // default is [UIFont fontWithName:@"Avenir-Light" size:19.0f]
@property (nonatomic, strong) UIColor *textColor; // default is [UIColor blackColor]
@property (nonatomic, strong) UIColor *selectedTextColor; // default is [UIColor whiteColor]

@property (nonatomic, strong) UIImage* backGoundImage;//default is top_background@2x
@property (nonatomic, strong) UIImage *selectionIndicatorImage; // red_line_and_shadow@2x.png

@property (nonatomic, assign) enum NGSelectionIndicatorMode selectionIndicatorMode; // Default is HMSelectionIndicatorResizesToStringWidth
@property (nonatomic, assign) enum NGSelectionIndicatorFillMode selectionIndicatorFillMode; // Default is HMSelectionIndicatorResizesToStringWidth


@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, readwrite) CGFloat height; // default is 32.0
@property (nonatomic, readwrite) UIEdgeInsets segmentEdgeInset; // default is UIEdgeInsetsMake(0, 5, 0, 5)
@property (nonatomic,readwrite) CGFloat leftRightPadding;//add by ng
- (CGRect)frameForSelectionIndicator;
- (id)initWithSectionTitles:(NSArray *)sectiontitles;
- (void)setSelectedIndex:(NSUInteger)index animated:(BOOL)animated;

@end
