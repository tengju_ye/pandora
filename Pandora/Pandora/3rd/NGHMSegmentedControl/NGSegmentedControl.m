#import "NGSegmentedControl.h"
#import <QuartzCore/QuartzCore.h>

@interface NGSegmentedControl ()

@property (nonatomic, strong) CALayer *selectedSegmentLayer;
@property (nonatomic, readwrite) CGFloat segmentWidth;
@property (nonatomic, readwrite) CGFloat selectionIndicatorHeight; // default is 5.0
@property (nonatomic, strong) NSMutableArray* textLayers;
@end

@implementation NGSegmentedControl

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];

    if (self) {
        [self setDefaults];
    }
    
    return self;
}

- (id)initWithSectionTitles:(NSArray *)sectiontitles {
    self = [super initWithFrame:CGRectZero];
    
    if (self) {
        self.sectionTitles = sectiontitles;
        [self setDefaults];
    }
    
    return self;
}

- (void)setDefaults {
    self.font = [UIFont fontWithName:@"STHeitiSC-Light" size:14.0f];
    self.textColor = [UIColor blackColor];
    self.selectedTextColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor whiteColor];
    self.selectedIndex = 0;
    self.segmentEdgeInset = UIEdgeInsetsMake(0, 5, 0, 5);
    self.height = 32.0f;
    self.selectionIndicatorMode = NGSelectionIndicatorResizesToStringWidth;
    self.selectionIndicatorFillMode = NGSelectionIndicatorFillModeAll;
    self.selectedSegmentLayer = [CALayer layer];
    [self.selectedSegmentLayer setCornerRadius:5];
    if (self.selectedTextColor) {
        [self.selectedSegmentLayer setBackgroundColor:self.selectedTextColor.CGColor];
    }else{
        [self.selectedSegmentLayer setBackgroundColor:[UIColor colorWithRed:0.987 green:0.136 blue:0.392 alpha:1.000].CGColor];
    }
    
}

#pragma mark - Drawing

- (void)drawRect:(CGRect)rect
{
    if (self.backGoundImage) {
        [self.backGoundImage drawInRect:self.bounds];
    }else{
        [self.backgroundColor set];
        UIRectFill([self bounds]);
    }
    self.selectedSegmentLayer.frame = [self frameForSelectionIndicator];
    [self.layer addSublayer:self.selectedSegmentLayer];
    
    for (CALayer* layer in self.textLayers) {
        [layer removeFromSuperlayer];
    }
    
    [self.sectionTitles enumerateObjectsUsingBlock:^(id titleString, NSUInteger idx, BOOL *stop) {
        CGFloat stringHeight = [titleString sizeWithFont:self.font].height;
        CGFloat y = ((self.height) / 2) + (- stringHeight / 2);
        CGRect rect = CGRectMake(self.segmentWidth * idx+self.leftRightPadding, y, self.segmentWidth, stringHeight);
        CATextLayer* tl = [CATextLayer layer];
        [self.textLayers addObject:tl];
        
        tl.frame = rect;
        tl.font = (__bridge CFTypeRef)(self.font);
        tl.fontSize = [self.font pointSize];
        tl.alignmentMode = kCAAlignmentCenter;
        tl.wrapped = NO;
        tl.truncationMode = kCATruncationEnd;
        tl.string = titleString;
        if (idx==self.selectedIndex) {
            tl.foregroundColor = self.selectedTextColor.CGColor;
        }else{
            tl.foregroundColor = self.textColor.CGColor;
        }
        tl.contentsScale = [[UIScreen mainScreen] scale] ;
        [self.layer addSublayer:tl];
    }];
}



- (CGRect)frameForSelectionIndicator {
    CGFloat stringWidth = [[self.sectionTitles objectAtIndex:self.selectedIndex] sizeWithFont:self.font].width;
    static CGFloat dashHeight = 2;
    if (self.selectionIndicatorMode == NGSelectionIndicatorResizesToStringWidth) {
        CGFloat widthTillEndOfSelectedIndex = (self.segmentWidth * self.selectedIndex)+self.leftRightPadding + self.segmentWidth;
        CGFloat widthTillBeforeSelectedIndex = (self.segmentWidth * self.selectedIndex)+self.leftRightPadding;
        
        CGFloat x = ((widthTillEndOfSelectedIndex - widthTillBeforeSelectedIndex) / 2) + (widthTillBeforeSelectedIndex - stringWidth / 2);
        CGPoint cent = self.center;
        if (self.selectionIndicatorFillMode==NGSelectionIndicatorFillModeDash) {
            return CGRectMake(x+self.leftRightPadding, CGRectGetHeight(self.bounds)-dashHeight, stringWidth, dashHeight);
        }
        return CGRectMake(x+self.leftRightPadding, cent.y-self.selectionIndicatorHeight/2, stringWidth, self.selectionIndicatorHeight);
    } else {
        CGPoint cent = self.center;
        if (self.selectionIndicatorFillMode==NGSelectionIndicatorFillModeDash) {
            return CGRectMake(self.segmentWidth * self.selectedIndex+self.leftRightPadding, CGRectGetHeight(self.bounds)-dashHeight, self.segmentWidth, dashHeight);
        }
        return CGRectMake(self.segmentWidth * self.selectedIndex+self.leftRightPadding, cent.y-self.selectionIndicatorHeight/2, self.segmentWidth, self.selectionIndicatorHeight);
    }
}

- (void)updateSegmentsRects {
    // If there's no frame set, calculate the width of the control based on the number of segments and their size
    if (CGRectIsEmpty(self.frame)) {
        self.segmentWidth = 0;
        
        for (NSString *titleString in self.sectionTitles) {
            CGFloat stringWidth = [titleString sizeWithFont:self.font].width + self.segmentEdgeInset.left + self.segmentEdgeInset.right;
            self.segmentWidth = MAX(stringWidth, self.segmentWidth);
        }
        
        self.bounds = CGRectMake(0, 0, self.segmentWidth * self.sectionTitles.count+2*self.leftRightPadding, self.height);
    } else {
        self.segmentWidth = (self.frame.size.width-2*self.leftRightPadding) / self.sectionTitles.count;
        self.height = self.frame.size.height;
    }
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    // Control is being removed
    if (newSuperview == nil)
        return;
    
    [self updateSegmentsRects];
}

#pragma mark - Touch

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    CGRect textFrame = self.bounds;
    textFrame.origin.x +=self.leftRightPadding;
    textFrame.size.width-=(2*self.leftRightPadding);
    if (CGRectContainsPoint(textFrame, touchLocation)) {
        NSInteger segment = (touchLocation.x - self.leftRightPadding) / self.segmentWidth;
        if (segment>(self.sectionTitles.count-1)) {
            segment = (self.sectionTitles.count-1);
        }else if(segment<0){
            segment = 0;
        }
        
        if (segment != self.selectedIndex) {
            [self setSelectedIndex:segment animated:YES];
        }
    }
}

#pragma mark -

- (void)setSelectedIndex:(NSInteger)index {
    [self setSelectedIndex:index animated:NO];
}


- (void)setSelectedIndex:(NSUInteger)index animated:(BOOL)animated {
    BOOL tf = self.textLayers.count>index && self.textLayers.count>_selectedIndex;
    if (tf) {
        CATextLayer* tl = [self.textLayers objectAtIndex:_selectedIndex];
        tl.foregroundColor = self.textColor.CGColor;
        tl = [self.textLayers objectAtIndex:index];
        tl.foregroundColor = self.selectedTextColor.CGColor;
    }
    _selectedIndex = index;
    if (animated) {
        // Restore CALayer animations
        self.selectedSegmentLayer.actions = nil;
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:0.15f];
        [CATransaction setCompletionBlock:^{
            if (self.superview)
                [self sendActionsForControlEvents:UIControlEventValueChanged];
            
            if (self.indexChangeBlock)
                self.indexChangeBlock(index);
        }];
        self.selectedSegmentLayer.frame = [self frameForSelectionIndicator];
        [CATransaction commit];
    } else {
        // Disable CALayer animations
        NSMutableDictionary *newActions = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNull null], @"position", [NSNull null], @"bounds", nil];
        self.selectedSegmentLayer.actions = newActions;
        
        self.selectedSegmentLayer.frame = [self frameForSelectionIndicator];
        
        if (self.superview)
            [self sendActionsForControlEvents:UIControlEventValueChanged];
        
        if (self.indexChangeBlock)
            self.indexChangeBlock(index);

    }
    [self setNeedsDisplay];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    if (self.sectionTitles)
        [self updateSegmentsRects];
    
    [self setNeedsDisplay];
}

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    
    if (self.sectionTitles)
        [self updateSegmentsRects];
    
    [self setNeedsDisplay];
}

-(void)setBackGoundImage:(UIImage *)backGoundImage
{
    _backGoundImage = backGoundImage;
    [self setNeedsDisplay];
}
-(void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:backgroundColor];
    [self setNeedsDisplay];
}

-(void)setSelectionIndicatorImage:(UIImage *)selectionIndicatorImage
{
    _selectionIndicatorImage = selectionIndicatorImage;
    [self setNeedsDisplay];
}

-(void)setSelectionIndicatorFillMode:(enum NGSelectionIndicatorFillMode)selectionIndicatorFillMode
{
    _selectionIndicatorFillMode = selectionIndicatorFillMode;
    if (self.selectionIndicatorFillMode==NGSelectionIndicatorFillModeAll) {
        [self.selectedSegmentLayer setCornerRadius:5];
    }else{
        [self.selectedSegmentLayer setCornerRadius:0];
    }
    [self setNeedsDisplay];
}


-(CGFloat)selectionIndicatorHeight
{
    return self.frame.size.height/2;
}

-(NSMutableArray *)textLayers
{
    if (!_textLayers) {
        _textLayers = [NSMutableArray array];
    }
    return _textLayers;
}

@end
