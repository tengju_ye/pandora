//
//  XLCycleScrollView.h
//  CycleScrollViewDemo
//
//  Created by xie liang on 9/14/12.
//  Copyright (c) 2012 xie liang. All rights reserved.



//  modified by ng

#import <UIKit/UIKit.h>

@protocol XLCycleScrollViewDelegate;
@protocol XLCycleScrollViewDatasource;

@interface XLCycleNGScrollView : UIView<UIScrollViewDelegate>
{
    UIScrollView *_scrollView;

    
    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curViews;
}

@property (nonatomic,readonly) UIScrollView *scrollView;
@property (nonatomic,assign) NSInteger currentPage;
@property (nonatomic,assign,setter = setDataource:) IBOutlet id<XLCycleScrollViewDatasource> datasource;
@property (nonatomic,assign,setter = setDelegate:) IBOutlet id<XLCycleScrollViewDelegate> delegate;
@property (nonatomic,readonly) UILabel* titleLabel;

- (void)reloadData;
- (void)setViewContent:(UIView *)view atIndex:(NSInteger)index;

@end

@protocol XLCycleScrollViewDelegate <NSObject>

@optional
- (void)didClickPage:(XLCycleNGScrollView *)csView atIndex:(NSInteger)index;
- (void)didChangePageTo:(NSUInteger)page view:(XLCycleNGScrollView *)csView;
@end

@protocol XLCycleScrollViewDatasource <NSObject>

@required
- (NSInteger)numberOfPages;
- (UIView *)pageAtIndex:(NSInteger)index;
@optional
- (NSString*)titleAtIndex:(NSInteger)index;
@end
