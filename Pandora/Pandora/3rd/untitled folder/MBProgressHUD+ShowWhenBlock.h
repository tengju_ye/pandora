//
//  MBProgressHUD+ShowWhenBlock.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/5.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
@interface MBProgressHUD(ShowWhenBlock)
+(MBProgressHUD*)showAddViewWhen:(UIView*)view when:(dispatch_block_t)block text:(NSString*)text;
+(MBProgressHUD *)showAddViewWhen:(UIView *)view when:(dispatch_block_t)block complete:(MBProgressHUDCompletionBlock)compelete text:(NSString*)text;

@end
