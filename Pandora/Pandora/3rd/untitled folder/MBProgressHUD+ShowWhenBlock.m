//
//  MBProgressHUD+ShowWhenBlock.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/5.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "MBProgressHUD+ShowWhenBlock.h"
@implementation MBProgressHUD(ShowWhenBlock)
+(MBProgressHUD *)showAddViewWhen:(UIView *)view when:(dispatch_block_t)block text:(NSString*)text{
    MBProgressHUD* hud = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:hud];
    [hud setLabelText:text];
    [hud setRemoveFromSuperViewOnHide:YES];
    [hud showAnimated:YES whileExecutingBlock:block];
    return hud;
}

+(MBProgressHUD *)showAddViewWhen:(UIView *)view when:(dispatch_block_t)block complete:(MBProgressHUDCompletionBlock)compelete text:(NSString *)text{
    MBProgressHUD* hud = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:hud];
    [hud setLabelText:text];
    [hud setRemoveFromSuperViewOnHide:YES];
    [hud showAnimated:YES whileExecutingBlock:block completionBlock:compelete];
    return hud;

}

@end
