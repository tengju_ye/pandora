//
//  APP.h
//  Pandora
//
//  Created by 小南宫的mac on 14/11/24.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>

//const
#define STORYBOARD_ID_MAIN4_NVC @"Main4NavigationController" //系统主导航页面
#define STORYBOARD_ID_LOGIN_NVC @"LoginViewNavigationController"
#define STORYBOARD_ID_REGISTER_VC @"RegisterViewController"
#define Alipay_ID @"3"

@interface APP : NSObject
@property(nonatomic,strong)NSString* SESSIONID;
@property(nonatomic,strong)dispatch_queue_t initialQueue;
@property(nonatomic,strong)NSDateFormatter* defaultFormat;
@property(nonatomic,strong,readonly) NSString* username;
@property(nonatomic,strong,readonly) NSString* password;

+(APP*)instance;
+(void)runBlockInInitialQ:(dispatch_block_t)block;
+(void)runBlockInMainQ:(dispatch_block_t)block;
+(NSString*)dateStringWithDefaultFormatForStamp:(NSTimeInterval)interval;
@end
