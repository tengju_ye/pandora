//
//  APP.m
//  Pandora
//
//  Created by 小南宫的mac on 14/11/24.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "APP.h"

@interface APP()
@end

@implementation APP

static APP* singleTon = nil;
static dispatch_once_t pred = 0;
+(APP *)instance{
    dispatch_once( &pred, ^{
        singleTon = [[self alloc] init];
    });
    return singleTon;
}

-(dispatch_queue_t)initialQueue{
    if (!_initialQueue) {
        _initialQueue = dispatch_queue_create("init queue", 0);
    }
    return _initialQueue;
}

-(NSString *)username{
    return [userDefaults objectForKey:UD_KEY_ACCOUNT];
}

-(NSString *)password{
    return [userDefaults objectForKey:UD_KEY_PSW];
}

+(void)runBlockInInitialQ:(dispatch_block_t)block{
    dispatch_async([[APP instance] initialQueue], block);
}
+(void)runBlockInMainQ:(dispatch_block_t)block{
    dispatch_async(MAIN_QUEUE, block);
}
+(NSString *)dateStringWithDefaultFormatForStamp:(NSTimeInterval)interval{
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:interval];
    return [[[APP instance] defaultFormat] stringFromDate:date];
}

-(NSDateFormatter *)defaultFormat{
    if (!_defaultFormat) {
        _defaultFormat = [[NSDateFormatter alloc] init];
        [_defaultFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    return _defaultFormat;
}

@end
