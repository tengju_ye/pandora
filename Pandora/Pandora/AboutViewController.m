//
//  AboutViewController.m
//  Pandora
//
//  Created by junchen on 12/3/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "AboutViewController.h"
#import "HelpServices.h"
#import "tools.h"
@interface AboutViewController () <UIWebViewDelegate>

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"关于";
    [tools addRightBarItem:self];
    GetAboutInfoResponse *response = [[HelpServices instance] getAboutInfo];
    NSLog(@"%@", response.help_contents);
    NSString* imgStyle = [NSString stringWithFormat:@"<style>img{max-width:%fpx}</style>",CGRectGetWidth(self.view.frame) - 10];
    NSString* html = [NSString stringWithFormat:@"%@\n%@",imgStyle,response.help_contents];
    [self.webView loadHTMLString:html baseURL:[NSURL URLWithString:@"http://smartplant.pandorabox.mobi"]];
    self.webView.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if(navigationType == UIWebViewNavigationTypeLinkClicked) {
        return NO;
    }
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
