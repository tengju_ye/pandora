//
//  AccountListTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/5.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Account.h"
#import "BaseTableViewCell.h"
@interface AccountListTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *beforeMoney;
@property (weak, nonatomic) IBOutlet UILabel *laterMoney;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;
@property (weak, nonatomic) IBOutlet UILabel *inMoneyLabel;
@property (nonatomic,strong) Account* account;
@end
