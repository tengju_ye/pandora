//
//  AccountListTableViewCell.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/5.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "AccountListTableViewCell.h"

@implementation AccountListTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setAccount:(Account *)account{
    _account = account;
    [self.timeLabel setText:[NSString stringWithFormat:@"账户发生时间:%@",_account.addtime]];
    [self.beforeMoney setText:[NSString stringWithFormat:@"操作前余额:%@",_account.amount_before_pay]];
    [self.laterMoney setText:[NSString stringWithFormat:@"操作后余额:%@",_account.amount_after_pay]];
    [self.inMoneyLabel setText:[NSString stringWithFormat:@"入账金额数:%@",_account.amount_in]];
    [self.remarkLabel setText:[NSString stringWithFormat:@"备注:%@",_account.remark]];
}

@end
