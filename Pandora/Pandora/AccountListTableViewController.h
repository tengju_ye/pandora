//
//  AccountListTableViewController.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/5.
//  Copyright (c) 2014年 NG. All rights reserved.
//
//充值/财务

#import <UIKit/UIKit.h>

@interface AccountListTableViewController : UITableViewController
@property(nonatomic,assign)BOOL onlyRecharge;//是否只展示充值的信息
@end
