//
//  AccountListTableViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/5.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "AccountListTableViewController.h"
#import "AccountServices.h"
#import "AccountListTableViewCell.h"
static NSString* const Identifier = @"identifier";
@interface AccountListTableViewController ()
@property(nonatomic,strong) NSArray* accountArray;
@property (nonatomic,assign) NSUInteger nextFirstRow;

@end

@implementation AccountListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView addHeaderWithTarget:self action:@selector(webGetAccountListRequest)];
    [self.tableView registerNib:[AccountListTableViewCell nib] forCellReuseIdentifier:Identifier];
    [self.tableView headerBeginRefreshing];
    
    
    __weak AccountListTableViewController* ws = self;

    [self.tableView addFooterWithCallback:^{
        [ws webrequestOfNextPage];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.accountArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AccountListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
    [cell setAccount:self.accountArray[indexPath.row]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 178;
}

#pragma mark - WebRequest
-(void)webGetAccountListRequest{
    [APP runBlockInInitialQ:^{
        self.nextFirstRow = 0;
        GetAccountListResponse* response =  [[AccountServices instance] getAccountList:self.onlyRecharge?@"1":@"0" firstRow:0];
        self.accountArray = response.account_list;
        self.nextFirstRow = response.nextFirstRow;
        [APP runBlockInMainQ:^{
            [self.tableView headerEndRefreshing];
            [self.tableView reloadData];
        }];
    }];
}

-(void)webrequestOfNextPage{
    [APP runBlockInInitialQ:^{
        GetAccountListResponse* response =  [[AccountServices instance] getAccountList:self.onlyRecharge?@"1":@"0" firstRow:self.nextFirstRow];
        self.accountArray = response.account_list;
        self.nextFirstRow = response.nextFirstRow;
        [APP runBlockInMainQ:^{
            [self.tableView headerEndRefreshing];
            [self.tableView reloadData];
        }];
    }];
}

@end
