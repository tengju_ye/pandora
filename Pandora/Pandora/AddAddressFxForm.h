//
//  AddAddressForm.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FXForms.h"
@interface AddAddressFxForm : NSObject<FXForm>
@property(nonatomic,strong)NSString* consignName;//收货人名字
@property(nonatomic,strong)NSString* mobile;//电话
@property(nonatomic,strong)NSString* province;//省份
@property(nonatomic,strong)NSString* city;//城市
@property(nonatomic,strong)NSString* area;//区县
@property(nonatomic,strong)NSString* detailAddress;//详细地址

@property(nonatomic,strong)NSArray* provinceArray;
@property(nonatomic,strong)NSArray* cityArray;
@property(nonatomic,strong)NSArray* areaArray;
@end
