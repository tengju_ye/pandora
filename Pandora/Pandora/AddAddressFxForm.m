//
//  AddAddressForm.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "AddAddressFxForm.h"
@implementation AddAddressFxForm
-(NSArray *)fields{
    NSMutableArray* fields = [NSMutableArray array];
    [fields addObject:
     @{FXFormFieldKey: @"consignName", FXFormFieldType: FXFormFieldTypeText,FXFormFieldTitle: @"收货人"}
     ];
    
    [fields addObject:
     @{FXFormFieldKey:@"mobile",FXFormFieldTitle:@"手机号码",FXFormFieldType:FXFormFieldTypePhone,FXFormFieldPlaceholder:@"手机号码"}
     ];
    
    if (self.provinceArray.count) {
        [fields addObject:
         @{FXFormFieldKey:@"province",FXFormFieldTitle:@"省份",FXFormFieldOptions:self.provinceArray, FXFormFieldPlaceholder: @"--选择省份--", FXFormFieldAction: @"updateProvince:"}
         ];
    }
    
    if (self.cityArray.count) {
        [fields addObject:
         @{FXFormFieldKey:@"city",FXFormFieldTitle:@"城市",FXFormFieldOptions:self.cityArray, FXFormFieldPlaceholder: @"--选择城市--", FXFormFieldAction: @"updateCity:"}
         ];
    }
    
    if (self.areaArray.count) {
        [fields addObject:
         @{FXFormFieldKey:@"area",FXFormFieldTitle:@"区县",FXFormFieldOptions:self.areaArray, FXFormFieldPlaceholder: @"--选择区县--"}//,FXFormFieldType:FXFormFieldTypeOption
         ];
    }
    


    [fields addObject:
     @{FXFormFieldKey:@"detailAddress",FXFormFieldType:FXFormFieldTypeLongText,FXFormFieldTitle: @"详细地址"}
     ];
    
    [fields addObject:
     @{FXFormFieldTitle: @"提交", FXFormFieldAction: @"submit:"}
     ];
    return fields;
}

@end
