//
//  AddAddressViewController.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Address.h"
@interface AddAddressViewController : UIViewController
@property(nonatomic,strong) Address* address;
@property(nonatomic,assign) BOOL editMode;
@end
