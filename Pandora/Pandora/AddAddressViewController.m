//
//  AddAddressViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "AddAddressViewController.h"
#import "FXForms.h"
#import "AddAddressFxForm.h"
#import "tools.h"
#import "AddressServices.h"
#import "AddAddressForm.h"
#import "EditAddressForm.h"
#import "AddAddressResponse.h"
#import "EditAddressForm.h"
#import "GetProvinceListResponse.h"
#import "Area.h"

@interface AddAddressViewController ()<FXFormControllerDelegate>
@property (nonatomic, strong) FXFormController *formController;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSMutableDictionary* provinces;//name - > province
@property (nonatomic,strong) NSMutableDictionary* citys;//name - >city
@property (nonatomic,strong) NSMutableDictionary* districts;//name ->District
@end

@implementation AddAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"添加地址";
    _provinces = [NSMutableDictionary dictionary];
    _citys = [NSMutableDictionary dictionary];
    _districts = [NSMutableDictionary dictionary];
    
    
    [self updateProvinceFields];
    
    self.formController = [[FXFormController alloc] init];
    AddAddressFxForm* form = [[AddAddressFxForm alloc] init];
    
    if (self.editMode) {
        self.title = @"地址修改";
        form.detailAddress = self.address.address;
        form.consignName = self.address.realname;
        form.mobile = self.address.mobile;
    }
    self.formController.form = form;
    
    [MBProgressHUD showAddViewWhen:self.view when:^{
        
        //province
        GetProvinceListResponse* response =  [[AddressServices instance] getProvinceList];
        NSMutableArray* array = [NSMutableArray array];
        Province* current;
        for (Province* province in response.data) {
            [self.provinces setObject:province forKey:province.province_name];
            [array addObject:province.province_name];
            if ([province.province_id isEqualToString:self.address.province_id]) {
                current = province;
            }
        }
        AddAddressFxForm* form = (AddAddressFxForm*) self.formController.form;
        form.provinceArray = array;
        form.province = current.province_name;
        
        
        //city
        GetCityListResponse* cityresponse =  [[AddressServices instance] getCityList:self.address.province_id.intValue];
        City* currentcity;
        
        array = [NSMutableArray array];
        for (City* city in cityresponse.data) {
            [self.citys setObject:city forKey:city.city_name];
            [array addObject:city.city_name];
            if ([city.city_id isEqualToString:self.address.city_id]) {
                currentcity = city;
            }
        }
        form = (AddAddressFxForm*) self.formController.form;
        form.cityArray = array;
        form.city = currentcity.city_name;
        
        
        
        GetAreaListResponse* arearesponse =  [[AddressServices instance] getAreaList:self.address.city_id.intValue];
        Area* currentArea = nil;
        NSMutableArray  *areaarray = [NSMutableArray array];
        for (Area* area in arearesponse.data) {
            if ([area.area_id isEqualToString:self.address.area_id]) {
                currentArea = area;
            }
            [self.districts setObject:area forKey:area.area_name];
            [areaarray addObject:area.area_name];
        }
        
        form = (AddAddressFxForm*) self.formController.form;
        form.areaArray = areaarray;
        form.area = currentArea.area_name;
        
        
        [APP runBlockInMainQ:^{
            self.formController.form = self.formController.form;
            [self.tableView reloadData];
        }];
        
    } complete:^{
        
    } text:@"请稍等"];
    
    self.formController.tableView = self.tableView;
    self.formController.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Fxform
- (void)submit:(UITableViewCell<FXFormFieldCell> *)cell
{
    //we can lookup the form from the cell if we want, like this:
    AddAddressFxForm *form = (AddAddressFxForm*)cell.field.form;
    
    Province* province = self.provinces[form.province];
    int proviceId = [province.province_id intValue];
    
    
    City* city = self.citys[form.city];
    int cityId = [city.city_id intValue];
    
    
    Area* area = self.districts[form.area];
    int areaId = [area.area_id intValue];
    
    //we can then perform validation, etc
    if (form.consignName.length==0)
    {
        [[[UIAlertView alloc] initWithTitle:@"对不起，请填写收货人姓名" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil] show];
    }
    else if(![tools validateNumber:form.mobile]||form.mobile.length!=11)
    {
        [[[UIAlertView alloc] initWithTitle:@"对不起，请填写正确手机号码" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil] show];
    }else if (form.province.length==0){
        [[[UIAlertView alloc] initWithTitle:@"对不起，请选择省份" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil] show];
    }else if(form.city.length==0){
        [[[UIAlertView alloc] initWithTitle:@"对不起，请选择区／县" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil] show];
    }else if(form.detailAddress.length==0){
        [[[UIAlertView alloc] initWithTitle:@"对不起，请填写详细地址" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil] show];
    }else{
        //submit add || edit form
        [MBProgressHUD showAddViewWhen:self.view when:^{
            if (!self.editMode) {
                AddAddressResponse* response = [[AddressServices instance] addAddress:form.consignName mobile:form.mobile province_id:proviceId city_id:cityId area_id:areaId address:form.detailAddress];
                if ([response.code isEqualToString:OBJC_SUCCEED_CODE]) {
                    [APP runBlockInMainQ:^{
                        [SVProgressHUD showSuccessWithStatus:@"添加成功"];
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }else{
                    [APP runBlockInMainQ:^{
                        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"添加失败%@",response.error_msg?response.error_msg:@""]];
                    }];
                }
            }else{
                EditAddressResponse* response = [[AddressServices instance] editAddress:form.consignName mobile:form.mobile province_id:proviceId city_id:cityId area_id:areaId address:form.detailAddress ofUserAddressID:self.address.user_address_id];
                if ([response.code isEqualToString:OBJC_SUCCEED_CODE]) {
                    [APP runBlockInMainQ:^{
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }
                
                
            }
        } text:@"请稍等"];
        
    }
}


- (void)updateProvince:(UITableViewCell<FXFormFieldCell> *)cell{
    AddAddressFxForm *form = (AddAddressFxForm*)cell.field.form;
    Province* province  = self.provinces[form.province];
    int pid = [[province province_id] intValue];
    [self updateCityFields:pid];
}

- (void)updateCity:(UITableViewCell<FXFormFieldCell> *)cell{
    AddAddressFxForm *form = (AddAddressFxForm*)cell.field.form;
    City* city = self.citys[form.city];
    int cityID = [city.city_id intValue];
    [self updateDistrictFields:cityID];
}

- (void)updateProvinceFields
{
    //refresh the form
    //fetch city based on the province
    [MBProgressHUD showAddViewWhen:self.view when:^{
        GetProvinceListResponse* response =  [[AddressServices instance] getProvinceList];
        NSMutableArray* array = [NSMutableArray array];
        for (Province* province in response.data) {
            [self.provinces setObject:province forKey:province.province_name];
            [array addObject:province.province_name];
        }
        AddAddressFxForm* form = (AddAddressFxForm*) self.formController.form;
        form.provinceArray = array;
        //        form.area = nil;
        [APP runBlockInMainQ:^{
            self.formController.form = self.formController.form;
            [self.tableView reloadData];
        }];
    } text:@"请稍等"];
}

- (void)updateCityFields:(int)provinceID
{
    [MBProgressHUD showAddViewWhen:self.view when:^{
        GetCityListResponse* response =  [[AddressServices instance] getCityList:provinceID];
        
        NSMutableArray* array = [NSMutableArray array];
        for (City* city in response.data) {
            [self.citys setObject:city forKey:city.city_name];
            [array addObject:city.city_name];
        }
        AddAddressFxForm* form = (AddAddressFxForm*) self.formController.form;
        form.cityArray = array;
        //        form.districtArray = nil;
        [APP runBlockInMainQ:^{
            self.formController.form = self.formController.form;
            [self.tableView reloadData];
        }];
    } text:@"请稍等"];
    
    
}

- (void)updateDistrictFields:(int)cityID
{
    [MBProgressHUD showAddViewWhen:self.view when:^{
        GetAreaListResponse* response =  [[AddressServices instance] getAreaList:cityID];
        
        NSMutableArray* array = [NSMutableArray array];
        for (Area* area in response.data) {
            [self.districts setObject:area forKey:area.area_name];
            [array addObject:area.area_name];
        }
        AddAddressFxForm* form = (AddAddressFxForm*) self.formController.form;
        form.areaArray = array;
        [APP runBlockInMainQ:^{
            self.formController.form = self.formController.form;
            [self.tableView reloadData];
        }];
    } text:@"请稍等"];
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
