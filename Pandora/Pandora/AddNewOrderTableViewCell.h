//
//  AddNewOrderTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/7.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "Cart.h"
@interface AddNewOrderTableViewCell : BaseTableViewCell
@property(nonatomic,strong) Cart* cart;
@end
