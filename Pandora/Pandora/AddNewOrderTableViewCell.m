//
//  AddNewOrderTableViewCell.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/7.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "AddNewOrderTableViewCell.h"
@interface AddNewOrderTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *itemImage;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *amoutLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end


@implementation AddNewOrderTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCart:(Cart *)cart{
    _cart = cart;
    [_itemImage sd_setImageWithURL:[NSURL URLWithString:_cart.small_pic relativeToURL:[NSURL URLWithString:OBJC_IMGBASE_URL]]];
    [_priceLabel setText:[NSString stringWithFormat:@"¥ %@",_cart.mall_price]];
    [_amoutLabel setText:[NSString stringWithFormat:@"%@",_cart.number]];
    [_titleLabel setText:[NSString stringWithFormat:@"%@",_cart.item_name]];
}

@end
