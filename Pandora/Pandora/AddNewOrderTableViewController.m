//
//  AddNewOrderTableViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/7.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "AddNewOrderTableViewController.h"
#import "AddNewOrderTableViewCell.h"
#import "AddressServices.h"
#import "ItemServices.h"
#import "OrderServices.h"
#import "PayOrderViewController.h"
#import "AddressTableViewController.h"
#define Section1Cell @"Section1Cell"
#define Section2Cell @"Section2Cell"

@interface AddNewOrderTableViewController ()
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (nonatomic,strong) Address* address;
@property (weak, nonatomic) IBOutlet UILabel *totalMoney;
@property (weak, nonatomic) IBOutlet UITextField *remarkLabel;

@property (weak, nonatomic) IBOutlet UILabel *deliverToLabel;

@property (weak, nonatomic) IBOutlet UILabel *expressFeeLabel;
@property (nonatomic,strong) NSString* expressFee;
@end

@implementation AddNewOrderTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"确认订单";
    //    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:Section1Cell];
    [self.tableView registerNib:[AddNewOrderTableViewCell nib] forCellReuseIdentifier:Section2Cell];
    self.clearsSelectionOnViewWillAppear = YES;
    [MBProgressHUD showAddViewWhen:self.view when:^{
        GetAddressListResponse* addressList =  [[AddressServices instance] getAddressList];
        
        self.address = addressList.data.firstObject;
        if (self.address) {
            GetProvinceListResponse* province = [[AddressServices instance] getProvinceList];
            for (Province* p in province.data) {
                if ([p.province_id isEqualToString:self.address.province_id]) {
                    self.address.province_name = p.province_name;
                    break;
                }
            }
            
            GetCityListResponse* city = [[AddressServices instance] getCityList:self.address.province_id.intValue];
            for (City* p in city.data) {
                if ([p.city_id isEqualToString:self.address.city_id]) {
                    self.address.city_name = p.city_name;
                    break;
                }
            }
            
            GetAreaListResponse* areas = [[AddressServices instance] getAreaList:self.address.city_id.intValue];
            for (Area* p in areas.data) {
                if ([p.area_id isEqualToString:self.address.area_id]) {
                    self.address.area_name = p.area_name;
                    break;
                }
            }
            
        }
        
        
        
    } complete:^{
        [self.tableView reloadData];
    } text:@"正在生成订单"];
    
    self.footerView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 200.0);
    [self.tableView setTableFooterView:self.footerView];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section==0) {
        return 1;
    }else{
        return self.carts.count;
    }
    return 0;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section==1) {
        return @"订单信息";
    }
    return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Section1Cell];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:Section1Cell];
        }
        [[cell textLabel] setText:@"收货地址:"];
        [[cell textLabel] setTextColor:DEFAULT_GREEN];

        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (self.address!=nil) {
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@",self.address.province_name,self.address.city_name,self.address.area_name,self.address.address,self.address.realname,self.address.mobile]];
            [[cell detailTextLabel] setMinimumScaleFactor:0.7];
            [[cell detailTextLabel] setFont:[UIFont systemFontOfSize:15.0]];
        }
        return cell;
    }else{
        AddNewOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Section2Cell forIndexPath:indexPath];
        [cell setCart:self.carts[indexPath.row]];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        AddressTableViewController* adtc = [[AddressTableViewController alloc] init];
        [adtc setSelectBlock:^(Address* address){
            self.address = address;
        }];
        [self.navigationController pushViewController:adtc animated:YES];
    }else{
        
    }
}

-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        return NO;
    }else{
        return YES;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return 60.0;
    }else{
        return 85.0;
    }
}

-(void)setAddress:(Address *)address{
    _address = address;
    if (_address) {
        [APP runBlockInInitialQ:^{
            [self webrequestCalFee];
            [APP runBlockInMainQ:^{
                [[self deliverToLabel] setText:[NSString stringWithFormat:@"送至［%@］需运费:",_address.city_name?_address.city_name:@""]];
                [self.tableView reloadData];
            }];
        }];
    }    
}


-(void)webrequestCalFee{
    double weight = 0 ;
    double value = 0;
    for (Cart* cart in self.carts) {
        if (!cart.perWeight) {
            GetItemInfoResponse* response = [[ItemServices instance] getItemInfo:cart.item_id.intValue];
            weight+=response.data.weight.doubleValue*cart.number.integerValue;
            value += cart.number.integerValue*cart.mall_price.doubleValue;
        }else{
            weight+=cart.perWeight.doubleValue*cart.number.integerValue;
            value += cart.number.integerValue*cart.mall_price.doubleValue;
        }
        
    }
    CalExpressFeeResponse* response = [[AddressServices instance] calExpressFee:self.address.city_id.intValue total_weight:weight total_amount:value];
    [APP runBlockInMainQ:^{
        self.expressFee = [NSString stringWithFormat:@"%.2f",response.express_fee];
        self.totalMoney.text = [@"¥ " stringByAppendingFormat:@"%.2f",(value + response.express_fee)];
    }];
    
}


-(void)setExpressFee:(NSString *)expressFee{
    _expressFee = expressFee;
    [APP runBlockInMainQ:^{
        self.expressFeeLabel.text = [@"¥ " stringByAppendingString:_expressFee];
    }];
}

- (IBAction)paybyali:(id)sender {
    if (self.address==nil) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"地址空" message:@"请先去设置地址" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        Cart* cart = self.carts.firstObject;
        NSMutableString* cartIDs = [cart.shopping_cart_id mutableCopy];
        NSMutableString* numbers = [[self.carts.firstObject number] mutableCopy];
        for (int i=1;i < self.carts.count;i++) {
            Cart* cart = [self.carts objectAtIndex:i];
            [cartIDs appendFormat:@",%@",cart.shopping_cart_id];
            [numbers appendFormat:@",%@",cart.number];
        }
        [MBProgressHUD showAddViewWhen:self.view when:^{
            AddOrderResponse* response = [[OrderServices instance] addOrder:self.address.user_address_id.intValue shopping_cart_ids:cartIDs number_str:numbers payway_id:Alipay_ID.intValue payway_name:@"支付宝" user_remark:self.remarkLabel.text? self.remarkLabel.text:@""];
            if (response.data) {
                [APP runBlockInMainQ:^{
                    PayOrderViewController* pvc = [PayOrderViewController getInstanceWithSameNibName];
                    pvc.orderID = response.data;
                    pvc.defaultPayway_name = @"支付宝";
                    [self.navigationController pushViewController:pvc animated:YES];
                }];
            }
            
        } complete:^{
            
        } text:@"正在提交订单"];
        
        
        
    }
    
}

- (IBAction)paybywallet:(id)sender {
    if (self.address==nil) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"地址空" message:@"请先去设置地址" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        Cart* cart = self.carts.firstObject;

        NSMutableString* cartIDs = [cart.shopping_cart_id mutableCopy];
        NSMutableString* numbers = [[self.carts.firstObject number] mutableCopy];
        for (int i=1;i < self.carts.count;i++) {
            Cart* cart = [self.carts objectAtIndex:i];
            [cartIDs appendFormat:@",%@",cart.shopping_cart_id];
            [numbers appendFormat:@",%@",cart.number];
        }
        [MBProgressHUD showAddViewWhen:self.view when:^{
            AddOrderResponse* response = [[OrderServices instance] addOrder:self.address.user_address_id.intValue shopping_cart_ids:cartIDs number_str:numbers payway_id:Alipay_ID.intValue payway_name:@"种植币" user_remark:self.remarkLabel.text? self.remarkLabel.text:@""];
            if (response.data) {
                [APP runBlockInMainQ:^{
                    PayOrderViewController* pvc = [PayOrderViewController getInstanceWithSameNibName];
                    pvc.orderID = response.data;
                    pvc.defaultPayway_name = @"种植币";
                    pvc.completeBlock = ^(BOOL success){
                        [self.navigationController popViewControllerAnimated:YES];
                    };
                    [self.navigationController pushViewController:pvc animated:YES];
                }];
            }
            
        } complete:^{
            
        } text:@"正在提交订单"];
        
        
        
    }
}

@end
