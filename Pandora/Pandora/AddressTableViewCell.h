//
//  AddrssManageTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "Address.h"
@interface AddressTableViewCell : BaseTableViewCell
@property(nonatomic,strong)Address* address;
@end
