//
//  AddrssManageTableViewCell.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "AddressTableViewCell.h"
@interface AddressTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *consignLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkImageview;
@end

@implementation AddressTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.checkImageview.image = [UIImage imageNamed:!selected?@"confirm":@"confirm_on"];
}

-(void)setAddress:(Address *)address{
    _address =  address;
    self.consignLabel.text = _address.realname;
    self.addressLabel.text = _address.address;
}

@end
