//
//  AddressTableViewController.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//  新地址

#import <UIKit/UIKit.h>
#import "Address.h"
typedef void(^SelectBlock)(Address* address);
@interface AddressTableViewController : UITableViewController
@property(nonatomic,strong) SelectBlock selectBlock ;
@end
