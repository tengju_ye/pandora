//
//  AddressTableViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "AddressTableViewController.h"
#import "MJRefresh.h"
#import "AddressTableViewCell.h"
#import "BaseTableViewCell.h"
#import "AddressServices.h"
#import "AddAddressViewController.h"
@interface AddressTableViewController ()
@property(nonatomic,strong) NSArray* datasource;
@property(nonatomic,strong) Address* userAddress;
@property(nonatomic,strong) UIButton* addAddressButton;
@end

static NSString* const Identifier_AddressCell = @"Identifier_AddressCell";
static NSString* const Identifier_AddAddressCell = @"Identifier_AddAddressCell";

@implementation AddressTableViewController

-(instancetype)init{
    if (self = [super init]) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"地址管理";
    [self.tableView registerNib:[AddressTableViewCell nib] forCellReuseIdentifier:Identifier_AddressCell];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:Identifier_AddAddressCell];
    
    __weak AddressTableViewController* ws = self;
    [self.tableView addHeaderWithCallback:^{
        [APP runBlockInInitialQ:^{
            GetAddressListResponse* response = [[AddressServices instance] getAddressList];
            ws.datasource = response.data;
            
            [APP runBlockInMainQ:^{
                [ws.tableView reloadData];
                [ws.tableView headerEndRefreshing];
            }];
        }];
    }];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [self.tableView headerBeginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;//address && add address
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    switch (section) {
    //        case 0:
    //            return self.datasource.count;
    //        case 1:
    //            return 1;
    //        default:
    //            break;
    //    }
    return self.datasource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    switch (indexPath.section) {
        case 0:{
            AddressTableViewCell* adcell = [tableView dequeueReusableCellWithIdentifier:Identifier_AddressCell forIndexPath:indexPath];
            [adcell setAddress:self.datasource[indexPath.row]];
            cell = adcell;
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
            break;
            //        case 1:
            //        {
            //            cell = [tableView dequeueReusableCellWithIdentifier:Identifier_AddAddressCell forIndexPath:indexPath];
            //            cell.textLabel.text = @"新增收货地址";
            //            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            //            cell.backgroundColor  = [UIColor lightGrayColor];
            //        }
            //            break;
        default:
            break;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
            
            if (self.selectBlock!=nil) {
                Address* address = self.datasource[indexPath.row];
                
                [MBProgressHUD showAddViewWhen:self.view when:^{
                    
                    GetProvinceListResponse* province = [[AddressServices instance] getProvinceList];
                    for (Province* p in province.data) {
                        if ([p.province_id isEqualToString:address.province_id]) {
                            address.province_name = p.province_name;
                            break;
                        }
                    }
                    
                    GetCityListResponse* city = [[AddressServices instance] getCityList:address.province_id.intValue];
                    for (City* p in city.data) {
                        if ([p.city_id isEqualToString:address.city_id]) {
                            address.city_name = p.city_name;
                            break;
                        }
                    }
                    
                    GetAreaListResponse* areas = [[AddressServices instance] getAreaList:address.city_id.intValue];
                    for (Area* p in areas.data) {
                        if ([p.area_id isEqualToString:address.area_id]) {
                            address.area_name = p.area_name;
                            break;
                        }
                    }
                    
                    
                } complete:^{
                } text:@"正在设置地址"];
                
                self.selectBlock(address);
            }
        }
            break;
        case 1:{
            //            //进入添加地址
            //            AddAddressViewController* aavc = [AddAddressViewController getInstanceWithSameNibName];
            //            aavc.editMode = NO;
            //            [self.navigationController pushViewController:aavc animated:YES];
            //            break;
        }
        default:
            break;
    }
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:{
            //进入地址修改
            Address* address = [self.datasource objectAtIndex:indexPath.row];
            AddAddressViewController* aavc = [AddAddressViewController getInstanceWithSameNibName];
            aavc.address = address;
            aavc.editMode = YES;
            [self.navigationController pushViewController:aavc animated:YES];
        }
            break;

        default:
            
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 63.0;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 60.0;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return self.addAddressButton;
            break;
            
        default:
            break;
    }
    return nil;
}


-(void)addAddress:(id)sender{
    //进入添加地址
    AddAddressViewController* aavc = [AddAddressViewController getInstanceWithSameNibName];
    aavc.editMode = NO;
    [self.navigationController pushViewController:aavc animated:YES];
}


#pragma mark - getter
-(UIButton *)addAddressButton{
    if (!_addAddressButton) {
        UIButton* addAddressButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
        [addAddressButton setBackgroundColor:DEFAULT_GREEN];
        [addAddressButton setTintColor:[UIColor whiteColor]];
        [addAddressButton setTitle:@"添加地址" forState:UIControlStateNormal];
        [addAddressButton addTarget:self action:@selector(addAddress:) forControlEvents:UIControlEventTouchUpInside];
        _addAddressButton = addAddressButton;
    }
    return _addAddressButton;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
