//
//  AuthFriendTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 15/2/12.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
@interface AuthFriendTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@end
