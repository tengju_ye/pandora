//
//  AuthFriendTableViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 15/2/12.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "AuthFriendTableViewController.h"
#import "AuthFriendTableViewCell.h"
#import "PlanterServices.h"
#import "tools.h"
#import "WXApi.h"
static NSString* const CellIdentifier = @"CellIdentifier";

@interface AuthFriendTableViewController ()
@property (nonatomic,strong) NSMutableArray* datasource;
@end

@implementation AuthFriendTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.title = @"授权好友列表";
    __weak AuthFriendTableViewController* ws = self;
    [self.tableView registerNib:[AuthFriendTableViewCell nib] forCellReuseIdentifier:CellIdentifier];
    
    [self.tableView addHeaderWithCallback:^{
        GetPlanterAuthListResponse* response = [[PlanterServices instance] getPlanterAuthList:ws.planter_id];
        [APP runBlockInInitialQ:^{
            [ws.datasource removeAllObjects];
            [ws.datasource addObjectsFromArray:response.data];
            [ws.tableView headerEndRefreshing];
            [ws.tableView reloadData];
        }];
    }];
    [self.tableView headerBeginRefreshing];
    
    
    UIImage* image = [UIImage imageNamed:@"back_icon"];
    UIButton* btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    [btn setImage:image forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItems = @[item,self.editButtonItem];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AuthFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    GetPlanterAuthListData* data = [self.datasource objectAtIndex:indexPath.row];
    cell.nameLabel.text = data.nickname;
    cell.phoneLabel.text = data.mobile;
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self.datasource removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
}



#pragma mark - Table view delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [MBProgressHUD showAddViewWhen:self.view when:^{
        GetPlanterShareInfoResponse* shareInfo = [[PlanterServices instance] getPlanterShareInfo:self.planter_id];
        
        NSURL *url = [NSURL URLWithString:shareInfo.img];
        NSData* data = [NSData dataWithContentsOfURL:url];
        UIImage* image =[UIImage imageWithData:data];
        [APP runBlockInMainQ:^{
            WXMediaMessage *message = [WXMediaMessage message];
            message.title = shareInfo.title;
            message.description = shareInfo.content;
            [message setThumbImage:image];
            
            WXWebpageObject *ext = [WXWebpageObject object];
            ext.webpageUrl = shareInfo.link;
            
            message.mediaObject = ext;
            message.mediaTagName = @"WECHAT_TAG_JUMP_SHOWRANK";
            
            SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
            req.bText = NO;
            req.message = message;
            req.scene = WXSceneSession;
            [WXApi sendReq:req];
        }];
    } text:nil];
}



-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - getter
-(NSMutableArray *)datasource{
    if (!_datasource) {
        _datasource = [NSMutableArray new];
    }
    return _datasource;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
