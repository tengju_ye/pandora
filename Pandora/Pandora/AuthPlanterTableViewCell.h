//
//  AuthPlanterTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 15/2/11.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
@interface AuthPlanterTableViewCell : BaseTableViewCell
@property (nonatomic,strong) NSString* name;
@property (nonatomic,strong) void (^didTouchAuthButton)(AuthPlanterTableViewCell* cell) ;
@end
