//
//  AuthPlanterTableViewCell.m
//  Pandora
//
//  Created by 小南宫的mac on 15/2/11.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "AuthPlanterTableViewCell.h"

@interface AuthPlanterTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *authButton;

@end

@implementation AuthPlanterTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.authButton.layer.cornerRadius = CGRectGetHeight(self.authButton.bounds) / 2;
    self.authButton.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setName:(NSString *)name{
    _name = name;
    self.nameLabel.text = _name;
}


- (IBAction)touchAuthButton:(id)sender {
    if (self.didTouchAuthButton) {
        self.didTouchAuthButton(self);
    }
}

@end
