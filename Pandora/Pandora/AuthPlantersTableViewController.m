//
//  AuthPlantersTableViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 15/2/11.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "AuthPlantersTableViewController.h"
#import "MJRefresh.h"
#import "PlanterServices.h"
#import "AuthPlanterTableViewCell.h"
#import "AuthFriendTableViewController.h"
#import "UIActionSheet+Blocks.h"
#import "tools.h"
#import "UIViewController+ShowRight.h"
#import "IBActionSheet.h"
static NSString* const CellIdentifier = @"CellIdentifier";
@interface AuthPlantersTableViewController ()
@property (nonatomic,strong) NSArray* datasource;
@end

@implementation AuthPlantersTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"种植机列表";
    
    [tools addRightBarItem:self];
    
    [self.tableView registerNib:[AuthPlanterTableViewCell nib] forCellReuseIdentifier:CellIdentifier];
    [self.tableView setSeparatorColor:DEFAULT_GREEN];
    __weak AuthPlantersTableViewController* ws = self;
    [self.tableView addHeaderWithCallback:^{
        GetOwnedPlanterListResponse* response = [[PlanterServices instance] getOwnedPlanterList];
        [APP runBlockInInitialQ:^{
            if ([response.code isEqualToString:OBJC_SUCCEED_CODE]) {
                ws.datasource = response.data;
            }
            [[ws tableView] headerEndRefreshing];
            [[ws tableView] reloadData];
        }];
    }];
    [self.tableView headerBeginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AuthPlanterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    GetOwnedPlanterPlanter* planter = [self.datasource objectAtIndex:indexPath.row];
    
    [cell setName:planter.planter_name];
    [cell setDidTouchAuthButton:^(AuthPlanterTableViewCell * cell) {
        
        UIActionSheet* sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:nil cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"微信好友", nil];
        
        [sheet setTapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex){
            if (buttonIndex == 0) {
                AuthFriendTableViewController* friends = [AuthFriendTableViewController new];
                [friends setPlanter_id:planter.planter_id];
                [self.navigationController pushViewController:friends animated:YES];
            }
        }];
        
        
        [sheet showInView:self.view];

    }];
    
    return cell;
}

#pragma mark - TableView delegate
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

@end
