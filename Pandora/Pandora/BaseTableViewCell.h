//
//  BaseTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewCell : UITableViewCell
+(UINib*)nib;
//override
-(void)configCellWithModel:(id)model;
@end
