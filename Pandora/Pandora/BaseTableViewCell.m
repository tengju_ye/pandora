//
//  BaseTableViewCell.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "BaseTableViewCell.h"

@implementation BaseTableViewCell

+(UINib *)nib{
    UINib* nib = [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
    return nib;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configCellWithModel:(id)model{


}

@end
