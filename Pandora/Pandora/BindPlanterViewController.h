//
//  BindPlanterViewController.h
//  Pandora
//
//  Created by junchen on 12/3/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BindPlanterViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *textField;

@end
