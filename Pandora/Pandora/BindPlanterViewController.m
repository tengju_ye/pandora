//
//  BindPlanterViewController.m
//  Pandora
//
//  Created by junchen on 12/3/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "BindPlanterViewController.h"
#import "PlanterServices.h"
#import "UIAlert+Blocks.h"
#import "tools.h"
#import "QRCodeReaderViewController.h"
#import "RightMenuViewController.h"
#import "NGQRCodeReaderViewController.h"
@interface BindPlanterViewController ()<QRCodeReaderDelegate>

@end

@implementation BindPlanterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"绑定种植机";
    [tools addRightBarItem:self];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)bindPlanter:(id)sender {
    if([self.textField.text isEqualToString:@""]) {
    } else {
        NSString *serialNum = self.textField.text;
        BindNewPlanterResponse *response = [[PlanterServices instance] bindNewPlanter:serialNum];
        if ([response.code intValue] == 0) {
            
            UIAlertView* alert =  [[UIAlertView alloc] initWithTitle:@"绑定成功" message:nil buttonTitle:@"确定" block:^{
                RightMenuViewController* rmvc = (RightMenuViewController*)[self.mm_drawerController rightDrawerViewController];
                [rmvc selectedIdx:kMyPlantIdx];
            }];
            [alert show];
        } else {
            UIAlertView* alert =  [[UIAlertView alloc] initWithTitle:@"绑定失败" message:nil cancelButtonTitle:@"确定" cancelBlock:^{
            } proceedButtonTitle:nil proceedBlock:nil];
            [alert show];
        }
        
    }
    
}

- (IBAction)scanCode:(id)sender {
    QRCodeReaderViewController *reader = [[NGQRCodeReaderViewController alloc] initWithCancelButtonTitle:@"关闭"];
    reader.modalPresentationStyle      = UIModalPresentationFormSheet;
    
    // Using delegate methods
    reader.delegate                    = self;
    
    
    [self presentViewController:reader animated:YES completion:NULL];

}


#pragma mark - QRCodeReader Delegate Methods

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result
{
    [self dismissViewControllerAnimated:YES completion:^{
        self.textField.text = result;
    }];
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}
 
 
- (IBAction)hidekeyboard:(id)sender {
    [self.textField resignFirstResponder];
}

@end
