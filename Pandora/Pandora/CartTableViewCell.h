//
//  CartTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "Cart.h"
@class CartTableViewCell;
@protocol CartTableDelegate<NSObject>
-(void)deleteDidclick:(CartTableViewCell*)cell;
-(void)coundtfDidendEditing:(CartTableViewCell*)cell;
-(void)checkBtnDidClick:(CartTableViewCell*)cell;
@end

@interface CartTableViewCell : BaseTableViewCell
@property(nonatomic,strong) id<CartTableDelegate> delegate;
@property(nonatomic,strong) Cart* cart;
@end
