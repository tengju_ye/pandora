//
//  CartTableViewCell.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "CartTableViewCell.h"
@interface CartTableViewCell()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UITextField *countTF;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cartImage;
@property (weak, nonatomic) IBOutlet UILabel *shouldPay;

@end


@implementation CartTableViewCell



- (void)awakeFromNib {
    // Initialization code
    self.deleteBtn.layer.cornerRadius = 1.5;
    self.countTF.delegate = self;
    [self.deleteBtn setClipsToBounds:YES];
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    [self.countTF resignFirstResponder];
    int value = self.countTF.text.integerValue;
    self.cart.number = @(value).stringValue;
    if ([self.delegate respondsToSelector:@selector(coundtfDidendEditing:)]) {
        [self.delegate coundtfDidendEditing:self];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}



//- (IBAction)editEnd:(id)sender {
//    [self.countTF resignFirstResponder];
//    if ([self.delegate respondsToSelector:@selector(coundtfDidendEditing:)]) {
//        [self.delegate coundtfDidendEditing:self];
//    }
//}


- (IBAction)checkBtn:(id)sender {
    [self.checkBtn setSelected:!self.checkBtn.isSelected];
    if ([self.delegate respondsToSelector:@selector(checkBtnDidClick:)]) {
        [self.delegate checkBtnDidClick:self];
    }
}

- (IBAction)deleteCell:(id)sender {
    if ([self.delegate respondsToSelector:@selector(deleteDidclick:)]) {
        [self.delegate deleteDidclick:self];
    }
}


-(void)setCart:(Cart *)cart{
    _cart = cart;
    [self.cartImage sd_setImageWithURL:[NSURL URLWithString:cart.small_pic relativeToURL:[NSURL URLWithString:OBJC_IMGBASE_URL]]];
    [self.titleLabel setText:_cart.item_name];
    [self.priceLabel setText:[@"¥" stringByAppendingString:_cart.mall_price]];
    self.countTF.text = _cart.number;
    double value = _cart.number.integerValue * _cart.mall_price.doubleValue;
    self.shouldPay.text = [NSString stringWithFormat:@"¥%.2f",value];
    self.checkBtn.selected = _cart.choose.boolValue;
}

@end
