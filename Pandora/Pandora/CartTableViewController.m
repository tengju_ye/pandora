//
//  CartTableViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "CartTableViewController.h"
#import "CartServices.h"
#import "CartTableViewCell.h"
#import "UIAlertView+MyBlocks.h"
#import "ItemRevealController.h"
#import "ItemServices.h"
#import "Item.h"
#import "AddNewOrderTableViewController.h"
#define Identifier @"reuseIdentifier"
@interface CartTableViewController ()<CartTableDelegate>
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryLabel;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@property (nonatomic,strong)GetCartListResponse* cartListResponse;
@property (nonatomic,strong) NSMutableArray* carts;
@end

@implementation CartTableViewController


-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        UIImage* image =[UIImage imageNamed:@"cart_icon"];
        [self.navigationController.tabBarItem setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        image = [UIImage imageNamed:@"cart_icon_on"];
        [self.navigationController.tabBarItem setSelectedImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.submitBtn.layer.cornerRadius = 1.0;
    [self.submitBtn setClipsToBounds:YES];
    __weak CartTableViewController* ws = self;
    [self.tableView registerNib:[CartTableViewCell nib] forCellReuseIdentifier:Identifier];
    [self.tableView addHeaderWithCallback:^{
        [APP runBlockInInitialQ:^{
            [ws webGetList];
            [APP runBlockInMainQ:^{
                [ws.tableView headerEndRefreshing];
                [ws.tableView reloadData];
                [ws refreshTotalNumber];
            }];
        }];
    }];
    [self.tableView headerBeginRefreshing];

}



-(void)webGetList{
    GetCartListResponse* cs = [[CartServices instance] getCartList];

    self.cartListResponse = cs;
    [self.carts removeAllObjects];
    for (Cart* cart in self.cartListResponse.data) {
        cart.choose = @(YES);
        [self.carts addObject:cart];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.carts.count;
}
- (IBAction)chooseAll:(id)sender {
    [sender setSelected:![sender isSelected]];
    for (Cart* cart in self.carts) {
        cart.choose = @([sender isSelected]);
    }
    [self.tableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier" forIndexPath:indexPath];
    [cell setCart:self.carts[indexPath.row]];
    cell.delegate = self;
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 151.0;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Table view delegate
 
 // In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 // Navigation logic may go here, for example:
 // Create the next view controller.
 <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
 
 // Pass the selected object to the new view controller.
 
 // Push the view controller.
 [self.navigationController pushViewController:detailViewController animated:YES];
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)deleteDidclick:(CartTableViewCell*)cell{
    __block DeleteCartResponse* deleteresponse = nil;
    [MBProgressHUD showAddViewWhen:self.view when:^{
        deleteresponse =   [[CartServices instance] deleteCart:cell.cart.shopping_cart_id.intValue];
        
    } complete:^{
        if ([deleteresponse.code isEqualToString:OBJC_SUCCEED_CODE]) {
            NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
            if (indexPath!=nil) {
                [self.carts removeObjectAtIndex:indexPath.row];
                    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
            [self refreshTotalNumber];
        }
    } text:@"请稍等"];
}
-(void)coundtfDidendEditing:(CartTableViewCell*)cell{
    NSIndexPath* path = [self.tableView indexPathForCell:cell];
    if (path!=nil) {
            [self.tableView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
    }
    [self refreshTotalNumber];
}

-(void)checkBtnDidClick:(CartTableViewCell*)cell{
    NSIndexPath* path = [self.tableView indexPathForCell:cell];
    cell.cart.choose = @(!cell.cart.choose.boolValue);
    if (path!=nil) {
            [self.tableView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
    }

}

-(void)refreshTotalNumber{
    double v = 0.0;
    int count = 0;
    for (Cart* cart in self.carts) {
        v+=(cart.mall_price.doubleValue*cart.number.intValue);
        count +=cart.number.intValue;
    }
    [self.totalPriceLabel setText:[NSString stringWithFormat:@"¥%.2f",v]];
    [self.summaryLabel setText:[NSString stringWithFormat:@"(共%d件，不含运费)",count]];

}

- (IBAction)submit:(id)sender {
    int count = 0;
    NSMutableArray* arr = [NSMutableArray array];
    for (Cart* cart in self.carts) {
        if (cart.number.integerValue!=0&&cart.choose.boolValue) {
            [arr addObject:cart];
        }
        count +=cart.number.intValue;
    }
    if (count==0) {
        [UIAlertView showAlertViewWithTitle:@"请至少选一种商品" message:nil cancelButtonTitle:@"好的" otherButtonTitles:nil onDismiss:^(int buttonIndex, UIAlertView *alertView) {
            
        } onCancel:^{
            
        }];
    }else{
        AddNewOrderTableViewController* anotc = [AddNewOrderTableViewController getInstanceWithSameNibName];
        anotc.carts = arr ;
        anotc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:anotc animated:YES];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Cart* cart = [[self carts] objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    __block Item* item = nil;
    [MBProgressHUD showAddViewWhen:self.view when:^{
        GetItemInfoResponse* response = [[ItemServices instance] getItemInfo:cart.item_id.intValue];
        item = response.data;
        item.item_id = cart.item_id;
    } complete:^{
        if (item!=nil) {
            ItemRevealController* irc = [[ItemRevealController alloc] initWithNibName:[ItemRevealController nibName] bundle:nil];
            irc.item = item;
            [self.navigationController pushViewController:irc animated:YES];
        }
    } text:@"请稍等"];
    
}

-(NSMutableArray *)carts{
    if (!_carts) {
        _carts = [NSMutableArray array];
    }
    return _carts;
}

@end
