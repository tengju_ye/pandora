//
//  ChangeSettingViewController.h
//  Pandora
//
//  Created by junchen on 12/6/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SeedStateViewController.h"

@interface ChangeSettingViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *time;
@property (weak, nonatomic) IBOutlet UITextField *period;


@property int ton;
@property int toff;

@property (nonatomic) SeedStateViewController *parentController;

@end
