//
//  ChangeSettingViewController.m
//  Pandora
//
//  Created by junchen on 12/6/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "ChangeSettingViewController.h"
#import "PlanterServices.h"

@interface ChangeSettingViewController ()<UITextFieldDelegate>

@end

@implementation ChangeSettingViewController

- (void)viewDidLoad {
    self.time.text = @(self.ton).stringValue;
    self.period.text = @(self.toff).stringValue;
    [super viewDidLoad];
    self.time.text = [NSString stringWithFormat:@"%d", self.ton/60];
    self.period.text = [NSString stringWithFormat:@"%d", self.toff/60];
}

-(CGSize)preferredContentSize{
    return CGSizeMake(295, 302);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)changeSetting:(id)sender {
    
    MBProgressHUD* hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.removeFromSuperViewOnHide = YES;
    hud.labelText = @"保存设置";
    [hud showAnimated:YES whileExecutingBlock:^{
        [[PlanterServices instance] setWaterTime:@(self.time.text.intValue*60).stringValue toff:@(self.period.text.intValue*60).stringValue];
    } completionBlock:^{
        [self dismissViewControllerAnimated:YES completion:^{
        }];
        [self.parentController updateData];
    }];;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
