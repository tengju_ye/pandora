//
//  ChoosePlantViewController.h
//  Pandora
//
//  Created by junchen on 12/4/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoosePlantViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
