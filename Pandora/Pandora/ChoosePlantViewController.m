//
//  ChoosePlantViewController.m
//  Pandora
//
//  Created by junchen on 12/4/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "ChoosePlantViewController.h"
#import "ItemServices.h"
#import "ItemList.h"
#import "Item.h"
#import "PlantTableViewCell.h"
#import "PlantListTableViewCell.h"
#import "SeedStateViewController.h"
#import "tools.h"
#import "UserServices.h"
#define kCellISClose 1
#define  kCellISOpen 2
@interface ChoosePlantViewController () <UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray *plantList;
@property (strong, nonatomic) NSMutableArray *cellIsOpened;

@end

@implementation ChoosePlantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"选择植物";
    [tools addRightBarItem:self];
    
    //    self.plantList = [[NSMutableArray alloc]initWithArray:[[ItemServices instance] getItemListGroupBySort:@""].data];
    
    
    [self.tableView registerNib:[UINib nibWithNibName:@"PlantTableViewCell" bundle:nil] forCellReuseIdentifier:@"plantCell"];
    //    [self.tableView registerNib:[UINib nibWithNibName:@"PlantListTableViewCell" bundle:nil] forCellReuseIdentifier:@"plantTableCell"];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"plantTableCell"];
    
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    
    
    MBProgressHUD* hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    
    [hud showAnimated:YES whileExecutingBlock:^{
        GetItemListGroupBySortResponse* response = [[ItemServices instance] getItemListGroupBySort:@""];
        NSArray* data = response.data;
        self.plantList = [data mutableCopy];
        self.cellIsOpened = [[NSMutableArray alloc] init];
        for(int i = 0; i < [self.plantList count]; i++) {
            [self.cellIsOpened addObject:@(kCellISClose)];
        }
    } completionBlock:^{
        
        [self.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.plantList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.cellIsOpened[section] intValue]== kCellISOpen) {
        ItemList* list =  [self.plantList objectAtIndex:section];
        return list.item_list.count+1;//title
    }else{
        return 1;
    }
    
    return [[self.cellIsOpened objectAtIndex:section] intValue];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ItemList *plantSort = [self.plantList objectAtIndex:[indexPath section]];
    if ([indexPath row] == 0) {
        PlantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"plantCell" forIndexPath:indexPath];
        cell.sortName.text = plantSort.sort_name;
        [cell.sortImage setImage:[UIImage imageNamed:plantSort.sort_tag]];
        return cell;
    } else {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"plantTableCell" forIndexPath:indexPath];
        
        ItemList* list =  [self.plantList objectAtIndex:indexPath.section];
        Item* item = list.item_list[indexPath.row-1];
        cell.textLabel.text = item.item_name;
        
        return cell;
        
        
        //        PlantListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"plantTableCell" forIndexPath:indexPath];
        //        cell.itemList = plantSort;
        //        cell.parentController = self;
        //        [cell.tableView reloadData];
        //        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath row] == 0) {
        if ([[self.cellIsOpened objectAtIndex:[indexPath section]] integerValue]==kCellISOpen) {
            self.cellIsOpened[indexPath.section] = @(kCellISClose);
        } else {
            self.cellIsOpened[indexPath.section] = @(kCellISOpen);
        }
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
    } else {
        __block GetUserInfoResponse* resposne = nil;
        [MBProgressHUD showAddViewWhen:self.view when:^{
            resposne =  [[UserServices instance] getUserInfo];
        } complete:^{
            if (resposne.data&&resposne.data.current_planter_id.intValue!=0) {
                [APP runBlockInMainQ:^{
                    SeedStateViewController *newController = [[SeedStateViewController alloc] init];
                    ItemList* list =  [self.plantList objectAtIndex:indexPath.section];
                    Item* item = list.item_list[indexPath.row-1];
                    newController.seedId = [item.item_id intValue];
                    newController.name = item.item_name;
                    [self.navigationController pushViewController:newController animated:YES];
                }];
            }else{
                MBProgressHUD* hud = [MBProgressHUD showAddViewWhen:self.view when:^{
                    sleep(1.0);
                } text:@"当前未绑定种植机，请先绑定种植机"];
                [hud setMode:MBProgressHUDModeText];
            }
        }  text:@"请稍等"];
        
        
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
