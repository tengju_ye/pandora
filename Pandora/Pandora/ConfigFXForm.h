//
//  ConfigFXForm.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/12.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FXForms.h"
@interface ConfigFXForm : NSObject<FXForm>
@property(nonatomic,strong) NSString* wifiName;
@property(nonatomic,strong) NSString* wifiPassWord;
@end
