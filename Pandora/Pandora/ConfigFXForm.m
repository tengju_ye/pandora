//
//  ConfigFXForm.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/12.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "ConfigFXForm.h"
#import "CustomButtonCell.h"
@implementation ConfigFXForm
-(NSArray *)fields{
    return @[@{FXFormFieldPlaceholder:@"输入要连接的无线名称",FXFormFieldType:FXFormFieldTypeText,FXFormFieldKey:@"wifiName",FXFormFieldTitle:@"无线名称"},@{FXFormFieldPlaceholder:@"输入无线密码",FXFormFieldType:FXFormFieldTypeText,FXFormFieldKey:@"wifiPassWord",FXFormFieldTitle:@"无线密码"}];
}

-(NSArray *)extraFields{
    NSMutableArray* fields = [NSMutableArray array];
    [fields addObject:
     @{FXFormFieldHeader: @"",FXFormFieldCell: [CustomButtonCell class],FXFormFieldTitle: @"提交", FXFormFieldAction: @"submit:"}
     ];
    return fields;
}

@end
