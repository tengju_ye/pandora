//
//  ConfigViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/12.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "ConfigViewController.h"
#import "FXForms.h"
#import "ConfigFXForm.h"
#import "GCDAsyncUdpSocket.h"
#import "PandoraUDP.h"
@interface ConfigViewController ()<FXFormControllerDelegate,GCDAsyncUdpSocketDelegate>{
    long tag;
    GCDAsyncUdpSocket *udpSocket;

}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) FXFormController* fxformController;
@property (nonatomic,strong) NSArray* cmds;
@property (nonatomic,assign) int currentSend;
@property (nonatomic,strong) MBProgressHUD*hud;
@end

@implementation ConfigViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (udpSocket == nil)
    {
        [self setupSocket];
    }

    [[self.navigationController view] setBackgroundColor:[UIColor whiteColor]];
    
    [self setEdgesForExtendedLayout:UIRectEdgeNone];

    self.fxformController = [[FXFormController alloc] init];
    self.fxformController.form = [ConfigFXForm new];
    self.fxformController.tableView = self.tableView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)submit:(UITableViewCell<FXFormFieldCell>*)cell{
    ConfigFXForm* form = (ConfigFXForm*)cell.field.form;
    if (form.wifiName.length==0) {
        UIAlertView* av = [[UIAlertView alloc] initWithTitle:@"wifi名不可为空" message:nil delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
        [av show];
    }else{
        [self.view addSubview:self.hud];
        [self.hud setLabelText:@"请稍等"];
        [self.hud show:YES];
        [self startSendCmd:form.wifiName pasw:form.wifiPassWord];
//
//        [self.hud showAnimated:YES whileExecutingBlock:^{
//        }];;
    }
}

- (void)setupSocket
{
    
    udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:MAIN_QUEUE];
    
    NSError *error = nil;
    
    if (![udpSocket bindToPort:0 error:&error])
    {
        return;
    }
    if (![udpSocket beginReceiving:&error])
    {
        return;
    }
    
}

-(void)startSendCmd:(NSString*)wifi pasw:(NSString*)password{
    self.currentSend = 0;
    self.cmds = @[@"123456AT+LKSTT", @"123456AT+WPRT=!0", @"123456AT+NIP=!0",
                     @"123456AT+ENCRY=!7", [NSString stringWithFormat:@"123456AT+SSID=!\"%@\"",wifi],
                     [NSString stringWithFormat:@"123456AT+KEY=!1,0,\"%@\"",password],
                     @"123456AT+ATRM=!0,0,smartplant.pandorabox.mobi,80", @"123456AT+PASS=!111222",
                     @"123456AT+Z" ];
    [self sendCMD: [self.cmds objectAtIndex:self.currentSend]];
}

- (void)sendCMD:(NSString*)msg
{
    NSString *host = @"192.168.2.1";
    if ([host length] == 0)
    {
        return;
    }
    
    int port = 988;
    if (port <= 0 || port > 65535)
    {
        return;
    }
    
    if ([msg length] == 0)
    {
        return;
    }
    
    NSData *data = [msg dataUsingEncoding:NSUTF8StringEncoding];
    [udpSocket sendData:data toHost:host port:port withTimeout:5 tag:tag];
    tag++;
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
    // You could add checks here
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
    // You could add checks here
    if (error) {
            DLog(@"%@",error);
    }
    [self.hud hide:YES];
    self.hud = nil;
    [SVProgressHUD showErrorWithStatus:@"设置失败"];

}

-(void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error{
    if (error) {
        [self.hud hide:YES];
        self.hud = nil;
        [SVProgressHUD showErrorWithStatus:@"设置失败"];
        DLog(@"%@",error);
    }
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext
{
    NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    DLog(@"%@",msg);
    if (msg)
    {
        if ([msg containsString:@"OK"]) {
            self.currentSend++;
            if (self.currentSend<self.cmds.count) {
                NSString* string = [self.cmds objectAtIndex:self.currentSend];
                [self sendCMD:string];
            }else{
                [self.hud hide:YES];
                
                self.hud = nil;
                [SVProgressHUD showSuccessWithStatus:@"设置成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }else{
            [self.hud hide:YES];
            self.hud = nil;
            [SVProgressHUD showErrorWithStatus:@"设置失败"];
        }
    }
    else
    {
        NSString *host = nil;
        uint16_t port = 0;
        [GCDAsyncUdpSocket getHost:&host port:&port fromAddress:address];
        
    }
}

-(MBProgressHUD *)hud{
    if (!_hud) {
        _hud = [[MBProgressHUD alloc] initWithView:self.view];
        [_hud setRemoveFromSuperViewOnHide:YES];
    }
    return _hud;
}

@end
