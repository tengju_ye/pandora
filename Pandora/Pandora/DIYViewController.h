//
//  DIYViewController.h
//  Pandora
//
//  Created by junchen on 12/5/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PieGraph.h"
#import "RJProgressView.h"
#import "SeedState.h"
@interface DIYViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *humidityLabel;
@property (weak, nonatomic) IBOutlet UILabel *lightLabel;
@property (weak, nonatomic) IBOutlet UISlider *temperatureBar;
@property (weak, nonatomic) IBOutlet UISlider *humidityBar;
@property (weak, nonatomic) IBOutlet UISlider *lightBar;
@property (weak, nonatomic) IBOutlet RJProgressView *temperatureCircle;
@property (weak, nonatomic) IBOutlet RJProgressView *humidityCircle;
@property (weak, nonatomic) IBOutlet RJProgressView *lightCircle;
@property (nonatomic,strong) NSString* planterSeedId;
@property (nonatomic,strong) NSString* seedId;
@property (nonatomic,strong) SeedState* initialState;
@end
