//
//  DIYViewController.m
//  Pandora
//
//  Created by junchen on 12/5/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "DIYViewController.h"
#import "PlanterSeedServices.h"
#import "ItemServices.h"
#import "MyWheelView.h"
@interface DIYViewController ()<MyWheelViewDelegate>

@property (nonatomic,strong) NSMutableArray* states;
@property (nonatomic,assign) int selectedIndex;
@property (nonatomic,assign)int tempTemperature;
@property (nonatomic,assign)int tempHumidity;
@property (nonatomic,assign)int tempLight;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *defaultBtn;

@property (nonatomic,strong) NSArray *stateNameList;
@property (weak, nonatomic) IBOutlet UIView *whellViewContainer;
@property (nonatomic,strong) MyWheelView* myWheelView;
@end

@implementation DIYViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.temperatureBar addTarget:self action:@selector(temperatureBarValueChanged) forControlEvents:UIControlEventValueChanged];
    [self.humidityBar addTarget:self action:@selector(humidityBarValueChanged) forControlEvents:UIControlEventValueChanged];
    [self.lightBar addTarget:self action:@selector(lightBarValueChanger) forControlEvents:UIControlEventValueChanged];
    
    [self.temperatureCircle setColor:RGBCOLOR(231, 34, 37)];
    [self.humidityCircle setColor:RGBCOLOR(0, 162, 233)];
    [self.lightCircle setColor:RGBCOLOR(242, 148, 57)];
    
    for (UIButton* btn in @[self.saveBtn,self.defaultBtn]) {
        [btn setBackgroundColor:DEFAULT_GREEN];
        [[btn layer] setCornerRadius:2.0];
        [btn setClipsToBounds:YES];
        [btn setTintColor:[UIColor whiteColor]];
    }
    


    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    hud.removeFromSuperViewOnHide = YES;
    

    
    [self.view addSubview:hud];
    [self.whellViewContainer addSubview:self.myWheelView];
    [hud showAnimated:YES whileExecutingBlock:^{
        [self updateData];
    } completionBlock:^{
        self.selectedIndex = self.selectedIndex;
        if (self.initialState) {
            NSInteger index = [self.states indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                SeedState* state = obj;
                return [[state state] isEqualToString:self.initialState.state];
            }];
            if (index!=NSNotFound) {
                NSMutableArray* array = [NSMutableArray array];
                [array addObjectsFromArray:[self.states subarrayWithRange:NSMakeRange(0, index)]];
                [self.states removeObjectsInRange:NSMakeRange(0, index)];
                [self.states addObjectsFromArray:array];
            }
            
        }
        [[self myWheelView] reloadData];
    }];
    
}

- (void) updateData {
    self.states = nil;
    GetSeedStateListResponse *response = [[ItemServices instance] getSeedStateList:[self.seedId intValue] is_diy:YES];
    [self.states addObjectsFromArray:response.data];
    self.selectedIndex = self.selectedIndex;
}

- (NSArray *)stateNameList {
    if(!_stateNameList) {
        _stateNameList = @[@"种子期",@"萌发期",@"幼苗期",@"根茎叶期",@"花芽期",@"开花期",@"传粉期",@"结果期",@"种子形成期"];
    }
    return _stateNameList;
}

-(void)viewDidLayoutSubviews{
    self.myWheelView.frame = self.whellViewContainer.bounds;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - light delegate
- (void)temperatureBarValueChanged{
    self.tempTemperature = self.temperatureBar.value;
}

- (void)humidityBarValueChanged{
    self.tempHumidity = self.humidityBar.value;
}

- (void)lightBarValueChanger {
    self.tempLight = self.lightBar.value;
}

- (IBAction)saveSetting:(id)sender {
    SeedState* state = self.states[self.selectedIndex];
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    hud.removeFromSuperViewOnHide = YES;
    [self.view addSubview:hud];
    [hud showAnimated:YES whileExecutingBlock:^{
        EditPlanterSeedInfoResponse* response =  [[PlanterSeedServices instance] editPlanterSeedInfo:[self.planterSeedId intValue] state:state.state.intValue seed_id:[self.seedId intValue] seed_state_id:state.seed_state_id.intValue t_num:self.tempTemperature h_num:self.tempHumidity i_num:self.tempLight];
        if ([response.code isEqualToString:OBJC_SUCCEED_CODE]) {
            [self updateData];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_SEEDSTATE_DIY_DATA object:nil];
        }else{
            [APP runBlockInMainQ:^{
                [SVProgressHUD showErrorWithStatus:@"设置失败"];
            }];
        }
    } completionBlock:^{
        self.selectedIndex = self.selectedIndex;
    }];
}


-(NSMutableArray *)states{
    if (!_states) {
        _states = [NSMutableArray array];
    }
    return _states;
}

-(MyWheelView *)myWheelView{
    if (!_myWheelView) {
        _myWheelView = [MyWheelView new];
        _myWheelView.delegate = self;
    }
    return _myWheelView;
}

-(void)setTempHumidity:(int)tempHumidity{
    _tempHumidity = tempHumidity;
    [APP runBlockInMainQ:^{
        self.humidityLabel.text = [NSString stringWithFormat:@"%d%%",tempHumidity];
        self.humidityCircle.precent = tempHumidity/self.humidityBar.maximumValue;
        self.humidityBar.value = tempHumidity;
    }];
}

-(void)setTempLight:(int)tempLight{
    _tempLight = tempLight;
    [APP runBlockInMainQ:^{
        self.lightLabel.text = [NSString stringWithFormat:@"%dlx", tempLight];
        self.lightCircle.precent = tempLight/self.lightBar.maximumValue;
        self.lightBar.value = tempLight;
    }];
}

-(void)setTempTemperature:(int)tempTemperature{
    _tempTemperature = tempTemperature;
    [APP runBlockInMainQ:^{
        self.temperatureLabel.text = [NSString stringWithFormat:@"%d℃", tempTemperature];
        self.temperatureCircle.precent = tempTemperature/self.temperatureBar.maximumValue;
        self.temperatureBar.value = tempTemperature;
    }];
}

-(void)setSelectedIndex:(int)selectedIndex{
    _selectedIndex = selectedIndex;
    SeedState* state = self.states[self.selectedIndex];
    self.tempHumidity = state.humidity.intValue;
    self.tempTemperature = state.outside_temperature.intValue;
    self.tempLight = state.illuminance_limit.intValue;
}

- (IBAction)recover:(id)sender {
    SeedState* state = self.states[self.selectedIndex];
    __block RecoverPlanterSeedStateResponse* response = nil;
    [MBProgressHUD showAddViewWhen:self.view when:^{
        response =  [[PlanterSeedServices instance] recoverPlanterSeedState:self.planterSeedId.intValue state:state.state.intValue seed_id:self.seedId.intValue];
    } complete:^{
        if ([response.code isEqualToString:OBJC_SUCCEED_CODE]) {
            state.outside_temperature = response.outside_temperature;
            state.illuminance_limit = response.illuminance_limit;
            state.humidity = response.humidity;
            self.selectedIndex = self.selectedIndex;
        }
    } text:@"重置中"];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - mywheel view
-(int)wheelViewNumofPages{
    return (int)self.states.count;
}

-(NSString*)stringOfPage:(int)page{
    SeedState* state = self.states[page];
    return  [self.stateNameList objectAtIndex:(state.state.integerValue-1)];
}

-(void)switchToPages:(int)page{
    DLog(@"%d",page);
    self.selectedIndex = page;
    
}

@end
