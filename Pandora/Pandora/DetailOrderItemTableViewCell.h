//
//  DetailOrderItemTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/5.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "OrderItem.h"
@interface DetailOrderItemTableViewCell : BaseTableViewCell
@property(nonatomic,strong)OrderItem* order;
@end
