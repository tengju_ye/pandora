//
//  DetailOrderItemTableViewCell.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/5.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "DetailOrderItemTableViewCell.h"

@interface DetailOrderItemTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *itemImage;
@property (weak, nonatomic) IBOutlet UILabel *itemName;
@property (weak, nonatomic) IBOutlet UILabel *itemPrice;
@property (weak, nonatomic) IBOutlet UILabel *itemCount;


@end

@implementation DetailOrderItemTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setOrder:(OrderItem *)order{
    _order = order;
    [self.itemImage sd_setImageWithURL:[NSURL URLWithString:_order.small_pic relativeToURL:[NSURL URLWithString:OBJC_IMGBASE_URL]]];
    [self.itemPrice setText:_order.real_price];
    [self.itemName setText:_order.item_name];
    [self.itemCount setText:[NSString stringWithFormat:@"%@件",_order.number]];
}

@end
