//
//  DetailOrderTableViewController.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/5.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
@interface DetailOrderTableViewController : UITableViewController
@property(nonatomic,strong)Order* order;
@end
