//
//  DetailOrderTableViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/5.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "DetailOrderTableViewController.h"
#import "DetailOrderTableViewCell.h"
#import "OrderServices.h"
#import "DetailOrderItemTableViewCell.h"
#import "OrderCancelTableViewCell.h"
#import "UIAlertView+MyBlocks.h"
#import "PayOrderViewController.h"
static NSString* const SummaryIdentifier = @"SummaryIdentifier";
static NSString* const ItemIdentifier = @"ItemIdentifier";
static NSString* const BtnIdentifier = @"BtnIdentifier";

@interface DetailOrderTableViewController ()<OrderCancelTableViewCellDelegate>
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerOrderStateLbl;
@property (weak, nonatomic) IBOutlet UILabel *headerOrderNumber;

@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UILabel *footerPrice;

@property (strong, nonatomic) IBOutlet UIView *section2Header;
@property (weak, nonatomic) IBOutlet UILabel *section2CountLabel;

#define Item_Key_Name @"Name"
#define Item_Key_Value @"Value"
#define Item_Key_PropertyName @"PropertyName"

@property (nonatomic,strong) NSMutableArray* summaryDatasource;
@property (nonatomic,strong) GetOrderInfoResponse* response;
@end

@implementation DetailOrderTableViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    if (self = [super initWithNibName:nibNameOrNil bundle:nil]) {
        [self setHidesBottomBarWhenPushed:YES];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"订单详情";
    _summaryDatasource = [NSMutableArray array];
    
    [self.tableView registerNib:[DetailOrderTableViewCell nib] forCellReuseIdentifier:SummaryIdentifier];
    [self.tableView registerNib:[OrderCancelTableViewCell nib] forCellReuseIdentifier:BtnIdentifier];
    [self.tableView registerNib:[DetailOrderItemTableViewCell nib] forCellReuseIdentifier:ItemIdentifier];
    CGRect r = self.headerView.frame;
    r.size.height = 80;
    self.headerView.frame = r;
    [self.tableView setTableHeaderView:self.headerView];
    
    r = self.footerView.frame;
    r.size.height = 40;
    self.footerView.frame = r;
    [self.tableView setTableFooterView:self.footerView];
    [MBProgressHUD showAddViewWhen:self.view when:^{
        [self webGetOrderDetail];
    } complete:^{
        [self.tableView reloadData];
    }  text:@"请稍等"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        //去支付或取消订单
        if (self.order.order_status.integerValue==Order_Status_Prepay) {
            return 1;
        }else{
            return 0;
        }
    }else if(section==1){
        //订单详情
        return self.summaryDatasource.count;
    }else{
        //商品小记
        return self.response.order_item_list.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section==0) {
        OrderCancelTableViewCell* cancel = [tableView dequeueReusableCellWithIdentifier:BtnIdentifier];
        cancel.delegate = self;
        return cancel;
    }else if (indexPath.section==1) {
        DetailOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SummaryIdentifier forIndexPath:indexPath];
        NSDictionary* dict = [self.summaryDatasource objectAtIndex:indexPath.row];
        cell.itemDetailLabel.text = dict[Item_Key_Value];
        cell.itemNameLabel.text = dict[Item_Key_Name];
        return cell;
    }else{
        DetailOrderItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ItemIdentifier forIndexPath:indexPath];
        OrderItem* item = [self.response.order_item_list objectAtIndex:indexPath.row];
        [cell setOrder:item];
        return cell;
    }
    
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==2) {

        self.section2CountLabel.text = [NSString stringWithFormat:@"共%d件",self.response.order_item_list.count];
        return self.section2Header;
    }else{
        return [UIView new];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section==2) {
        //summary section
        return 19.0;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            return 54.0;
            break;
        case 1:
            return 30.0;
            break;
        default:
            return 84.0;
            break;
    }
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Table view delegate
 
 // In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 // Navigation logic may go here, for example:
 // Create the next view controller.
 <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
 
 // Pass the selected object to the new view controller.
 
 // Push the view controller.
 [self.navigationController pushViewController:detailViewController animated:YES];
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - WebGetOrderDetail
-(void)webGetOrderDetail{
    GetOrderInfoResponse* response =  [[OrderServices instance] getOrderInfo:self.order.order_id];
    self.response = response;
}
-(void)setResponse:(GetOrderInfoResponse *)response{
    _response = response;
    
    if (_response!=nil) {
        [APP runBlockInMainQ:^{
            
            
            self.headerOrderNumber.text = _response.order_sn;
            self.headerOrderStateLbl.text = _response.order_status_name;
            self.footerPrice.text = [@"¥ " stringByAppendingString:_response.pay_amount];
            
            ;
            if (_response.shop_name == nil) {
                _response.shop_name = @"-";
            }
            [self.summaryDatasource addObject:@{Item_Key_Name:@"供货商",Item_Key_Value:_response.shop_name}];
            [self.summaryDatasource addObject:@{Item_Key_Name:@"总额",Item_Key_Value:[NSString stringWithFormat:@"¥%.2f",_response.pay_amount.doubleValue]}];
            
            if (_response.shop_name == nil) {
                _response.shop_name = @"-";
            }
            [self.summaryDatasource addObject:@{Item_Key_Name:@"收货地址",Item_Key_Value:_response.area_string}];
            
            
            if (_response.consignee == nil) {
                _response.consignee = @"-";
            }
            [self.summaryDatasource addObject:@{Item_Key_Name:@"收货人",Item_Key_Value:_response.consignee}];
            
            if (_response.addtime == nil && _response.addtime.doubleValue!=0) {
                _response.addtime = @"-";
            }else{
                _response.addtime = [APP dateStringWithDefaultFormatForStamp:_response.addtime.doubleValue];
            }
            [self.summaryDatasource addObject:@{Item_Key_Name:@"下单时间",Item_Key_Value:_response.addtime}];
            
            if (_response.pay_time == nil && _response.pay_time.doubleValue!=0) {
                _response.pay_time = @"-";
            }else{
                _response.pay_time = [APP dateStringWithDefaultFormatForStamp:_response.pay_time.doubleValue];
            }
            [self.summaryDatasource addObject:@{Item_Key_Name:@"支付时间:",Item_Key_Value:_response.pay_time}];
            
            if (_response.payway_name == nil) {
                _response.payway_name = @"-";
            }
            [self.summaryDatasource addObject:@{Item_Key_Name:@"支付方式:",Item_Key_Value:_response.payway_name}];
            
            if (_response.express_number == nil) {
                _response.express_number = @"-";
            }
            [self.summaryDatasource addObject:@{Item_Key_Name:@"物流单号:",Item_Key_Value:_response.express_number}];
            
            if (_response.express_company_name == nil) {
                _response.express_company_name = @"-";
            }
            [self.summaryDatasource addObject:@{Item_Key_Name:@"物流公司:",Item_Key_Value:_response.express_company_name}];
            
            
            [self.summaryDatasource addObject:@{Item_Key_Name:@"物流费用:",Item_Key_Value:[NSString stringWithFormat:@"¥%@",_response.express_fee]}];
            
            if (_response.user_remark == nil) {
                _response.user_remark = @"-";
            }
            [self.summaryDatasource addObject:@{Item_Key_Name:@"用户备注:",Item_Key_Value:_response.user_remark}];
        }];
    }
    
}


#pragma mark - cell delegate
-(void)didTouchCancel:(OrderCancelTableViewCell *)cell{
    //取消订单
    [UIAlertView showAlertViewWithTitle:@"确实取消订单吗" message:nil cancelButtonTitle:@"不取消" otherButtonTitles:@[@"取消"] onDismiss:^(int buttonIndex, UIAlertView *alertView) {
        if (buttonIndex==0) {
            [MBProgressHUD showAddViewWhen:self.view when:^{
                CancelOrderResponse* response = [[OrderServices instance] cancelOrder:self.order.order_id];
                if ([response.code isEqualToString:OBJC_SUCCEED_CODE]) {
                    [APP runBlockInMainQ:^{
                        [SVProgressHUD showSuccessWithStatus:@"取消成功"];
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }else{
                    [APP runBlockInMainQ:^{
                        [SVProgressHUD showSuccessWithStatus:@"取消失败"];
                    }];
                }
                
            } complete:^{
                
            } text:@"正在取消中"];
            
        }
    } onCancel:^{
        
    }];
}

-(void)didTouchPay:(OrderCancelTableViewCell *)cell{
    PayOrderViewController* pvc = [[PayOrderViewController alloc] initWithNibName:[PayOrderViewController nibName] bundle:nil];
    pvc.orderID = self.order.order_id;
    [self.navigationController pushViewController:pvc animated:YES];
}

@end
