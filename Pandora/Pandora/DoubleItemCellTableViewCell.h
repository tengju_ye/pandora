//
//  DoubleItemCellTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "Item.h"

@class DoubleItemCellTableViewCell;
@protocol DoubleItemTableViewCellDelegate<NSObject>
-(void)doubleItemCell:(DoubleItemCellTableViewCell*)cell didClickItem:(Item*)item;
@end

@interface DoubleItemCellTableViewCell : BaseTableViewCell
@property(nonatomic,strong)Item* item1;
@property(nonatomic,strong)Item* item2;
@property(nonatomic,strong)id<DoubleItemTableViewCellDelegate> delegate;
@end
