//
//  DoubleItemCellTableViewCell.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "DoubleItemCellTableViewCell.h"
#import "UIButton+WebCache.h"

@interface DoubleItemCellTableViewCell()
@property (weak, nonatomic) IBOutlet UIView *item1View;
@property (weak, nonatomic) IBOutlet UIView *item2View;

@property (weak, nonatomic) IBOutlet UIButton *item1ImageBtn;
@property (weak, nonatomic) IBOutlet UIButton *item2ImageBtn;

@property (weak, nonatomic) IBOutlet UILabel *item1TitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *item2TitleLbl;

@property (weak, nonatomic) IBOutlet UILabel *item1PriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *item2PriceLbl;

@property (weak, nonatomic) IBOutlet UILabel *item1SellLbl;
@property (weak, nonatomic) IBOutlet UILabel *item2SellLbl;

@end
@implementation DoubleItemCellTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.item1SellLbl.hidden = YES;
    self.item2SellLbl.hidden = YES;
    
    [self.item1ImageBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.item2ImageBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setItem1:(Item *)item1{
    _item1 = item1;
    if (_item1) {
        _item1View.hidden = NO;
        //img
        NSURL* url = [NSURL URLWithString:_item1.base_pic relativeToURL:[NSURL URLWithString:OBJC_IMGBASE_URL]];
        
        [self.item1ImageBtn sd_setImageWithURL:url forState:UIControlStateNormal];

        self.item1TitleLbl.text = _item1.item_name;
        [self.item1SellLbl setText:[NSString stringWithFormat:@"销量:%@",_item1.sales_num]];
        
        NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithString:@"价格: "];
        [string appendAttributedString:[[NSAttributedString alloc] initWithString:_item1.mall_price attributes:@{NSForegroundColorAttributeName:[UIColor redColor]}]];
        [self.item1PriceLbl setAttributedText:string];
        
    }else{
        _item1View.hidden = YES;
    }
}

-(void)setItem2:(Item *)item2{
    _item2 = item2;
    if (_item2) {
        _item2View.hidden = NO;
        //img
        NSURL* url = [NSURL URLWithString:_item2.base_pic relativeToURL:[NSURL URLWithString:OBJC_IMGBASE_URL]];
        [self.item2ImageBtn sd_setImageWithURL:url forState:UIControlStateNormal];
        self.item2TitleLbl.text = _item2.item_name;
        [self.item2SellLbl setText:[NSString stringWithFormat:@"销量:%@",_item2.sales_num]];
        
        NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithString:@"价格: "];
        [string appendAttributedString:[[NSAttributedString alloc] initWithString:_item2.mall_price attributes:@{NSForegroundColorAttributeName:[UIColor redColor]}]];
        [self.item2PriceLbl setAttributedText:string];
        
    }else{
        _item2View.hidden = YES;
    }
}


-(void)btnClick:(id)sender{
    if ([self.delegate respondsToSelector:@selector(doubleItemCell:didClickItem:)]) {
        if (sender==self.item1ImageBtn) {
            [self.delegate doubleItemCell:self didClickItem:self.item1];
        }else{
            [self.delegate doubleItemCell:self didClickItem:self.item2];
        }

    }
}
@end
