//
//  EditPlanterNameTableViewCell.h
//  Pandora
//
//  Created by junchen on 12/3/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwitchPlanterViewController.h"
@interface EditPlanterNameTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *nameField;

@property int planter_id;
@property (weak, nonatomic) SwitchPlanterViewController *parentController;

@end
