//
//  EditPlanterNameTableViewCell.m
//  Pandora
//
//  Created by junchen on 12/3/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "EditPlanterNameTableViewCell.h"
#import "PlanterServices.h"

@implementation EditPlanterNameTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)editName:(id)sender {
    if ([self.nameField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"请输入新名字" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
    } else {
        [[PlanterServices instance] setPlanterName:self.planter_id planter_name:self.nameField.text];
        [self.parentController initDataSource];
        [self.parentController.tableView reloadData];
    }
}

@end
