//
//  FindPassFXForm.h
//  Pandora
//
//  Created by 小南宫的mac on 15/1/17.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXForms.h"

@interface FindPassFXForm : NSObject<FXForm>
@property (nonatomic,strong) NSString* mobile;
@property (nonatomic,strong) NSString* verifyCode;
@property (nonatomic,strong) NSString* newpassword;
@end
