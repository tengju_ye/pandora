//
//  FindPassFXForm.m
//  Pandora
//
//  Created by 小南宫的mac on 15/1/17.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "FindPassFXForm.h"
#import "CustomButtonCell.h"
@implementation FindPassFXForm

-(NSArray *)fields{
    return @[
             @{FXFormFieldPlaceholder:@"输入手机号",FXFormFieldType:FXFormFieldTypeText,FXFormFieldKey:@"mobile",FXFormFieldTitle:@"手机号"},
             @{FXFormFieldTitle:@"获取验证码",FXFormFieldAction:@"getVerifyCode:",FXFormFieldCell:[CustomButtonCell class],FXFormFieldKey:@"getVerify"},
             
             @{FXFormFieldTitle:@"验证码",FXFormFieldKey:@"verifyCode",FXFormFieldPlaceholder:@"输入验证码",FXFormFieldType:FXFormFieldTypeText}
             ];
}

-(NSDictionary*)getVerifyField{
    return @{@"titleString":@"获取验证码"};
}

-(NSArray *)extraFields{
    
    return @[
             @{FXFormFieldHeader:@"",FXFormFieldPlaceholder:@"输入新密码",FXFormFieldType:FXFormFieldTypeText,FXFormFieldKey:@"newpassword",FXFormFieldTitle:@"新密码"},
             
             @{FXFormFieldAction:@"submit:",FXFormFieldCell:[CustomButtonCell class]}
             
             ];
}

@end
