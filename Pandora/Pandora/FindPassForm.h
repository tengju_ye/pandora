//
//  FindPassForm.h
//  Pandora
//
//  Created by 小南宫的mac on 15/1/17.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "DefaultForm.h"
#define APINAME_FindPassWord @"plant.user.findPassword"

@interface FindPassForm : DefaultForm
@property ( nonatomic , strong ) NSString* verify_code;
@property ( nonatomic , strong ) NSString* password;
@end
