//
//  FindPassForm.m
//  Pandora
//
//  Created by 小南宫的mac on 15/1/17.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "FindPassForm.h"

@implementation FindPassForm
-(instancetype)init{
    if (self = [super init]) {
        self.api_name = APINAME_FindPassWord;
    }
    return self;
}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"new_password": @"password"}];
}

@end
