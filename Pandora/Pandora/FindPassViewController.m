//
//  FindPassViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 15/1/17.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "FindPassViewController.h"
#import "FindPassFXForm.h"
#import "UserServices.h"
@interface FindPassViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) FXFormController* fxformController;
@end

@implementation FindPassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fxformController = [[FXFormController alloc] init];
    self.fxformController.form = [FindPassFXForm new];
    self.fxformController.tableView = self.tableView;

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getVerifyCode:(UITableViewCell<FXFormFieldCell>*)cell{
    FindPassFXForm* form = (FindPassFXForm*)cell.field.form;
    if (form.mobile.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入手机号"];
        return;
    }
    
    [MBProgressHUD showAddViewWhen:self.view when:^{
        [[UserServices instance] sendVerifyCode:form.mobile];
    } text:@"获取验证码中"];
    
}

-(void)submit:(UITableViewCell<FXFormFieldCell>*)cell{
    FindPassFXForm* form = (FindPassFXForm*)cell.field.form;
    if (form.verifyCode.length==0) {
        [SVProgressHUD showErrorWithStatus:@"请获取验证码"];
        return;
    }
    
    [MBProgressHUD showAddViewWhen:self.view when:^{
        FindPassResponse* response =  [[UserServices instance] findPassword:[form.newpassword md5] verifyCode:form.verifyCode];
        if ([response.code isEqualToString:OBJC_SUCCEED_CODE]) {
            [APP runBlockInMainQ:^{
                [SVProgressHUD showSuccessWithStatus:@"重置密码成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }else{
            [APP runBlockInMainQ:^{
                [SVProgressHUD showSuccessWithStatus:@"重置密码失败，请重试"];
            }];
        }
    } text:@"获取验证码中"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
