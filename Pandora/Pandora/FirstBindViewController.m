//
//  FirstBindViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 15/1/20.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "FirstBindViewController.h"
#import "tools.h"
#import "RightMenuViewController.h"
@interface FirstBindViewController ()
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;

@end

@implementation FirstBindViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"首次绑定";
    [[self.navigationController view] setBackgroundColor:[UIColor whiteColor]];
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    [tools addRightBarItem:self];
    if (self.username) {
        self.welcomeLabel.text = [NSString stringWithFormat:@"欢迎你,%@",self.username];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)initPandora:(id)sender {
    [(RightMenuViewController*)[[self mm_drawerController] rightDrawerViewController] selectedIdx:kInitialIdx];
}

- (IBAction)bindPandora:(id)sender {
    [(RightMenuViewController*)[[self mm_drawerController] rightDrawerViewController] selectedIdx:kBindNewPlanterIdx];
}

- (IBAction)switchToMall:(id)sender {
    [(RightMenuViewController*)[[self mm_drawerController] rightDrawerViewController] selectedIdx:kMallIdx];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
