//
//  HelpInfoViewController.h
//  Pandora
//
//  Created by junchen on 12/3/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpInfoViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) NSString *htmlString;
@property (weak, nonatomic) NSString *helpTitle;
@property (weak, nonatomic) NSString *time;

@end
