//
//  HelpInfoViewController.m
//  Pandora
//
//  Created by junchen on 12/3/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "HelpInfoViewController.h"
#import "tools.h"
@interface HelpInfoViewController ()

@end

@implementation HelpInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.titleLabel setText:self.helpTitle];
    [self.timeLabel setText:self.time];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.time.doubleValue];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *time = [formatter stringFromDate:date];
    NSString* imgStyle = [NSString stringWithFormat:@"<style>img{max-width:%fpx}</style>",CGRectGetWidth(self.view.frame) - 10];
    NSString* html = [NSString stringWithFormat:@"%@\n<p>%@</p>\n<p>%@</p>%@",imgStyle, self.helpTitle, time, self.htmlString];
    [self.webView loadHTMLString:html baseURL:[NSURL URLWithString:@"http://smartplant.pandorabox.mobi"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
