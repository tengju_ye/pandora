//
//  HelpTableViewController.m
//  Pandora
//
//  Created by junchen on 12/2/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "HelpTableViewController.h"
#import "HelpServices.h"
#import "APIHeader.h"
#import "HelpInfoViewController.h"
#import "tools.h"
@interface HelpTableViewController ()

@property (nonatomic, strong) NSArray<Help>* dataSource;

@end

@implementation HelpTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [tools addRightBarItem:self];
    self.title = @"帮助";
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"CellIdentifier"];
    self.tableView.tableFooterView = [[UIView alloc] init];
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(NSArray *)dataSource
{
    if (!_dataSource) {
        GetHelpListResponse* response = [[HelpServices instance] getHelpList];
        if([response.code isEqualToString:OBJC_SUCCEED_CODE]) {
            _dataSource = response.data;
        }
    }
    return _dataSource;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.dataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[self.dataSource objectAtIndex:section] article_list] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier" forIndexPath:indexPath];
    Article *article = [[[self.dataSource objectAtIndex:[indexPath section]] article_list] objectAtIndex:[indexPath row]];
    [cell.textLabel setText:article.title];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [[self.dataSource objectAtIndex:section] help_sort_name];
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(10, 0, tableView.bounds.size.width, 30);
    label.text = sectionTitle;
    UIView *sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 22)];
    [sectionView addSubview:label];
    return sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Help *help = [self.dataSource objectAtIndex:[indexPath section]];
    Article *article = [help.article_list objectAtIndex:[indexPath row]];
    int help_id = [article.help_id intValue];
    GetHelpInfoResponse* response = [[HelpServices instance] getHelpInfo:help_id];
    HelpInfoViewController *newControler = [[HelpInfoViewController alloc] init];
    newControler.htmlString = response.help_contents;
    newControler.helpTitle = response.title;
    newControler.time =  response.addtime;
    NSLog(@"%@", response.help_contents);
    [self.navigationController pushViewController:newControler animated:YES];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
