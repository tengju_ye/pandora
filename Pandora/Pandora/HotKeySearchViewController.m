//
//  HotKeySearchViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/4.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "HotKeySearchViewController.h"
#import "Item.h"
#import "MallServices.h"
#import "ItemRevealController.h"
#import "HotKeyCollectionViewCell.h"
#import "SearchResultTableViewController.h"
#import "tools.h"
@interface HotKeySearchViewController ()<UISearchBarDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) NSArray* hotkeyDatasource;

@end
static NSString* const CollectionIdentifier = @"CollectionIdentifier";

@implementation HotKeySearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchBar.delegate = self;
    [self configViews];
    [tools changeSearchCancelText:self.searchBar];
    self.title = @"搜索商品";
}

-(void)configViews{
    
    [[self.navigationController view] setBackgroundColor:[UIColor whiteColor]];
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"HotKeyCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:CollectionIdentifier];
    
    [MBProgressHUD showAddViewWhen:self.view when:^{
        GetHotKeywordsResponse* response = [[MallServices instance] getHotKeywords:0];
        self.hotkeyDatasource = [response.data componentsSeparatedByString:@","];
    } complete:^{
        [self.collectionView reloadData];
    } text:@"加载中"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - collectionview
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.hotkeyDatasource.count;
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    SearchResultTableViewController* srtvc = [SearchResultTableViewController getInstanceWithSameNibName];
    srtvc.item_name = self.hotkeyDatasource[indexPath.row];
    [self.navigationController pushViewController:srtvc animated:YES];

}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HotKeyCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:CollectionIdentifier forIndexPath:indexPath];
    cell.label.text = self.hotkeyDatasource[indexPath.row];
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}


#pragma mark - search bar delegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    SearchResultTableViewController* srtvc =  [SearchResultTableViewController getInstanceWithSameNibName];
    srtvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:srtvc animated:YES];
    srtvc.item_name = searchBar.text;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    searchBar.text = nil;
}


@end
