//
//  HotKeyTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/4.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "Item.h"
@interface HotKeyTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *salesLabel;
@property (weak, nonatomic) IBOutlet UIImageView *comImageView;
@property (nonatomic,strong) Item* item;
@end
