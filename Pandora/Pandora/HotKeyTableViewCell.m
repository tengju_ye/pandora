//
//  HotKeyTableViewCell.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/4.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "HotKeyTableViewCell.h"

@implementation HotKeyTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setItem:(Item *)item{
    _item = item;
    self.titleLabel.text = item.item_name;
    self.priceLabel.text = [@"¥ " stringByAppendingString:item.mall_price];
    
    self.salesLabel.text = [@"销量: " stringByAppendingString:_item.sales_num];
    [self.comImageView sd_setImageWithURL:[NSURL URLWithString:_item.small_img relativeToURL:[NSURL URLWithString:OBJC_IMGBASE_URL]]];
    
}

@end
