
#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import "Reachability.h"
#import "NSString+Extension.h"
#import <UIKit/UIKit.h>

@interface NSString (HttpManager)
- (NSString *)md5;
- (NSString *)encode;
- (NSString *)decode;
- (NSString *)object;
@end

@interface NSObject (HttpManager)
- (NSString *)json;
@end

@interface HttpManager : NSObject

+ (HttpManager *)defaultManager;
+(void)deleteManager;
/*  -------判断当前的网络类型----------
 1、NotReachable     - 没有网络连接
 2、ReachableViaWWAN - 移动网络(2G、3G)
 3、ReachableViaWiFi - WIFI网络
 */
+ (NetworkStatus)networkStatus;

typedef void(^HttpManagerCompleteBlock)(BOOL successed, NSDictionary * result);


//GET 请求
//- (void)getRequestToUrl:(NSString *)url params:(NSDictionary *)params complete:(void (^)(BOOL successed, NSDictionary *result))complete;
//- (void)getRequestToUrlSynchornize:(NSString *)url params:(NSDictionary *)params complete:(void (^)(BOOL successed, NSDictionary *result))complete;
//POST 请求
- (void)postRequestToUrl:(NSString *)url params:(NSDictionary *)params complete:(void (^)(BOOL successed, id result,NSURLResponse* fullResponse))complete;

-(void)postRequestToUrlSynchornize:(NSString *)url params:(NSDictionary *)params complete:(void (^)(BOOL successed,id result,NSURLResponse* fullResponse))complete;

- (void)downloadFromUrl:(NSString *)url
                                   complete:(void (^)(BOOL successed, UIImage* image))complete;
//特殊处理的post请求
-(void)postSecretRequest:(NSString *)url model:(NSDictionary *)dict complete:(void (^)(BOOL succeed, id result,NSURLResponse* fullResponse))complete sync:(BOOL)sync;

-(void)postSecretRequest:(NSString *)url model:(NSDictionary *)dict complete:(void (^)(BOOL succeed, id result,NSURLResponse* fullResponse))complete sync:(BOOL)sync containPHPSESSIONID:(BOOL)contain;


@end
