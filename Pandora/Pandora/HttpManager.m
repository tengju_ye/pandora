//
//  HttpManager.m
//  zhuxin
//
//  Created by dave ding on 14-1-17.
//  Copyright (c) 2014年 dave ding. All rights reserved.
//

#import "HttpManager.h"
#import "NSString+Extension.h"
#import "Reachability.h"
#import "APP.h"
#import "SVProgressHUD.h"
#import "APIHeader.h"
#import "LoginForm.h"
#import "LoginResponse.h"
#import "UserServices.h"
#import "LoginViewController.h"
@interface HttpManager()
@property(nonatomic,strong) NSOperationQueue* queue;
@end

@implementation NSString (HttpManager)
- (NSString *)md5
{
    if(self == nil || [self length] == 0){
        return nil;
    }
    const char *value = [self UTF8String];
    
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, (CC_LONG)strlen(value), outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++){
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    }
    
    return outputString;
}
- (NSString *)encode
{
    NSString *outputStr = (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                              (CFStringRef)self,
                                                              NULL,
                                                              NULL,
                                                              kCFStringEncodingUTF8));
    return outputStr;
}
- (NSString *)decode
{
    NSMutableString *outputStr = [NSMutableString stringWithString:self];
    [outputStr replaceOccurrencesOfString:@"+" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [outputStr length])];
    return [outputStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}
- (id)object
{
    id object = nil;
    @try {
        NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];;
        object = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    }
    @catch (NSException *exception) {
        NSLog(@"%s [Line %d] JSON字符串转换成对象出错了-->\n%@",__PRETTY_FUNCTION__, __LINE__,exception);
    }
    @finally {
    }
    return object;
}
@end
@implementation NSObject (HttpManager)
- (NSString *)json
{
    NSString *jsonStr = @"";
    @try {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:0 error:nil];
        jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    @catch (NSException *exception) {
        NSLog(@"%s [Line %d] 对象转换成JSON字符串出错了-->\n%@",__PRETTY_FUNCTION__, __LINE__,exception);
    }
    @finally {
    }
    return jsonStr;
}
@end

@implementation HttpManager

- (id)init{
    self = [super init];
    if (self) {
    }
    return self;
}

__strong static id defaultHttpManager = nil;
static dispatch_once_t pred = 0;

+ (HttpManager *)defaultManager
{
    dispatch_once( &pred, ^{
        
        defaultHttpManager = [[self alloc] init];
    });
    return defaultHttpManager;
}

-(void)getRequestToUrlSynchornize:(NSString *)url params:(NSDictionary *)params complete:(void (^)(BOOL, NSDictionary *))complete{
    params = [[HttpManager getRequestBodyWithParams:params] copy];
    //compose url request
    NSString*get = @"?";
    NSArray* keys = [params allKeys];
    for (int i=0; i<[keys count]; i++) {
        NSString* key =keys[i];
        get = [get stringByAppendingFormat:@"%@=%@",key,params[key]];
        if (i!=keys.count-1) {
            get = [get stringByAppendingString:@"&"];
        }
    }
    url = [url stringByAppendingString:get];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    //add cookie
    
    [request setHTTPMethod:@"GET"];
    
    NSURLResponse* response;
    NSError* error;
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error) {
        NSString* errstr = [error description];
        if (errstr) {
            NSDictionary* dict = @{@"message":errstr};
            complete(NO,dict);
        }
    }else{
        NSString* string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        if (string) {
            //        [self checkStatus:[string jsonValue]];
            complete(YES,[string jsonValue]);
        }
    }
}

-(void)postRequestToUrl:(NSString *)url params:(NSDictionary *)params complete:(void (^)(BOOL, id, NSURLResponse *))complete
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    
    //add post data
    NSString*post = @"";
    NSArray* keys = [params allKeys];
    for (int i=0; i<[keys count]; i++) {
        NSString* key =keys[i];
        post = [post stringByAppendingFormat:@"%@=%@",key,params[key]];
        if (i!=keys.count-1) {
            post = [post stringByAppendingString:@"&"];
        }
    }
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:self.queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (connectionError) {
            NSString* errstr = [connectionError description];
            if (errstr) {
                NSDictionary* dict = @{@"message":errstr};
                dispatch_async(MAIN_QUEUE, ^{
                    [SVProgressHUD showErrorWithStatus:errstr];
                });
                complete(NO,dict,response);
            }
        }else{
            NSString* string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSDictionary* dict = [string jsonValue];
            BOOL validDict = [self checkValidationOfPardoraDict:dict];
            complete(validDict,dict,response);
        }
    }];
    
}

-(void)postRequestToUrlSynchornize:(NSString *)url params:(NSDictionary *)params complete:(void (^)(BOOL, id, NSURLResponse *))complete{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    
    //add post data
    NSString*post = @"";
    NSArray* keys = [params allKeys];
    for (int i=0; i<[keys count]; i++) {
        NSString* key =keys[i];
        post = [post stringByAppendingFormat:@"%@=%@",key,params[key]];
        if (i!=keys.count-1) {
            post = [post stringByAppendingString:@"&"];
        }
    }
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
    NSURLResponse* response;
    NSError* error;
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error) {
        NSString* errstr = [error description];
        if (errstr) {
            NSDictionary* dict = @{@"message":errstr};
            dispatch_async(MAIN_QUEUE, ^{
                [SVProgressHUD showErrorWithStatus:@"网络异常"];
            });
            complete(NO,dict,response);
        }
    }else{
        NSString* string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        DLog(@"%@",string);
        if (string) {
            NSDictionary* dict = [string jsonValue];
            BOOL validDict = [self checkValidationOfPardoraDict:dict];
            complete(validDict,dict,response);
        }
    }
    
}


//special codes
#define INVALID_SESSION_ID 40011
#define NON_VALID_PLANTER 40012
#define INVALID_USERNAME_LENGTH 40004
#define INVALID_PASSWORD_LENGTH 40005
#define INVALID_MOBILE_LENGTH 40006
#define INVALID_NICKNAME_LENGTH 40007
#define INVALID_USERNAME 41004
#define INVALID_PASSWORD 41005
#define INVALID_MOBILE 41006
#define INVALID_NICKNAME 41007
#define MISSING_USERNAME 44004
#define MISSING_PASSWORD 44005
#define MISSING_MOBILE 44006
#define MISSING_NICKNAME 44007
#define MISSING_REALNAME 44038
#define MISSING_MOBILE2 44039


-(BOOL)checkValidationOfPardoraDict:(NSDictionary*)dict{
    BOOL tf = NO;
    if ([[dict objectForKey:@"code"] intValue]!=0) {
        //invalid session
        void(^afterBlock)(void) = nil;//block after showing the message;
        if ([[dict objectForKey:@"code"]intValue]==INVALID_SESSION_ID) {
            //not show message but relogin
            dispatch_async(MAIN_QUEUE, ^{
                [SVProgressHUD showWithStatus:@"登录失效，请稍等,正在重新登录" maskType:SVProgressHUDMaskTypeGradient];
                
                [APP runBlockInInitialQ:^{
                    //login
                    NSString* username = [[APP instance] username];
                    NSString* password = [[APP instance] password];
                    
                    LoginResponse* response = [[UserServices instance] login:username password:password verify_code:nil];
                    //如果成功重新登录，则要求用户重新提交信息
                    if ([response.code isEqualToString:OBJC_SUCCEED_CODE]) {
                        [APP runBlockInMainQ:^{
                            [[APP instance] setSESSIONID:response.phpsessid];
                            [SVProgressHUD showSuccessWithStatus:@"重新登录成功，请重新提交请求"];
                            [SVProgressHUD dismiss];
                        }];
                    }else{
                        //否则强制退出
                        [[APP instance] setSESSIONID:nil];
                        [APP runBlockInMainQ:^{
                            [SVProgressHUD showErrorWithStatus:@"获取sessionid失败，请重新登录"];
                            UIViewController* controlelr = [[[[UIApplication sharedApplication] windows] firstObject] rootViewController];
                            if (![controlelr isKindOfClass:[LoginViewController class]]&&![controlelr isKindOfClass:[RegisterTableViewController class]]) {
                                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                UINavigationController* loginNVC = [storyboard instantiateViewControllerWithIdentifier:STORYBOARD_ID_LOGIN_NVC];

                                [controlelr presentViewController:loginNVC animated:YES completion:nil];
                            }
                        }];

                    }
                }];
            });
        }else{
            dispatch_async(MAIN_QUEUE, ^{
                switch ([dict[@"code"] intValue]) {
                    case NON_VALID_PLANTER:
                        //当前未绑定种植机器
                    {
                        UIWindow* window = [[[UIApplication sharedApplication] windows] firstObject];
                        MBProgressHUD* hud = [MBProgressHUD HUDForView:window];
                        if (!hud) {
                            hud = [[MBProgressHUD alloc] initWithView:window];
                        }
                        [hud setRemoveFromSuperViewOnHide:YES];
                        [hud setLabelText:[dict objectForKey:@"error_msg"]];
                        [hud setMode:MBProgressHUDModeText];
                        [window addSubview:hud];
                        [hud showAnimated:YES whileExecutingBlock:^{
                            sleep(1.0);
                        }];
                    }
                        break;
                    case INVALID_USERNAME_LENGTH :
                    case INVALID_PASSWORD_LENGTH :
                    case INVALID_MOBILE_LENGTH :
                    case INVALID_NICKNAME_LENGTH :
                    case INVALID_USERNAME :
                    case INVALID_PASSWORD :
                    case INVALID_MOBILE :
                    case INVALID_NICKNAME :
                    case MISSING_USERNAME :
                    case MISSING_PASSWORD :
                    case MISSING_MOBILE :
                    case MISSING_NICKNAME :
                    case MISSING_REALNAME :
                    case MISSING_MOBILE2 :{
                        NSDictionary* specificDict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ERRORINFO" ofType:@"plist"]];
                        NSString* errorCode = [dict[@"code"] stringValue];
                        NSString* errorStr = specificDict[errorCode];
                        [SVProgressHUD showErrorWithStatus:errorStr];
                        break;
                    }

                    default:
                        [SVProgressHUD showErrorWithStatus:[dict objectForKey:@"error_msg"]];
                        break;
                }
                if (afterBlock) {
                    afterBlock();
                }
            });
        }

        
    }else{
        tf = YES;
    }
    return tf;
}

#define SECRET  @"@J4*A9N7&B^A9Y7j6sWv8m5%q_p+z-h="
-(void)postSecretRequest:(NSString *)url model:(NSDictionary *)dict complete:(void (^)(BOOL, id, NSURLResponse *))complete sync:(BOOL)sync{

    [self postSecretRequest:url model:dict complete:complete sync:sync containPHPSESSIONID:YES];

}

-(void)postSecretRequest:(NSString *)url model:(NSDictionary *)dict complete:(void (^)(BOOL, id, NSURLResponse *))complete sync:(BOOL)sync containPHPSESSIONID:(BOOL)contain{
    
    NSMutableDictionary* md = [dict mutableCopy];
    NSString* sessid =  [[APP instance] SESSIONID];
    if (!sessid) {
        sessid = @"";
    }
    if (contain) {
        [md setObject:sessid forKey:@"PHPSESSID"];
    }
    
    NSArray* keys = [md allKeys];
    
    keys = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [(NSString*)obj1 compare:obj2];
    }];
    NSString* key = keys.firstObject;
    NSMutableString* append  = [@"" mutableCopy];
    if (key) {
        append = [NSMutableString stringWithFormat:@"%@=%@",key,md[key]];
    }
    if (keys.count>1) {
        for (int i =1 ; i<keys.count; i++) {
            key = keys[i];
            [append appendFormat:@"&%@=%@",key,md[key]];
        }
    }
    [append appendFormat:@"%@",SECRET];
    NSString* token = [append md5];
    //两个必填的参数
    [md setObject:token forKey:@"token"];
    if (!sync) {
        [self postRequestToUrl:url params:md complete:complete];
    }else{
        [self postRequestToUrlSynchornize:url params:md complete:complete];
    }


}


+ (NSMutableDictionary *)getRequestBodyWithParams:(NSDictionary *)params
{
    NSMutableDictionary *requestBody = params?[params mutableCopy]:[[NSMutableDictionary alloc] init];
    
    for (NSString *key in [params allKeys]){
        id value = [params objectForKey:key];
        if ([value isKindOfClass:[NSDate class]]) {
            [requestBody setValue:@([value timeIntervalSince1970]*1000) forKey:key];
        }
        if ([value isKindOfClass:[NSDictionary class]] || [value isKindOfClass:[NSArray class]]) {
            [requestBody setValue:[value json] forKey:key];
        }
    }
    
    return requestBody;
}

+ (NetworkStatus)networkStatus
{
    
    Reachability *reachability = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    // NotReachable     - 没有网络连接
    // ReachableViaWWAN - 移动网络(2G、3G)
    // ReachableViaWiFi - WIFI网络
    return [reachability currentReachabilityStatus];
}



-(NSOperationQueue *)queue
{
    if (!_queue) {
        _queue = [[NSOperationQueue alloc] init];
    }
    return _queue;
}

-(void)downloadFromUrl:(NSString *)url complete:(void (^)(BOOL, UIImage *))complete
{
    NSURL * urllink = [NSURL URLWithString:url];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:urllink];
    
    [NSURLConnection sendAsynchronousRequest:request queue:self.queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (connectionError) {
            NSString* errstr = [connectionError description];
            if (errstr) {
                complete(NO,nil);
            }
        }else{
            UIImage* image = [UIImage imageWithData:data];
            complete(YES,image);
            
        }
    }];
}

@end
