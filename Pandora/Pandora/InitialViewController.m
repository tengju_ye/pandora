//
//  InitialViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/10.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "InitialViewController.h"
#import "ConfigViewController.h"
#import "tools.h"
@interface InitialViewController ()
@property (weak, nonatomic) IBOutlet UIButton *startConfig;
@end

@implementation InitialViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"初始化设置";
    [tools addRightBarItem:self];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)startConfig:(id)sender {
    ConfigViewController* cvc = [ConfigViewController getInstanceWithSameNibName];
    [self.navigationController pushViewController:cvc animated:YES];
}

@end
