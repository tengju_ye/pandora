//
//  ItemRevealController.h
//  JDYUN
//
//  Created by 小南宫的mac on 14-7-20.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"
@interface ItemRevealController : UITableViewController
@property (weak, nonatomic) IBOutlet UILabel *cartNumLabel;
@property (nonatomic,strong) Item* item;
@end
