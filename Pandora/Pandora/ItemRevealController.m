//
//  ItemRevealController.m
//  JDYUN
//
//  Created by 小南宫的mac on 14-7-20.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "ItemRevealController.h"
#import "MBProgressHUD.h"
#import "JSONModelLib.h"
#import "HttpManager.h"
#import "UIImageView+WebCache.h"
#import "NGSegmentedControl.h"
#import "CollectServices.h"
#import "CartServices.h"
#import "UIAlertView+MyBlocks.h"
#import "Sku.h"
#import "SkuCellTableViewCell.h"
#import "ItemServices.h"
#import "CartTableViewController.h"
#import "HMSegmentedControl.h"
#import "AddNewOrderTableViewController.h"
#define CurrentWidth  CGRectGetWidth(self.view.frame)
#define WEBVIEWCONTENT_FRAME CGRectMake(0,0,CurrentWidth,50)
#define webViewInfo_FRAME  CGRectMake(CurrentWidth*1,0,CurrentWidth,50)
#define ScrollViewPadding 0
#define Sku_Identifier @"sku_identifier"

@interface ItemRevealController ()<UIWebViewDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIWebView *webview2;

@property (nonatomic,strong) MBProgressHUD* hud;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *greenPrice;
@property (weak, nonatomic) IBOutlet UILabel *oriPriceLabel;
@property (nonatomic,assign) NSUInteger currentSelectedTag;//当前选中的标签
@property (weak, nonatomic) IBOutlet UIButton *butButton;
@property (strong, nonatomic) IBOutlet UITableViewCell *contentCell;
@property (nonatomic,assign) NSUInteger selectedIndex;
@property (nonatomic,strong) GetItemSkuResponse* skuInfo;
@property (nonatomic,strong) GetItemDetailsResponse* detailResponse;
@property (nonatomic,strong) NSArray* itemImages;
@property (nonatomic,strong) GetItemPhotoListResponse* itemPhoto;
@property (nonatomic,strong) Sku* nosku;
@property (nonatomic,assign) BOOL loadOver;
@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property (nonatomic,strong) NSString* infoHtml;
@property (nonatomic,strong) NSString* detailHtml;
@property (weak, nonatomic) IBOutlet UIScrollView *cycScrollview;
@property (nonatomic,strong)  GetSeedStateListResponse* seedState;
@property (nonatomic,strong) NSArray *stateNameList;
@property (strong, nonatomic) IBOutlet UITableViewCell *segCell;
@property (weak, nonatomic) IBOutlet UIButton *contentInfoBtn;
@property (weak, nonatomic) IBOutlet UIButton *infoBtn;
@property (strong, nonatomic) IBOutlet UITableViewCell *headerCell;
- (IBAction)buy:(id)sender;
@property (nonatomic,assign) CGFloat webview1height;
@property (nonatomic,assign) CGFloat webview2height;
@property (weak, nonatomic) IBOutlet UILabel *alreadyBuyLabel;

@end

typedef enum{
    Section_Header=0,
    Section_SKU=1,
    Section_Seg=2,
    Section_Content=3
}Section;

@implementation ItemRevealController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configView];
}




-(void)configView{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SkuCellTableViewCell" bundle:nil] forCellReuseIdentifier:Sku_Identifier];
    
    self.title = @"商品详情";
    
    //show hud
    [self.view addSubview:self.hud];
    [self.hud show:YES];
    //锁住uitableview,加载完毕以后才能动
    self.tableView.scrollEnabled = NO;
    
    
    //获取数据
    [APP runBlockInInitialQ:^{
        GetItemDetailsResponse* itemdetail = [[ItemServices instance] getItemDetails:self.item.item_id.intValue];
        self.detailResponse = itemdetail;
        if (self.item.has_sku.boolValue) {
            GetItemSkuResponse* sku =  [[ItemServices instance] getItemSku:self.item.item_id.intValue];
            self.skuInfo = sku;
        }
        self.itemPhoto = [[ItemServices instance] getItemPhotoList:self.item.item_id.intValue];
        self.seedState = [[ItemServices instance] getSeedStateList:self.item.item_id.intValue];
        
        [APP runBlockInMainQ:^{
            [self reloadViews];
        }];
    }];
    
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    [self reloadCartCount];
}

-(void)reloadViews{
    
    //add item photos
    self.cycScrollview.pagingEnabled = YES;
    for (int i=0;i<[self numberOfPages];i++) {
        UIView* v = [self pageAtIndex:i];
        CGRect r = v.frame;
        r.origin.x = i*CGRectGetWidth(self.cycScrollview.bounds);
        v.frame = r;
        [self.cycScrollview addSubview:v];
    }
    
    self.cycScrollview.contentSize = CGSizeMake(CGRectGetWidth(self.cycScrollview.bounds)*[self numberOfPages], CGRectGetHeight(self.cycScrollview.bounds));
    
    if (!self.item.has_sku.boolValue) {
        self.nosku = [[Sku alloc] init];//亚元
        self.nosku.currentValue = @(1);
        self.nosku.sku_stock = self.item.stock;
        self.nosku.sku_price = self.item.mall_price;
    }
    self.titleLabel.text = [self.item item_name];
    self.priceLabel.text = [NSString stringWithFormat:@"本店价:%@", self.item.mall_price];
    self.greenPrice.text = self.item.mall_price;
    self.alreadyBuyLabel.text = [NSString stringWithFormat:@"已购买:%d",self.item.sales_num.intValue];
    
    NSDictionary* dict =@{ NSForegroundColorAttributeName:[UIColor colorWithWhite:0.459 alpha:1.000],NSFontAttributeName:[UIFont boldSystemFontOfSize:10.0],
                           NSStrikethroughStyleAttributeName:@1};
    [self.oriPriceLabel setAdjustsFontSizeToFitWidth:YES];
    [self.oriPriceLabel setTextAlignment:NSTextAlignmentCenter];
    
    
    
    self.oriPriceLabel.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.2f",self.item.market_price.floatValue] attributes:dict];
    self.detailHtml = [NSString stringWithFormat:@"<style>img{max-width: %fpx}</style>\n%@",CGRectGetWidth(self.view.bounds)-10,self.detailResponse.data];
    self.webview.delegate = self;
    [self.webview.scrollView setScrollEnabled:NO];
    [self.webview loadHTMLString:self.detailHtml baseURL:[NSURL URLWithString:OBJC_IMGBASE_URL]];
    [self.webview2.scrollView setScrollEnabled:NO];
    [self.webview2 loadHTMLString:self.infoHtml baseURL:nil];
    [self reloadCartCount];
}

-(void)reloadCartCount{
    [APP runBlockInInitialQ:^{
        GetCartTotalNumResponse* response = [[CartServices instance] getCartTotalNum];
        int i = response.data.intValue;;
        
        [APP runBlockInMainQ:^{
            if (i!=0) {
                self.cartNumLabel.text = response.data;
                [self.cartNumLabel setTextColor:[UIColor whiteColor]];
                self.cartNumLabel.backgroundColor = [UIColor redColor];
                [self.cartNumLabel.layer setCornerRadius:CGRectGetWidth(self.cartNumLabel.bounds)/2];
                [self.cartNumLabel setClipsToBounds:YES];
                [self.cartNumLabel setHidden:NO];
            }else{
                self.cartNumLabel.hidden = YES;
            }
        }];
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - XLscroll method and delegate
- (NSInteger)numberOfPages
{
    return self.itemPhoto.data.count;//self.singleFeed.detail_img.count;
}

-(UIView *)pageAtIndex:(NSInteger)index
{
    CGRect r = self.cycScrollview.bounds;
    UIImageView* imageview = [[UIImageView alloc] initWithFrame:r];
    NSString* url = [[self.itemPhoto data] objectAtIndex:index];
    [imageview sd_setImageWithURL:[NSURL URLWithString:url relativeToURL:[NSURL URLWithString:OBJC_IMGBASE_URL]]];
    
    return imageview;
    
}

#pragma mark - setter
-(void)setSelectedIndex:(NSUInteger)selectedIndex
{
    NSUInteger before = _selectedIndex;
    _selectedIndex = selectedIndex;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (before != selectedIndex) {
            if (_selectedIndex == 0) {
                self.webview2.hidden = YES;
            }else{
                self.webview2.hidden = NO;
                
            }
            [self.tableView reloadData];
        }
    });
    
}


#pragma mark - getter
-(MBProgressHUD *)hud
{
    if (!_hud) {
        _hud = [[MBProgressHUD alloc] initWithView:self.view];
        [_hud setLabelText:@"请稍后"];
    }
    return _hud;
}





#pragma mark - webview


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        return NO;
    }
    return YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    NSString* string = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"];
    CGRect frame = webView.frame;
    frame.size.height = 1;
    webView.frame = frame;
    
    //重新摄者webview的大小
    CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
    fittingSize.height = string.floatValue;
    frame.size = fittingSize;
    webView.frame = frame;
    
    if (webView==self.webview) {
        self.webview1height = fittingSize.height;
    }else{
        self.webview2height = fittingSize.height;

    }
    
    
    //计数
    if (!self.webview.isLoading&&!self.webview2.isLoading) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [self.hud hide:YES];
            self.tableView.scrollEnabled = YES;
        });
        
    }
}



- (CGFloat)minHeightForText:(NSString *)_text {
    
    return [_text
            sizeWithFont:[UIFont systemFontOfSize:16.0]
            constrainedToSize:CGSizeMake(320, 999999)
            lineBreakMode:NSLineBreakByWordWrapping
            ].height;
}


#pragma mark - table view delegate & datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==Section_Content) {
        return 1;
    }else if(section==Section_Seg){
        return 1;
    }else if(section == Section_SKU){
        if (self.item.has_sku.boolValue) {
            return self.skuInfo.sku_info.count+1;
        }else{
            return 2;
        }
    }
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section==Section_Content) {
        return CGRectGetHeight(self.footerView.bounds);
    }
    return 1.0;
}

-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section==Section_Content) {
        return self.footerView;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == Section_SKU) {
        SkuCellTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Sku_Identifier];
        if (indexPath.row==0) {
            cell.sku = nil;
            if (self.item.has_sku.boolValue) {
                cell.prop1.text = self.skuInfo.sku_name1;
                cell.prop2.text = self.skuInfo.sku_name2;
            }else{
                cell.prop1.hidden = YES;
                cell.prop2.hidden = YES;
            }
        }else{
            if (self.item.has_sku.boolValue) {
                Sku* sku = self.skuInfo.sku_info[indexPath.row-1];
                cell.sku = sku;
            }else{
                cell.prop1.hidden = YES;
                cell.prop2.hidden = YES;
                cell.sku = self.nosku;
            }
        }
        return cell;
    }else if(indexPath.section==Section_Content){
        return self.contentCell;
    }else if(indexPath.section == Section_Seg){
        return self.segCell;
    }else{
        return self.headerCell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==Section_Header) {
        return CGRectGetWidth(self.tableView.frame)+92;
    }else if (indexPath.section==Section_SKU) {
        return 40.0;
    }else if(indexPath.section == Section_Seg){
        return 30.0;
    }else{
        if (self.selectedIndex==0) {
            return self.webview1height;
        }else{
            return self.webview2height;
        }

    }
}



-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - IBAction
- (IBAction)resignKeyBoard:(id)sender {
    [self.tableView endEditing:YES];
}
- (IBAction)addToFav:(id)sender {
    [MBProgressHUD showAddViewWhen:self.view when:^{
        if ([sender isSelected]) {
            //remove
            [[CollectServices instance] cancelCollectFrom:self.item.item_id.intValue];
        }else{
            //add
            [[CollectServices instance] addCollect:self.item.item_id.intValue];
        }
    } complete:^{
        [sender setSelected:![sender isSelected]];
    } text:@"请稍等"];
}
- (IBAction)addToCart:(id)sender {
    
    NSString* string = nil;
    if (self.item.has_sku.boolValue) {
        NSString* tem = @"";
        int count =0 ;
        for (Sku* sku in self.skuInfo.sku_info) {
            count += sku.currentValue.integerValue;
            if (self.skuInfo.sku_name2.length!=0) {
                tem = [tem stringByAppendingString:[NSString stringWithFormat:@"%@:%@ %@:%@,%@,%@,%@;",self.skuInfo.sku_name1,sku.property_name1,self.skuInfo.sku_name2,sku.property_name2,sku.sku_price,sku.currentValue,sku.item_sku_price_id]];
            }else{
                tem = [tem stringByAppendingString:[NSString stringWithFormat:@"%@:%@,%@,%@,%@;",self.skuInfo.sku_name1,sku.property_name1,sku.sku_price,sku.currentValue,sku.item_sku_price_id]];
            }
            
        }
        
        if (count == 0) {
            [UIAlertView showAlertViewWithTitle:@"请至少选择一款商品" message:nil cancelButtonTitle:@"好的" otherButtonTitles:nil onDismiss:^(int buttonIndex, UIAlertView *alertView) {
                
            } onCancel:^{
                
            }];
        }else{
            string = tem;
        }
    }else{
        if (self.nosku.currentValue.integerValue==0) {
            [UIAlertView showAlertViewWithTitle:@"请至少选择一款商品" message:nil cancelButtonTitle:@"好的" otherButtonTitles:nil onDismiss:^(int buttonIndex, UIAlertView *alertView) {
                
            } onCancel:^{
                
            }];
        }else{
            string = [NSString stringWithFormat:@",%@,%@,0;",self.item.mall_price,self.nosku.currentValue];
        }
    }
    [MBProgressHUD showAddViewWhen:self.view when:^{
        [[CartServices instance] addCart:self.item.item_id.intValue item_str:string];
    } complete:^{
        [self reloadCartCount];
    } text:@"添加中"];
}

- (IBAction)buy:(id)sender {
    NSString* string = nil;
    if (self.item.has_sku.boolValue) {
        NSString* tem = @"";
        int count =0 ;
        for (Sku* sku in self.skuInfo.sku_info) {
            count += sku.currentValue.integerValue;
            if (self.skuInfo.sku_name2.length!=0) {
                tem = [tem stringByAppendingString:[NSString stringWithFormat:@"%@:%@ %@:%@,%@,%@,%@;",self.skuInfo.sku_name1,sku.property_name1,self.skuInfo.sku_name2,sku.property_name2,sku.sku_price,sku.currentValue,sku.item_sku_price_id]];
            }else{
                tem = [tem stringByAppendingString:[NSString stringWithFormat:@"%@:%@,%@,%@,%@;",self.skuInfo.sku_name1,sku.property_name1,sku.sku_price,sku.currentValue,sku.item_sku_price_id]];
            }
            
        }
        
        if (count == 0) {
            [UIAlertView showAlertViewWithTitle:@"请至少选择一款商品" message:nil cancelButtonTitle:@"好的" otherButtonTitles:nil onDismiss:^(int buttonIndex, UIAlertView *alertView) {
                
            } onCancel:^{
                
            }];
        }else{
            string = tem;
        }
    }else{
        if (self.nosku.currentValue.integerValue==0) {
            [UIAlertView showAlertViewWithTitle:@"请至少选择一款商品" message:nil cancelButtonTitle:@"好的" otherButtonTitles:nil onDismiss:^(int buttonIndex, UIAlertView *alertView) {
                
            } onCancel:^{
                
            }];
        }else{
            string = [NSString stringWithFormat:@",%@,%@,0;",self.item.mall_price,self.nosku.currentValue];
        }
    }
    
    if (string!=nil) {
        [MBProgressHUD showAddViewWhen:self.view when:^{
            AddCartResponse*response =  [[CartServices instance] addCart:self.item.item_id.intValue item_str:string];
            NSArray* cartList  = [response.shopping_cart_id_list componentsSeparatedByString:@","];
            NSArray* numlist  = [response.number_list componentsSeparatedByString:@","];
            

            NSMutableDictionary* requireCart = [NSMutableDictionary dictionary];
            for (int i =0; i<cartList.count; i++) {
                [requireCart setObject:[numlist objectAtIndex:i]forKey:[cartList objectAtIndex:i]];
            }
    
            GetCartListByIdsResponse* listresponse = [[CartServices instance] getCartListByIds:response.shopping_cart_id_list];
    
            NSMutableArray* arr = [NSMutableArray array];
            for (Cart* cart in listresponse.data) {
                NSString* number  = nil;
                if ((number = [requireCart objectForKey:cart.shopping_cart_id])) {
                    cart.choose = @YES;
                    cart.number = number;
                    [arr addObject:cart];
                }
            }
            [APP runBlockInMainQ:^{
                AddNewOrderTableViewController* aotvc = [AddNewOrderTableViewController getInstanceWithSameNibName];
                aotvc.carts = arr;
                [self.navigationController pushViewController:aotvc animated:YES];
            }];
        } text:@"请稍侯"];
        
    }
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
}

- (IBAction)checkCart:(id)sender {
    UIStoryboard* stboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    CartTableViewController* ctc = [stboard instantiateViewControllerWithIdentifier:@"CartTableViewController"];
    [self.navigationController pushViewController:ctc animated:YES];
}


-(NSString *)infoHtml{
    if (!_infoHtml) {
        NSMutableString* string = [@"<style>\
                                   table.table{\
                                   border: 0;\
                                   width:100%;\
                                   border-spacing: 0;\
                                   background: #eee;\
                                   border-collapse: collapse;\
                                   text-align: center;\
                                   }\
                                   table.table tr td, table.table tr th{\
                                   border: 1px #ddd solid;\
                                   display: table-cell;\
                                   padding: 5px;\
                                   }\
                                   </style>\n<table class=\"table\">\
                                   <tbody>" mutableCopy];
        if (self.seedState.data.count!=0) {
            for (SeedState* state in self.seedState.data) {
                [string appendString:@"<tr>"];
                [string appendFormat:@"<td>%@</td>",self.stateNameList[state.state.intValue]];
                [string appendFormat:@"<td>温度:%@</td>",state.outside_temperature];
                [string appendFormat:@"<td>湿度:%@</td>",state.humidity];
                [string appendFormat:@"<td>光照:%@Lux</td>",state.illuminance_limit];
                [string appendFormat:@"</tr>"];
            }
        }
        [string appendFormat:@"</tbody>"];
        [string appendFormat:@"</table>"];
        _infoHtml = string;
    }
    return _infoHtml;
}

- (NSArray *)stateNameList {
    if(!_stateNameList) {
        _stateNameList = @[@"",@"种子期",@"萌发期",@"幼苗期",@"根茎叶期",@"花芽期",@"开花期",@"传粉期",@"结果期",@"种子形成期"];
    }
    return _stateNameList;
}
- (IBAction)selectContent:(id)sender {
    self.contentInfoBtn.selected = YES;
    self.infoBtn.selected = NO;
    self.selectedIndex = 0;
    
}
- (IBAction)selectInfo:(id)sender {
    self.infoBtn.selected = YES;
    self.contentInfoBtn.selected = NO;
    self.selectedIndex = 1;
}

@end
