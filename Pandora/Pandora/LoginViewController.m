//
//  LoginViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/11/26.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "LoginViewController.h"
#import "UserServices.h"
#import "LoginForm.h"
#import "LoginResponse.h"
#import "APIHeader.h"
#import "UIViewController+ShowRight.h"
#import "ConfigViewController.h"
#import "RightMenuViewController.h"
#import "SeedStateViewController.h"
#import "PlanterSeedServices.h"
#import "PlanterServices.h"
#import "FirstBindViewController.h"
#import "XLCycleNGScrollView.h"

#define LoginCycleViewNames @[@"图1",@"图2",@"图3",@"图4"]
@interface LoginViewController ()<UITextFieldDelegate,XLCycleScrollViewDatasource,XLCycleScrollViewDelegate>
@property (weak, nonatomic) IBOutlet XLCycleNGScrollView *cycleView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic,strong) NSArray* cycleImageNames;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /**
     *  configure buttons
     */
    [self.loginButton setClipsToBounds:YES];
    [[self.loginButton layer] setCornerRadius:3.0];
    [[self.loginButton layer] setBorderWidth:1.0];
    [[self.loginButton layer] setBorderColor:[UIColor colorWithRed:0.831 green:0.827 blue:0.808 alpha:1.000].CGColor];
    
    UIColor* attributeColor = [UIColor colorWithRed:0.514 green:0.510 blue:0.510 alpha:1.000];
    NSDictionary* attributes = @{NSForegroundColorAttributeName:attributeColor,NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),NSFontAttributeName:[UIFont systemFontOfSize:14.0]};
    [self.registerBtn setAttributedTitle:[[NSAttributedString alloc] initWithString:@"新用户注册" attributes:attributes] forState:UIControlStateNormal];
    

    
    //self.accountTF.text = @"jiangwei804";//[userDefaults objectForKey:UD_KEY_ACCOUNT];
    //self.pswTF.text = @"123456";//[userDefaults objectForKey:UD_KEY_PSW];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    /**try login**/
    [self trylogin];
}

- (void)trylogin {
    MBProgressHUD* hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.labelText = @"请稍等";
    NSString* username = [userDefaults objectForKey:UD_KEY_ACCOUNT];
    NSString* psw = [userDefaults objectForKey:UD_KEY_PSW];
    
    if (!(username.length*psw.length)) {
        return;
    }
    
    [hud showAnimated:YES whileExecutingBlock:^{
        LoginResponse* response = [[UserServices instance] login:username password:psw verify_code:nil];

        if ([response.code isEqualToString:OBJC_SUCCEED_CODE]) {
            hud.labelText = @"登陆成功";
            userDefaultsSet(UD_KEY_PSW, response.password);
            GetUserInfoResponse* userInfo = [[UserServices instance] getUserInfo];
            GetPlanterInfoResponse* infoResponse = nil;
            if (userInfo.data.current_planter_id.intValue!=0) {
                infoResponse = [[PlanterServices instance] getPlanterInfo];
            }

            dispatch_async(dispatch_get_main_queue(), ^{
                [[APP instance] setSESSIONID:response.phpsessid];
                DLog(@"sessid = %@",response.phpsessid);
                if (infoResponse&&infoResponse.planter_seed_id) {
                    //存在当前种植机,并且有种植号,如果是正常登录状态则，推出后到主控界面
                    SeedStateViewController *newController = [SeedStateViewController getInstanceWithSameNibName];
                    newController.planterSeedId = infoResponse.planter_seed_id;
                    newController.seedId = infoResponse.seed_id.intValue;
                    newController.name = infoResponse.planter_name;
                    UINavigationController* nvc = [[UINavigationController alloc] initWithRootViewController:newController];
                    [self.rmvc.mm_drawerController setCenterViewController:nvc];
                    [self dismissViewControllerAnimated:YES completion:^{
                    }];
                }else{
                    FirstBindViewController* fbvc = [FirstBindViewController getInstanceWithSameNibName];
                    fbvc.username = userInfo.data.nickname;
                    UINavigationController* nvc = [[UINavigationController alloc] initWithRootViewController:fbvc];
                    [self.rmvc.mm_drawerController setCenterViewController:nvc];
                    [self dismissViewControllerAnimated:YES completion:^{

                    }];
                }
            });
        }else{
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)hideKeyBoard:(id)sender {
}

-(void)setRmvc:(RightMenuViewController *)rmvc{
    _rmvc = rmvc;
}

- (IBAction)action:(id)sender {
    ConfigViewController* cvc = [ConfigViewController getInstanceWithSameNibName];
    [self.navigationController pushViewController:cvc animated:YES];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - cycle
- (NSInteger)numberOfPages{
    [self.pageControl setNumberOfPages:self.cycleImageNames.count];
    return self.cycleImageNames.count;
}
- (UIView *)pageAtIndex:(NSInteger)index{
    NSString* imageName = [[self cycleImageNames] objectAtIndex:index];
    UIImageView* iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    [iv setContentMode:UIViewContentModeScaleAspectFit];
    return iv;
}

-(void)didChangePageTo:(NSUInteger)page view:(XLCycleNGScrollView *)csView{
    self.pageControl.currentPage = page;
}

#pragma mark - getter
-(NSArray *)cycleImageNames{
    if (!_cycleImageNames) {
        _cycleImageNames = LoginCycleViewNames;
    }
    return _cycleImageNames;
}

#pragma mark - segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"Register_Segue"]) {
        RegisterTableViewController* rtc = segue.destinationViewController;
        rtc.title = @"注册";
    }else if ([segue.identifier isEqualToString:@"Login_Segue"]) {
        RegisterTableViewController* rtc = segue.destinationViewController;
        rtc.loginMode = YES;
        rtc.title = @"登陆";
    }
}

@end
