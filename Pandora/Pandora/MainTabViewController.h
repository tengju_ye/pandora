//
//  MainTabViewController.h
//  Pandora
//
//  Created by 小南宫的mac on 14/11/26.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum:NSUInteger{
    MainTabItem_MALL,
    MainTabItem_Search,
    MainTabItem_Cart,
    MainTabItem_Personal
} MainTabItem;
@interface MainTabViewController : UITabBarController

@end
