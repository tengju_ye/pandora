//
//  MainTabViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/11/26.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "MainTabViewController.h"
@interface MainTabViewController ()

@end

@implementation MainTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    for (UIViewController* controller in self.viewControllers) {
        UIImage* image = [UIImage imageNamed:@"menu_icon"];
        UIButton* btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width/2, image.size.height/2)];
        [btn addTarget:self action:@selector(rightDrawerButtonPress:) forControlEvents:UIControlEventTouchUpInside];
        [btn setImage:image forState:UIControlStateNormal];
        UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btn];
        if ([controller isKindOfClass:[UINavigationController class]]) {
            [[[(UINavigationController*)controller topViewController] navigationItem] setRightBarButtonItem:item];
        }
    }
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
