//
//  MallHeaderView.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/3.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MallHeaderView;
@protocol MallHeaderViewDelegate<NSObject>
-(void)didTouchHeaderView:(MallHeaderView*) headerView;
@end
@interface MallHeaderView : UITableViewHeaderFooterView
@property(nonatomic,assign)NSUInteger section;
@property(nonatomic,strong)id<MallHeaderViewDelegate>delegate;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@end
