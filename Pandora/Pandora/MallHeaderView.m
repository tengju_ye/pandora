//
//  MallHeaderView.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/3.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "MallHeaderView.h"

@implementation MallHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (IBAction)didClickMore:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didTouchHeaderView:)]) {
        [self.delegate didTouchHeaderView:self];
    }
}

@end
