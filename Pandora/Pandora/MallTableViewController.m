//
//  MallTableViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/11/24.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "MallTableViewController.h"
#import "MallServices.h"
#import "ItemServices.h"
#import "CycleScrollView.h"
#import "HMSegmentedControl.h"
#import "CategoryServices.h"
#import "GetItemListGroupBySortResponse.h"
#import "AClass.h"
#import "SingleItemTableViewCell.h"
#import "DoubleItemCellTableViewCell.h"
#import "Item.h"
#import "MallHeaderView.h"
#import "ItemRevealController.h"
#import "UIViewController+MMDrawerController.h"
#import "MarketSearchViewController.h"
#import "MyOrderTableViewController.h"
#import "MainTabViewController.h"
#import "MyFavTableViewController.h"
#import "SearchResultTableViewController.h"
#import "LoginViewController.h"
#import "tools.h"

#pragma mark - Help Class
@interface Pair : NSObject
@property(nonatomic,strong) Item* item1;
@property(nonatomic,strong) Item* item2;
@end

@implementation Pair
@end


@interface MallTableViewController ()<MallHeaderViewDelegate,SingleItemTableViewCellDelegate,DoubleItemTableViewCellDelegate,UISearchBarDelegate>
@property(nonatomic,assign) BOOL inited;
@property(nonatomic,strong) MallServices* mallServices;
@property(nonatomic,strong) ItemServices* itemServices;
@property(nonatomic,strong) CategoryServices* cateServices;
@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;


/// mallHomeCache
@property(nonatomic,strong) NSMutableDictionary* mallHomeData;//classid<String> -> array[Section(ItemList)<Array<Pair>>]

/**
 segments
 */
@property (weak, nonatomic) IBOutlet UIView *segmentContainer;
@property (nonatomic,assign) NSUInteger selectedIndex;
@property (nonatomic,strong) HMSegmentedControl* segmentControl;
@property (nonatomic,strong) NSArray* segmentsArray;//<AClass>
/**
 *  titleviews
 */
@property(nonatomic,strong) CycleScrollView* cycscrollview;
@property (weak, nonatomic) IBOutlet UIView *titleImageViews;
@property (nonatomic,strong) NSMutableArray* titleViewDatasource;
@property (nonatomic,strong) DoubleItemCellTableViewCell* doubleCellPrototype;
@property (nonatomic,strong) SingleItemTableViewCell* singleCellPrototype;

@end

@implementation MallTableViewController

static NSString* const SingleItemIdentifier = @"SingleItemIdentifier";
static NSString* const DoubleItemIdentifier = @"DoubleItemIdentifier";
static NSString* const HeaderIdentifier = @"HeaderIdentifier";

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        
        UIImage* image =[UIImage imageNamed:@"mall_icon"];
        [self.navigationController.tabBarItem setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        image = [UIImage imageNamed:@"mall_icon_on"];
        [self.navigationController.tabBarItem setSelectedImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    

    [self.tableView setAllowsSelection:NO];
    
    self.cycscrollview = [[CycleScrollView alloc] initWithFrame:self.titleImageViews.bounds animationDuration:5.0];
    [self.titleImageViews addSubview:self.cycscrollview];
    
    self.cycscrollview.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    [self.tableView addHeaderWithTarget:self action:@selector(refreshCurrentMallView)];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MallHeaderView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:HeaderIdentifier];
    
    [self.searchbar setDelegate:self];
    [tools changeSearchCancelText:self.searchbar];
    

    
    self.doubleCellPrototype = [self.tableView dequeueReusableCellWithIdentifier:DoubleItemIdentifier];
    self.singleCellPrototype = [self.tableView dequeueReusableCellWithIdentifier:SingleItemIdentifier];

    [self    reCalcTableHeaderView];
//    [self.tableView setRowHeight:UITableViewAutomaticDimension];
//    [self.tableView setEstimatedRowHeight:320];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
//    [self reCalcTableHeaderView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#define HeaderContainerW2HRatio 1.78
- (void)reCalcTableHeaderView
{
    
    UIView* header = self.tableView.tableHeaderView;
//    self.tableView.tableHeaderView = nil;
    CGFloat height = CGRectGetWidth(self.view.bounds) / HeaderContainerW2HRatio + 144 ;
    CGRect rect = header.frame;
    rect.size.height = height;
    header.frame = rect;
    
//    self.tableView.tableHeaderView = header;
}

-(void)viewWillAppear:(BOOL)animated{
    if ([[APP instance] SESSIONID]==nil) {
        UINavigationController* loginNVC = [self.storyboard instantiateViewControllerWithIdentifier:STORYBOARD_ID_LOGIN_NVC];
        LoginViewController* login = (LoginViewController*)loginNVC.topViewController;
        login.rmvc  = (RightMenuViewController*)self.mm_drawerController.rightDrawerViewController;
        [self.navigationController presentViewController:loginNVC animated:NO completion:nil];
    }
    
    if (!self.inited&&[[APP instance] SESSIONID]) {
        [self initMallView];
        self.inited = YES;
    }
}

-(void)viewDidAppear:(BOOL)animated{

    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [[self sectionsForCurrentIndex] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
//    NSInteger count = [[[self datasInSectionInCurrentIndex:section] pairItemlist] count];
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Pair* pair = [self pairOfCurrentIndex:indexPath];
    
    UITableViewCell *cell = nil;
    if (pair.item2!=nil) {
        cell = [tableView dequeueReusableCellWithIdentifier:DoubleItemIdentifier];
        DoubleItemCellTableViewCell* dc = (DoubleItemCellTableViewCell*)cell;
        dc.delegate = self;
        dc.item1 = pair.item1;
        dc.item2 = pair.item2;
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:SingleItemIdentifier];
        SingleItemTableViewCell* sc = (SingleItemTableViewCell*)cell;
        sc.delegate = self;
        sc.item = pair.item1;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Pair* pair = [self pairOfCurrentIndex:indexPath];
    CGFloat height = 0;
    if (pair.item2) {
        height = 70+(CGRectGetWidth(self.tableView.bounds)-32-14)/2;
    }else{
        height = 53+CGRectGetWidth(self.tableView.bounds)-32;
    }
    return height+1;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 325;
}

#pragma mark - Header&Cell delegate
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    MallHeaderView* headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:HeaderIdentifier];
    NSArray* array = [self sectionsForCurrentIndex];
    ItemList* itemlist = array[section];
    headerView.section = section;
    headerView.titleLabel.text = itemlist.sort_name;
    headerView.delegate = self;
    return headerView;
}

-(void)doubleItemCell:(DoubleItemCellTableViewCell *)cell didClickItem:(Item *)item{
    ItemRevealController* controller = [[ItemRevealController alloc] initWithNibName:@"ItemRevealController" bundle:nil];
    controller.item = item;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)singleItemCell:(SingleItemTableViewCell *)cell didClickItem:(Item *)item{
    ItemRevealController* controller = [[ItemRevealController alloc] initWithNibName:@"ItemRevealController" bundle:nil];
    controller.item = item;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)didTouchHeaderView:(MallHeaderView *)headerView{
    ItemList* list = [[self sectionsForCurrentIndex] objectAtIndex:headerView.section];
    SearchResultTableViewController* srtvc = [SearchResultTableViewController getInstanceWithSameNibName];
    srtvc.datasource = [list.item_list mutableCopy];
    srtvc.aclasses = self.segmentsArray;
    [self.navigationController pushViewController:srtvc animated:YES];
    
}




- (IBAction)allCateAction:(id)sender {
    SearchResultTableViewController* srtvc =  [SearchResultTableViewController getInstanceWithSameNibName];
    srtvc.aclasses = self.segmentsArray;
    srtvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:srtvc animated:YES];

}

- (IBAction)myorderAction:(id)sender {
    MyOrderTableViewController* motvc = [[MyOrderTableViewController alloc] initWithNibName:[MyOrderTableViewController nibName] bundle:nil];
    [self.navigationController pushViewController:motvc animated:YES];
}

- (IBAction)myfavaction:(id)sender {
    MyFavTableViewController* fav = [[MyFavTableViewController alloc] init];
    fav.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:fav animated:YES];
}
- (IBAction)cartAction:(id)sender {
    [self.tabBarController setSelectedIndex:MainTabItem_Cart];
}


#pragma mark - setter
-(void)setTitleViewDatasource:(NSMutableArray *)titleViewDatasource{
    
    _titleViewDatasource = titleViewDatasource;
    dispatch_async(MAIN_QUEUE, ^{
        __weak MallTableViewController* ws = self;
        self.cycscrollview.fetchContentViewAtIndex = ^UIView *(NSInteger pageIndex){
            return [ws pageAtIndex:pageIndex];
        };
        self.cycscrollview.totalPagesCount = ^NSInteger(void){
            return [ws numberOfPages];
        };
    });
}

-(void)setSegmentsArray:(NSArray *)segmentsArray{
    _segmentsArray = segmentsArray;
    dispatch_async(MAIN_QUEUE, ^{
        [self.segmentControl removeFromSuperview];
        self.segmentControl = nil;
        if (_segmentsArray.count!=0) {
            [self.segmentContainer addSubview:self.segmentControl];
        }
    });

}


-(void)setSelectedIndex:(NSUInteger)selectedIndex
{
    _selectedIndex = selectedIndex;
    
    if ([self sectionsForCurrentIndex]==0) {
        dispatch_async(MAIN_QUEUE, ^{
            [[self tableView] headerBeginRefreshing];
        });
    }else{
        dispatch_async(MAIN_QUEUE, ^{
            [[self tableView] reloadData];
        });
    }
    

}

#pragma mark - getter
-(MallServices *)mallServices{
    return [MallServices instance];
}

-(ItemServices *)itemServices{
    return [ItemServices instance];
}

-(CategoryServices *)cateServices{
    return [CategoryServices instance];
}



-(HMSegmentedControl *)segmentControl
{
    if (!_segmentControl) {
        NSMutableArray* titles = [[NSMutableArray alloc] init];
        for (AClass* aclass in self.segmentsArray) {
            [titles addObject:aclass.class_name];
        }
        _segmentControl = [[HMSegmentedControl alloc] initWithFrame:self.segmentContainer.bounds];
        [_segmentControl setSelectionIndicatorLocation:HMSegmentedControlSelectionIndicatorLocationDown];
        [_segmentControl setSelectionIndicatorColor:[UIColor redColor]];
        [_segmentControl setSelectedTextColor:[UIColor redColor]];
        [_segmentControl setSelectionIndicatorHeight:2.0];
        [_segmentControl setSelectionStyle:HMSegmentedControlSelectionStyleFullWidthStripe];
        [_segmentControl setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];
        [_segmentControl setSectionTitles:titles];
        __weak MallTableViewController* weakSelf =self;
        [_segmentControl setIndexChangeBlock:^(NSInteger index) {
            weakSelf.selectedIndex = index;
        }];
        [_segmentControl setBackgroundColor:[UIColor colorWithWhite:0.936 alpha:1.000]];
        [_segmentControl setTextColor:[UIColor colorWithWhite:0.531 alpha:1.000]];
        [_segmentControl setSegmentEdgeInset:UIEdgeInsetsMake(0, 6, 0, 6)];
    }
    return _segmentControl;
}

-(NSMutableDictionary *)mallHomeData{
    if (!_mallHomeData) {
        _mallHomeData = [NSMutableDictionary dictionary];
    }
    return _mallHomeData;
}


#pragma mark - helper
- (NSInteger)numberOfPages{
  return  self.titleViewDatasource.count;
}
- (UIView *)pageAtIndex:(NSInteger)index{
    NSString* url = [self.titleViewDatasource objectAtIndex:index];

   UIImageView*iv = [[UIImageView alloc] initWithFrame:self.titleImageViews.bounds];
    [iv sd_setImageWithURL:[NSURL URLWithString:url relativeToURL:[NSURL URLWithString:OBJC_IMGBASE_URL]]];
    return iv;
}

-(NSArray*)sectionsForCurrentIndex{
    NSArray* sections = nil;
    if (self.segmentsArray.count>self.selectedIndex) {
        AClass* currentClass = self.segmentsArray[self.selectedIndex];
        sections = self.mallHomeData[currentClass.class_id];
    }
    return sections;
}

-(ItemList*)datasInSectionInCurrentIndex:(NSUInteger)section{
    NSArray* arr = [self sectionsForCurrentIndex];
    return [arr objectAtIndex:section];
}

-(Pair*)pairOfCurrentIndex:(NSIndexPath*)indexpath{
    ItemList* itemList = [self datasInSectionInCurrentIndex:indexpath.section];
    return itemList.pairItemlist[indexpath.row];
}


#pragma mark - WebRequest
-(void)initMallView{
    MBProgressHUD* hud = [[MBProgressHUD alloc] initWithView:self.view];
    hud.labelText = @"请稍等";
    [self.view addSubview:hud];
    [hud setRemoveFromSuperViewOnHide:YES];
    [hud showAnimated:YES whileExecutingBlock:^{
        
        //head imgs
        GetMallHomeImgListResponse* listResponse = [self.mallServices getMallHomeImg];
        NSArray* images = [[listResponse data] componentsSeparatedByString:@";"];
        self.titleViewDatasource = [images mutableCopy];
        
        //top level segments
        GetClassListResponse* classlist = [self.cateServices getClassList];
        if ([classlist.code isEqualToString:OBJC_SUCCEED_CODE]) {
            self.segmentsArray = [classlist.data mutableCopy];
        }
        self.selectedIndex = 0;
    }];
}

-(void)refreshCurrentMallView{
    AClass* aclass = [self.segmentsArray objectAtIndex:self.selectedIndex];
    dispatch_async([[APP instance] initialQueue], ^{
        GetItemListGroupBySortResponse* response = [self.itemServices getItemListGroupBySort:aclass.class_id];
        if (response.data.count!=0) {
            NSArray* itemLists = response.data;
            for (ItemList* itemlist in itemLists) {
                NSMutableArray* array = [NSMutableArray array];
                for (int i=0; i<itemlist.item_list.count; i+=2) {
                    Pair* pair  =[Pair new];
                    pair.item1 = itemlist.item_list[i];
                    if ((i+1)<itemlist.item_list.count) {
                        pair.item2 = itemlist.item_list[i+1];
                    }
                    [array addObject:pair];
                }
                itemlist.pairItemlist = array;
            }
            [self.mallHomeData setObject:itemLists forKey:aclass.class_id];//每个itemlist都是一个section
        }
        dispatch_async(MAIN_QUEUE, ^{
            [self.tableView headerEndRefreshing];
            [self.tableView reloadData];
        });
    });

}




#pragma mark - search bar delegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    SearchResultTableViewController* srtvc =  [SearchResultTableViewController getInstanceWithSameNibName];
    srtvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:srtvc animated:YES];
    srtvc.item_name = searchBar.text;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    searchBar.text = nil;
}

@end
