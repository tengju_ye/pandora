//
//  MarketSearchViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/11/24.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "MarketSearchViewController.h"
#import "MallServices.h"
#import "CategoryServices.h"
#import "HotKeyCollectionViewCell.h"
#import "ItemServices.h"
#import "SearchResultTableViewController.h"
#import "Sort.h"
#import "HotKeySearchViewController.h"
#import "ItemList.h"
#import "tools.h"
#define CollectionIdentifier @"CollectionIdentifier"
@interface MarketSearchViewController ()<UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) NSArray* classes;
@property (nonatomic,strong) NSMutableDictionary* subCates;//classid->Nsarray
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic,assign) NSUInteger selectedIndex;
@end

@implementation MarketSearchViewController

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        UIImage* image =[UIImage imageNamed:@"search_icon"];
        [self.navigationController.tabBarItem setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        image = [UIImage imageNamed:@"search_icon_on"];
        [self.navigationController.tabBarItem setSelectedImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"HotKeyCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:CollectionIdentifier];
    _subCates = [NSMutableDictionary new];
    [self configView];
}

-(void)configView{
    
    [self.tableview setTableFooterView:[UIView new]];
    
    [MBProgressHUD showAddViewWhen:self.view when:^{
        //top level segments
        GetClassListResponse* classlist = [[CategoryServices instance] getClassList];
        if ([classlist.code isEqualToString:OBJC_SUCCEED_CODE]) {
            self.classes = [classlist.data mutableCopy];
        }
    } complete:^{
        [self.tableview reloadData];
        [self.tableview selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        [self tableView:self.tableview didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] ];
    } text:@"请稍等"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.classes.count;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

#define TAG_LABEL 101
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* const identifier = @"ClassCellIdentifier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    UILabel* label = (UILabel*)[cell.contentView viewWithTag:TAG_LABEL];
    label.text =  [(AClass*)self.classes[indexPath.row] class_name];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AClass* aclass = self.classes[indexPath.row];
    NSArray* subcates = [self.subCates objectForKey:aclass.class_id];
    self.selectedIndex = indexPath.row;
    if (subcates==nil) {
        [MBProgressHUD showAddViewWhen:self.view when:^{
            GetSortListByClassIdResponse* response =  [[CategoryServices instance] getSortKListByClassId:aclass.class_id.intValue];
            [self.subCates setObject:response.data forKey:aclass.class_id];
        } complete:^{
            [self.collectionView reloadData];
        } text:@"请稍等"];
    }else{
        [self.collectionView reloadData];

    }
}


#pragma mark - collection view
#pragma mark - collectionview
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    AClass* aclass = [self.classes objectAtIndex:self.selectedIndex];
    return [self.subCates[aclass.class_id] count];
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SearchResultTableViewController* srtvc = [SearchResultTableViewController getInstanceWithSameNibName];
    srtvc.aclasses = self.classes;
    AClass* aclass = [self.classes objectAtIndex:self.selectedIndex];
    NSArray* sorts = [self.subCates objectForKey:aclass.class_id];
    Sort *sort = [sorts objectAtIndex:indexPath.item];
    
    srtvc.nextFirstRow = 0;
    srtvc.classID = sort.class_id;
    srtvc.sortID = sort.sort_id;
    srtvc.classTag = aclass.class_tag;
//    srtvc.item_name = sort.sort_name;
    srtvc.shouldSearch = YES;
    [self.navigationController pushViewController:srtvc animated:YES];

//    [MBProgressHUD showAddViewWhen:self.view when:^{
//        GetItemListResponse* response = [[ItemServices instance] getItemList:0 class_id:aclass.class_id.integerValue sort_id:sort.sort_id.integerValue class_tag:aclass.class_tag item_name:sort.sort_name];
//        srtvc.datasource = [response.item_list mutableCopy];
//    } complete:^{
//    } text:@"请稍侯"];
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    AClass* aclass = [self.classes objectAtIndex:self.selectedIndex];
    NSArray* sorts = [self.subCates objectForKey:aclass.class_id];
    Sort *sort = [sorts objectAtIndex:indexPath.item];

    HotKeyCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:CollectionIdentifier forIndexPath:indexPath];
    cell.label.text = sort.sort_name;
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}


#pragma mark - search bar delegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    SearchResultTableViewController* srtvc =  [SearchResultTableViewController getInstanceWithSameNibName];
    srtvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:srtvc animated:YES];
    srtvc.item_name = searchBar.text;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    searchBar.text = nil;
}

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    UIViewController* controller = [HotKeySearchViewController getInstanceWithSameNibName];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    return NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
