//
//  MyOrderTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Collect.h"
#import "BaseTableViewCell.h"
@class MyFavTableViewCell;
@protocol MyFavTableViewCellDelegate<NSObject>
-(void)didClickDeleteCollect:(MyFavTableViewCell*)cell;
@end
@interface MyFavTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (nonatomic,strong) id<MyFavTableViewCellDelegate> delegate;
@property (nonatomic,strong) Collect* collect;
@end
