//
//  MyOrderTableViewCell.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "MyFavTableViewCell.h"

@implementation MyFavTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCollect:(Collect *)collect{
    _collect = collect;
    [self.image sd_setImageWithURL:[NSURL URLWithString:_collect.small_pic relativeToURL:[NSURL URLWithString:OBJC_IMGBASE_URL]]];
    [self.stateLabel setText:_collect.status];
    [self.titleLabel setText:_collect.item_name];
    [self.priceLabel setText:_collect.mall_price];
}
- (IBAction)clickDeleta:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didClickDeleteCollect:)]) {
        [self.delegate didClickDeleteCollect:self];
    }
}

@end
