//
//  MyOrderTableViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "MyFavTableViewController.h"
#import "MJRefresh.h"
#import "CollectServices.h"
#import "ItemServices.h"
#import "MyFavTableViewCell.h"
#import "ItemRevealController.h"
static NSString* const MYFAV_IDENTIFIER = @"fav_identfier";
@interface MyFavTableViewController ()<MyFavTableViewCellDelegate>
@property(nonatomic,strong) NSMutableArray* myFavs;
@property(nonatomic,assign) NSUInteger nextFirstRow;
@property(nonatomic,strong) GetCollectListResponse* favResponse;
@end

@implementation MyFavTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的收藏";
    [self.tableView registerNib:[MyFavTableViewCell nib] forCellReuseIdentifier: MYFAV_IDENTIFIER];
    __weak MyFavTableViewController* ws = self;
    [self.tableView addHeaderWithCallback:^{
        [APP runBlockInInitialQ:^{
            [ws webGetFavs];
            [APP runBlockInMainQ:^{
                [ws.tableView headerEndRefreshing];
                [ws.tableView reloadData];
            }];
        }];
    }];
    
    [self.tableView headerBeginRefreshing];
     self.clearsSelectionOnViewWillAppear = YES;

    [self.tableView addFooterWithCallback:^{
        [APP runBlockInInitialQ:^{
            [ws webrequestOfNextPage];
            [APP runBlockInMainQ:^{
                [ws.tableView footerEndRefreshing];
                [ws.tableView reloadData];
            }];
        }];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.myFavs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyFavTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MYFAV_IDENTIFIER forIndexPath:indexPath];
    [cell setCollect:self.myFavs[indexPath.row]];
    cell.delegate = self;
    return cell;
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 30.0)];
    [label setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
    NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithString:@"共收藏" attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
    int num = self.favResponse.total_num.integerValue;
    [string appendAttributedString:[[NSAttributedString alloc] initWithString:@(num).stringValue attributes:@{NSForegroundColorAttributeName:[UIColor redColor]}]];
    label.attributedText = string;
    return label;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30.0;
}

-(void)webGetFavs{
    self.nextFirstRow = 0;
    GetCollectListResponse* response = [[CollectServices instance] getCollectList:0];
    self.favResponse = response;
    self.myFavs = [response.collect_list mutableCopy];
    self.nextFirstRow = response.nextFirstRow;
}

-(void)webrequestOfNextPage{
    GetCollectListResponse* response = [[CollectServices instance] getCollectList:self.nextFirstRow];
    self.myFavs = [response.collect_list mutableCopy];
    self.nextFirstRow = response.nextFirstRow;
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70.0;
}

-(void)didClickDeleteCollect:(MyFavTableViewCell *)cell{
    NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
    [MBProgressHUD showAddViewWhen:self.view when:^{
        Collect* collect = [self.myFavs objectAtIndex:indexPath.row];
        [[CollectServices instance] cancelCollectFrom:collect.item_id.intValue];
    } complete:^{
        [self.myFavs removeObject:self.myFavs[indexPath.row]];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView reloadData];
    } text:@"删除中"];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Collect* fav = [[self myFavs] objectAtIndex:indexPath.row];
    __block Item* item = nil;
    [MBProgressHUD showAddViewWhen:self.view when:^{
        GetItemInfoResponse* response = [[ItemServices instance] getItemInfo:fav.item_id.integerValue];
        item = response.data;
        item.item_id = fav.item_id;
    } complete:^{
        if (item!=nil) {
            ItemRevealController* irc = [[ItemRevealController alloc] initWithNibName:[ItemRevealController nibName] bundle:nil];
            irc.item = item;
            [self.navigationController pushViewController:irc animated:YES];
        }
    } text:@"请稍等"];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Getter
-(NSMutableArray *)myFavs{
    if (!_myFavs) {
        _myFavs = [NSMutableArray array];
    }
    return _myFavs;
}

@end
