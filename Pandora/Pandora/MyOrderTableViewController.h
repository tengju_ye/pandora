//
//  MyOrderTableViewController.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum{
    Order_SegIdx_ALL=0,
    Order_SegIdx_UnPay=1,
    Order_SegIdx_UnDeliver=2,
    Order_SegIdx_UnCheck=3
}Order_SegIdx;
@interface MyOrderTableViewController : UITableViewController
@property(nonatomic,assign) Order_SegIdx defaultShowItem;//默认进入显示的选项卡
@end
