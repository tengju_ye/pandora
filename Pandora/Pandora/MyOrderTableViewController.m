//
//  MyOrderTableViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "MyOrderTableViewController.h"
#import "HMSegmentedControl.h"
#import "OrderServices.h"
#import "OrderTableViewCell.h"
#import "DetailOrderTableViewController.h"
#import "PayOrderViewController.h"
#import "UIAlertView+MyBlocks.h"

@interface MyOrderTableViewController ()<UISearchBarDelegate,OrderTableViewCellDelegate>
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *segContainer;
@property (nonatomic,strong) HMSegmentedControl* seg;
@property (nonatomic,assign) NSUInteger selectedIndex;
@property (nonatomic,strong) NSMutableDictionary* orderDict;
@property (nonatomic,strong) NSString* searchText;
@property (nonatomic,assign) NSUInteger nextFirstRow;
@end

static NSString* const OrderCellIdentifier = @"orderCell";

@implementation MyOrderTableViewController


-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    if (self =[super initWithNibName:nibNameOrNil bundle:nil]) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"我的订单";
//    self.searchBar.delegate = self;
    [self.tableView registerNib:[OrderTableViewCell nib] forCellReuseIdentifier:OrderCellIdentifier];
    self.orderDict = [NSMutableDictionary dictionary];
    [self.tableView setTableHeaderView:self.headerView];
    self.seg = [[HMSegmentedControl alloc] initWithFrame:self.segContainer.bounds];
    [self.seg setSectionTitles:@[@"全部订单",@"待支付",@"待发货",@"待确认"]];
    [self.seg setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleWidth|
     UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|
     UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleBottomMargin];
    [self.seg setSelectionIndicatorColor:[UIColor redColor]];
    [self.seg setSelectionIndicatorHeight:3.0];
    [self.seg setSelectionIndicatorLocation:HMSegmentedControlSelectionIndicatorLocationDown];
    [self.segContainer addSubview:self.seg];
    
    __weak MyOrderTableViewController* ws = self;
    self.seg.selectedSegmentIndex = self.defaultShowItem;
    self.selectedIndex = self.defaultShowItem;
    [self.seg setIndexChangeBlock:^(NSInteger index) {
        ws.selectedIndex = index;
        [ws.tableView headerBeginRefreshing];
    }];
    
    
    [self.tableView addHeaderWithCallback:^{
        [APP runBlockInInitialQ:^{
            [ws webrequestOfCurrentType];
            [APP runBlockInMainQ:^{
                [ws.tableView headerEndRefreshing];
                [ws.tableView reloadData];
            }];
        }];
    }];
    
    
    
    [self.tableView addFooterWithCallback:^{
        [APP runBlockInInitialQ:^{
            [ws webrequestOfNextPage];
            [APP runBlockInMainQ:^{
                [ws.tableView footerEndRefreshing];
                [ws.tableView reloadData];
            }];
        }];
    }];
    
}




-(void)viewWillAppear:(BOOL)animated{
    [self.tableView headerBeginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.orderDict objectForKey:@(self.selectedIndex)] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:OrderCellIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    Order* order = [[self.orderDict objectForKey:@(self.selectedIndex)] objectAtIndex:indexPath.row];
    [cell setOrder:order];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80.0;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailOrderTableViewController *detailViewController = [[DetailOrderTableViewController alloc] initWithNibName:[DetailOrderTableViewController nibName] bundle:nil];
    Order* order = [[self.orderDict objectForKey:@(self.selectedIndex)] objectAtIndex:indexPath.row];
    detailViewController.order = order;
    [self.navigationController pushViewController:detailViewController animated:YES];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



#pragma mark - webrequest
-(void)webrequestOfCurrentType{
    self.nextFirstRow = 0;
    NSUInteger index = self.selectedIndex;
    Order_Status status = Order_Status_All;
    switch (index) {
        case Order_SegIdx_ALL:
            status = Order_Status_All;
            break;
        case Order_SegIdx_UnPay:
            status = Order_Status_Prepay;
            break;
        case Order_SegIdx_UnDeliver:
            status = Order_Status_Payed;
            break;
        case Order_SegIdx_UnCheck:
            status = Order_Status_Delivered;
        default:
            break;
    }
    
    //    if (self.searchText.length!=0) {
    //        [[OrderServices instance] get]
    //    }else{
#warning 订单搜索接口没有
    GetOrderListResponse* list = [[OrderServices instance] getOrderList:status firstRow:0];
    self.nextFirstRow = list.NextFirstRow.integerValue;
    if (list.order_list) {
        [self.orderDict setObject:[list.order_list mutableCopy] forKey:@(index)];
        
    }
    //    }
}

-(void)webrequestOfNextPage{
    NSUInteger index = self.selectedIndex;
    Order_Status status = Order_Status_All;
    switch (index) {
        case Order_SegIdx_ALL:
            status = Order_Status_All;
            break;
        case Order_SegIdx_UnPay:
            status = Order_Status_Prepay;
            break;
        case Order_SegIdx_UnDeliver:
            status = Order_Status_Payed;
            break;
        case Order_SegIdx_UnCheck:
            status = Order_Status_Delivered;
        default:
            break;
    }

#warning 订单搜索接口没有
    GetOrderListResponse* list = [[OrderServices instance] getOrderList:status firstRow:0];
    self.nextFirstRow = list.NextFirstRow.integerValue;
    if (list.order_list) {
        NSMutableArray* arr = self.orderDict[@(index)];
        [arr addObjectsFromArray:list.order_list];
    }
}


#pragma mark - celldelegate
-(void)didClickOrderBtn:(OrderTableViewCell*)cell{
    if (cell.order.order_status.integerValue == Order_Status_Delivered) {
        
        [UIAlertView showAlertViewWithTitle:@"要确认吗？" message:nil cancelButtonTitle:@"取消" otherButtonTitles:@[@"确认"] onDismiss:^(int buttonIndex, UIAlertView *alertView) {
            if (buttonIndex==0) {
                [MBProgressHUD showAddViewWhen:self.view when:^{
                    ConfirmOrderResponse* response = [[OrderServices instance]confirmOrder:cell.order.order_id];
                    if ([response.code isEqualToString:OBJC_SUCCEED_CODE]) {
                        [APP runBlockInMainQ:^{
                            [SVProgressHUD showSuccessWithStatus:@"确认成功"];
                            [self.tableView headerBeginRefreshing];
                        }];
                    }else{
                        [APP runBlockInMainQ:^{
                            [SVProgressHUD showSuccessWithStatus:@"确认失败"];
                        }];
                    }
                    
                } complete:^{
                    
                } text:@"正在确认中"];
                
            }
        } onCancel:^{
            
        }];
    }else{
        PayOrderViewController* poc = [[PayOrderViewController alloc] initWithNibName:[PayOrderViewController nibName] bundle:nil];
        poc.orderID = cell.order.order_id;
        [self.navigationController pushViewController:poc animated:YES];
        
    }
}

#pragma mark - search bar
-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    DLog(@"%@",@"search");
    self.searchText = searchBar.text;
    [self.tableView headerBeginRefreshing];
}

@end
