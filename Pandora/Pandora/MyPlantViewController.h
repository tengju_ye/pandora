//
//  MyPlantViewController.h
//  Pandora
//
//  Created by junchen on 12/5/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPlantViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *growingLabel;
@property (weak, nonatomic) IBOutlet UILabel *grownLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,assign) BOOL pushToValidIfExist;//是否要自动弹入到可用的植物
@property int state;

@end
