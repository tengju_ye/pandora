//
//  MyPlantViewController.m
//  Pandora
//
//  Created by junchen on 12/5/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "MyPlantViewController.h"
#import "PlanterSeedServices.h"
#import "PlanterSeed.h"
#import "PlantDetailTableViewCell.h"
#import "GrowHistoryViewController.h"
#import "SeedStateViewController.h"
#import "tools.h"
#import "UIAlertView+MyBlocks.h"
#import "PlanterServices.h"
#import "RightMenuViewController.h"
@interface MyPlantViewController ()

@property (strong, nonatomic) NSMutableArray *growingList;
@property (strong, nonatomic) NSMutableArray *grownList;
@property(nonatomic,assign) int nextFirstRow;
@property(nonatomic,assign) int nextFirstRowGrown;

//0:growing 1:grown

@end

@implementation MyPlantViewController

- (void) viewDidAppear:(BOOL)animated {
    
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    hud.removeFromSuperViewOnHide = YES;
    [self.tableView addSubview:hud];
    __block GetPlanterSeedListResponse *growingResponse = nil;
    __block GetPlanterSeedListResponse *grownResponse = nil;
    [hud showAnimated:YES whileExecutingBlock:^{
        self.nextFirstRow = 0;
        growingResponse = [[PlanterSeedServices instance] getPlanterSeedList:PlanterState_ING firstRow:0];
        grownResponse = [[PlanterSeedServices instance] getPlanterSeedList:PlanterState_DONE firstRow:0];
        self.growingList = [growingResponse.planter_seed_list mutableCopy];
        self.grownList = [grownResponse.planter_seed_list mutableCopy];
        self.nextFirstRowGrown = grownResponse.nextFirstRow;
        self.nextFirstRow = growingResponse.nextFirstRow;
    } completionBlock:^{
        if (self.state == 0) {
            [self.growingLabel setTextColor:[UIColor redColor]];
            [self.grownLabel setTextColor:[UIColor grayColor]];
        } else {
            [self.growingLabel setTextColor:[UIColor grayColor]];
            [self.grownLabel setTextColor:[UIColor redColor]];
        }
        self.growingLabel.text = [NSString stringWithFormat:@"进行中%d", growingResponse.total_num.intValue];
        self.grownLabel.text = [NSString stringWithFormat:@"已完成%d", grownResponse.total_num.intValue];
        [self.tableView reloadData];
        
        
        if (self.growingList.count!=0) {
            if (self.pushToValidIfExist) {
                [MBProgressHUD showAddViewWhen:self.view when:^{
                    GetPlanterInfoResponse* infoResponse = [[PlanterServices instance] getPlanterInfo];
                    SeedStateViewController *newController = [SeedStateViewController getInstanceWithSameNibName];
                    newController.planterSeedId = infoResponse.planter_seed_id;
                    newController.seedId = infoResponse.seed_id.intValue;
                    newController.name = infoResponse.planter_name;
                    [APP runBlockInMainQ:^{
                        [self.navigationController pushViewController:newController animated:YES];
                    }];
                } text:@"请稍等"];
            }
        }else{
            [UIAlertView showAlertViewWithTitle:@"您暂时没有植物" message:@"请去选择您要种植的植物" cancelButtonTitle:@"好的" otherButtonTitles:nil onDismiss:^(int buttonIndex, UIAlertView *alertView) {
                
            } onCancel:^{
                RightMenuViewController* right = (RightMenuViewController*)self.mm_drawerController.rightDrawerViewController;
                [right selectedIdx:kPlantChoiceIdx];
            }];
        }
        
    }];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的植物";
    [self.tableView setTableFooterView:[[UIView alloc] init]];
    [self.tableView registerNib:[UINib nibWithNibName:@"PlantDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"plantDetailCell"];
    [tools addRightBarItem:self];
    
    __weak MyPlantViewController* ws = self;
    [self.tableView addFooterWithCallback:^{
        [APP runBlockInInitialQ:^{
            [ws webrequestOfNextPage];
            [APP runBlockInMainQ:^{
                [ws.tableView footerEndRefreshing];
                [ws.tableView reloadData];
            }];
        }];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.state == 0) {
        return [self.growingList count];
    } else {
        return [self.grownList count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 88;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PlantDetailTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"plantDetailCell" forIndexPath:indexPath];
    if (self.state == 0) {
        PlanterSeed *planterSeed = [self.growingList objectAtIndex:[indexPath row]];
        cell.name.text = planterSeed.item_name;
        [NSString stringWithFormat:@"开始时间：%@", planterSeed.plant_time];
        cell.state.text = [NSString stringWithFormat:@"状态：%@", planterSeed.state];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://smartplant.pandorabox.mobi%@", planterSeed.base_pic]];
        [cell.image sd_setImageWithURL:url];
    } else {
        PlanterSeed *planterSeed = [self.grownList objectAtIndex:[indexPath row]];
        cell.name.text = planterSeed.item_name;
        cell.time.text = [NSString stringWithFormat:@"开始时间：%@", planterSeed.plant_time];
        cell.state.text = [NSString stringWithFormat:@"状态：%@", planterSeed.state];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://smartplant.pandorabox.mobi%@", planterSeed.base_pic]];
        [cell.image sd_setImageWithURL:url];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.state == 0) {
        SeedStateViewController *newController = [[SeedStateViewController alloc] init];
        PlanterSeed *seed = [self.growingList objectAtIndex:[indexPath row]];
        newController.planterSeedId = seed.planter_seed_id;
        newController.seedId = [seed.item_id intValue];
        newController.name = seed.item_name;
        [self.navigationController pushViewController:newController animated:YES];
    } else {
        PlanterSeed *seed = [self.grownList objectAtIndex:[indexPath row]];
        NSArray<PlanterSeedLog> *logList = [[[PlanterSeedServices instance] getPlanterSeedLogList:[seed.planter_seed_id intValue]] data];
        GrowHistoryViewController *newController = [[GrowHistoryViewController alloc] init];
        newController.timeList = [[NSMutableArray alloc] init];
        newController.stateList = [[NSMutableArray alloc] init];
        for (int i = 0; i < [logList count]; i++) {
            PlanterSeedLog *log = [logList objectAtIndex:i];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:[log.addtime intValue]];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *time = [formatter stringFromDate:date];
            [newController.timeList addObject:time];
            NSString *state = log.state;
            [newController.stateList addObject:state];
        }
        [self.navigationController pushViewController:newController animated:YES];
    }
}

- (IBAction)changeToGrowing:(id)sender {
    self.state = 0;
    [self.growingLabel setTextColor:[UIColor redColor]];
    [self.grownLabel setTextColor:[UIColor grayColor]];
    [self.tableView reloadData];
}

- (IBAction)changeToGrown:(id)sender {
    self.state = 1;
    [self.growingLabel setTextColor:[UIColor grayColor]];
    [self.grownLabel setTextColor:[UIColor redColor]];
    [self.tableView reloadData];
}

-(void)webrequestOfNextPage{
    GetPlanterSeedListResponse *growingResponse = [[PlanterSeedServices instance] getPlanterSeedList:1 firstRow:self.nextFirstRow];
    GetPlanterSeedListResponse *grownResponse = [[PlanterSeedServices instance] getPlanterSeedList:2 firstRow:self.nextFirstRowGrown];
    [self.growingList addObjectsFromArray:growingResponse.planter_seed_list] ;
    [self.grownList addObjectsFromArray:grownResponse.planter_seed_list];
    self.nextFirstRow = growingResponse.nextFirstRow;
    self.nextFirstRowGrown = grownResponse.nextFirstRow;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
