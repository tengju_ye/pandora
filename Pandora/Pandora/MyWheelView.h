//
//  MyWheelView.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/19.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyWheelViewDelegate <NSObject>
@required
-(int)wheelViewNumofPages;
-(NSString*)stringOfPage:(int)page;
@optional
-(void)switchToPages:(int)page;
@end

@interface MyWheelView : UIView
@property(nonatomic,strong) UIColor* color;
@property(nonatomic,strong) id<MyWheelViewDelegate> delegate;

-(int)currentSelectedIndex;
-(void)reloadData;
-(void)showLastPage;
-(void)showNextPage;
@end
