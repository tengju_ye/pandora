//
//  MyWheelView.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/19.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "MyWheelView.h"

typedef enum{
    CurrentTop_Center=0,
    CurrentTop_Left=1,
    CurrentTop_Right=2
} CurrentTop;
@interface MyWheelView()
@property (nonatomic,strong) NSArray* prepareViews;
@property (nonatomic,strong) UILabel* showView;
@property (nonatomic,assign) CurrentTop currentTop;//
//@property (nonatomic,strong) void (^animationBlock)(void);
@property(nonatomic,assign) int currentIdx;

@end

@implementation MyWheelView

-(instancetype)init{
    if (self = [super init]) {
        self.color = DEFAULT_GREEN;
        NSMutableArray* ma  = [NSMutableArray array];
        for (int i=0; i<4; i++) {
            UILabel* label = [[UILabel alloc] init];
            [label setTextColor:[UIColor whiteColor]];
            [label setMinimumScaleFactor:0.5];
            [label setBackgroundColor:self.color];
            [label setTextAlignment:NSTextAlignmentCenter];
            [ma addObject:label];
        }
        UILabel* label = [[UILabel alloc] init];
        [label setTextColor:[UIColor whiteColor]];
        [label setMinimumScaleFactor:0.5];
        [label setBackgroundColor:self.color];
        [label setTextAlignment:NSTextAlignmentCenter];
        self.showView = label;
        
        self.prepareViews = ma;
        [self setClipsToBounds:YES];
    }
    return self;
}

//-(instancetype)initWithFrame:(CGRect)frame{
//    if (self = [super initWithFrame:frame]) {
//        self.color = [UIColor greenColor];
//        NSMutableArray* ma  = [NSMutableArray array];
//        for (int i=0; i<4; i++) {
//            UILabel* label = [[UILabel alloc] init];
//            [label setBackgroundColor:self.color];
//            [ma addObject:label];
//        }
//        self.prepareViews = ma;
//    }
//    return self;
//}



-(void)layoutSubviews{
    [super   layoutSubviews];
    
    for (UIView* view  in self.prepareViews) {
        [view removeFromSuperview];
    }
    [self.showView removeFromSuperview];
    int total = [self.delegate wheelViewNumofPages];
    if (total<=0) {
        return;
    }
    
    
    CGRect bounds = self.bounds;
    CGRect eachBounds = CGRectMake(-CGRectGetWidth(bounds)/2, 0, CGRectGetWidth(bounds)/2, CGRectGetHeight(bounds));
    
    //最左
    int needIdx = [self lastPageIdxOf:[self lastPageIdx]];
    UILabel* label = [self.prepareViews objectAtIndex:0];
    [label setFrame:eachBounds];
    [self configLabel:label ofIndex:needIdx];
    [self addSubview:label];
    
    //次左
    needIdx = [self lastPageIdx];
    eachBounds.origin.x+=eachBounds.size.width;
    label = [self.prepareViews objectAtIndex:1];
    [label setFrame:eachBounds];
    [self configLabel:label ofIndex:needIdx];
    [self addSubview:label];
    
    //次右
    needIdx = [self nextPageIdx];
    eachBounds.origin.x+=eachBounds.size.width;
    label = [self.prepareViews objectAtIndex:2];
    [label setFrame:eachBounds];
    [self configLabel:label ofIndex:needIdx];
    [self addSubview:label];
    
    
    //最右
    needIdx = [self nextPageIdxOf:[self nextPageIdx]];
    eachBounds.origin.x+=eachBounds.size.width;
    label = [self.prepareViews objectAtIndex:3];
    [label setFrame:eachBounds];
    [self configLabel:label ofIndex:needIdx];
    [self addSubview:label];
    
    
    //中间
    self.showView.frame = CGRectMake(CGRectGetWidth(eachBounds)/2, 0, CGRectGetWidth(eachBounds), CGRectGetHeight(eachBounds));
    [self configLabel:self.showView ofIndex:self.currentIdx];
    [self addSubview:self.showView];
    
    switch (self.currentTop) {
        case CurrentTop_Center:
            [self bringSubviewToFront:self.showView];
            break;
        case CurrentTop_Left:
            [self bringSubviewToFront:[[self prepareViews] objectAtIndex:1]];
            break;
        case CurrentTop_Right:
            [self bringSubviewToFront:[[self prepareViews] objectAtIndex:2]];
            break;
        default:
            [self bringSubviewToFront:self.showView];
            break;
    }
}

-(void)reloadData{
    [self setNeedsLayout];
}


-(void)configLabel:(UILabel*)label ofIndex:(int)index{
    NSString* string = @"";
    if ([self.delegate respondsToSelector:@selector(stringOfPage:)]) {
        string =[self.delegate stringOfPage:index];
    }
    if (index==self.currentIdx) {
        [[label layer] setShadowColor:[UIColor colorWithWhite:0.285 alpha:1.000].CGColor];
        [[label layer] setShadowRadius:5];
        [[label layer] setShadowOpacity:0.9];
    }
    label.text = string;
    [label setBackgroundColor:self.color];
}

//->
-(void)showLastPage{
    self.currentTop  = CurrentTop_Left;
    __weak MyWheelView* ws = self;
    [self bringSubviewToFront:[ws.prepareViews objectAtIndex:1]];
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.5 animations:^{
        //2->3
        CGRect original = [[ws.prepareViews objectAtIndex:2] frame];
        CGRect wantTo = [[ws.prepareViews objectAtIndex:3] frame];
        [[ws.prepareViews objectAtIndex:2] setFrame:wantTo];
        
        //current->2
        wantTo = original;
        original = ws.showView.frame;
        [ws.showView setFrame:wantTo];
        
        
        //1->current
        wantTo = original;
        original  = [[ws.prepareViews objectAtIndex:1] frame];
        [[ws.prepareViews objectAtIndex:1] setFrame:wantTo];
        
        
        //0->1
        wantTo = original;
        [[ws.prepareViews objectAtIndex:0] setFrame:wantTo];
    } completion:^(BOOL finished) {
        ws.currentIdx = [ws lastPageIdx];
        ws.currentTop  = CurrentTop_Center;
        [ws setNeedsLayout];
        if ([ws.delegate respondsToSelector:@selector(switchToPages:)]) {
            [ws.delegate switchToPages:ws.currentIdx];
        }
    }];
    
}

//<-
-(void)showNextPage{
    self.currentTop  = CurrentTop_Right;
    __weak MyWheelView* ws = self;
    [self bringSubviewToFront:[ws.prepareViews objectAtIndex:2]];
    [self layoutIfNeeded];
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect wantTo = [[ws.prepareViews objectAtIndex:0] frame];
        CGRect original = [[ws.prepareViews objectAtIndex:1] frame];
        [[ws.prepareViews objectAtIndex:1] setFrame:wantTo];
        
        
        wantTo = original;
        original = ws.showView.frame;
        [ws.showView setFrame:wantTo];
        
        
        wantTo = original;
        original  = [[ws.prepareViews objectAtIndex:2] frame];
        [[ws.prepareViews objectAtIndex:2] setFrame:wantTo];
        
        
        wantTo = original;
        [[ws.prepareViews objectAtIndex:3] setFrame:wantTo];
    } completion:^(BOOL finished) {
        ws.currentIdx = [ws nextPageIdx];
        ws.currentTop  = CurrentTop_Center;
        [ws setNeedsLayout];
        if ([ws.delegate respondsToSelector:@selector(switchToPages:)]) {
            [ws.delegate switchToPages:ws.currentIdx];
        }
        
    }];
    
    
}

-(int)lastPageIdx{
    return [self lastPageIdxOf:self.currentIdx];
}

-(int)lastPageIdxOf:(int)page{
    int total = [self.delegate wheelViewNumofPages];
    int last = page-1;
    if (last<0) {
        last+=total;
    }
    return last;
}

-(int)nextPageIdx{
    return [self nextPageIdxOf:self.currentIdx];
}

-(int)nextPageIdxOf:(int)page{
    int total = [self.delegate wheelViewNumofPages];
    int next = page+1;
    if (next>=total) {
        next-=total;
    }
    return next;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch* touch = [touches anyObject];
    CGPoint point =  [touch locationInView:self];
    if (point.x<CGRectGetMidX(self.bounds)) {
        [self showNextPage];
    }else{
        [self showLastPage];
    }
}


-(int)currentSelectedIndex{
    return self.currentIdx;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
