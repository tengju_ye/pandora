//
//  NGDefine.h
//  sanyecao
//
//  Created by ng on 13-12-24.
//  Copyright (c) 2013年 ng. All rights reserved.
//

#ifndef NGDefine_h
#define NGDefine_h

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define IOS6 [[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0f
#define IOS7 [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f

#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define DEFAULT_GREEN  RGBCOLOR(80, 220 , 80) //默认的绿色色调
#define DEFAULT_RED  RGBCOLOR(220, 95 , 92) //默认的红色色调


#define iPhone5Frame CGRectMake(0,0,320,568)
#define iPhone4Frame CGRectMake(0,0,320,480)


//some methods to figure out frame
#define LeftDownY(frame) (frame.origin.y + frame.size.height)
#define HeightOf(frame) frame.size.height
#define WidthOf(frame)  frame.size.width

#define userDefaults  [NSUserDefaults standardUserDefaults]
#define userDefaultsSet(key,value)  [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];[[NSUserDefaults standardUserDefaults]synchronize]
#define ARC4RANDOM_MAX 0x100000000
#define RAND0_1   (arc4random()%10)/10.0


#ifdef DEBUG
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define DLog(...)
#endif

#define MAIN_QUEUE dispatch_get_main_queue()

#endif
