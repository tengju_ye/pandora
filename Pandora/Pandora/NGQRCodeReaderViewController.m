//
//  NGQRCodeReaderViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 15/1/22.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "NGQRCodeReaderViewController.h"

@interface NGQRCodeReaderViewController ()
@property (nonatomic,strong) UIImageView* scanBgView;
@end

@implementation NGQRCodeReaderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage* image = [UIImage imageNamed:@"pick_bg"];
    UIImageView* imageview = [[UIImageView alloc] initWithImage:image];
    [imageview setTranslatesAutoresizingMaskIntoConstraints:NO];

    [self.view addSubview:imageview];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:imageview attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:imageview attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[imageview(wid)]" options:0 metrics:@{@"wid":@(image.size.width)} views:NSDictionaryOfVariableBindings(imageview)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[imageview(hei)]" options:0 metrics:@{@"hei":@(image.size.height)} views:NSDictionaryOfVariableBindings(imageview)]];
    self.scanBgView = imageview;
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self.scanBgView superview] bringSubviewToFront:self.scanBgView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
