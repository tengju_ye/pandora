//
//  NSString+Extension.h
//  ChaoJuEyes
//
//  Created by hzyd on 13-7-10.
//  Copyright (c) 2013年 hzyd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)
+(BOOL)isEmptyOrNil:(NSString*) string;
-(id)jsonValue;

-(NSString *)appendForPath;

+(NSString *)getFilePathWithDir:(NSString *)aDirectory file:(NSString *)aFilename;
@end
