//
//  NSString+Extension.m
//  ChaoJuEyes
//
//  Created by hzyd on 13-7-10.
//  Copyright (c) 2013年 hzyd. All rights reserved.
//

#import "NSString+Extension.h"
@implementation NSString (Extension)
+(BOOL)isEmptyOrNil:(NSString *)string
{
    return !string || ![string length];
}


-(id)jsonValue{
    NSError* err;
    id value = [NSJSONSerialization JSONObjectWithData:[self dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&err];
    if (err) {
        return nil;
    }
    return value;
}

-(NSString *)appendForPath
{
	
	NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:self];
	
	return path;
}
+(NSString *)getFilePathWithDir:(NSString *)aDirectory file:(NSString *)aFilename
{
	NSString *dirpath = [aDirectory appendForPath];
	
	NSFileManager *manager = [NSFileManager defaultManager];
	if (![manager fileExistsAtPath:dirpath]) {
		[manager createDirectoryAtPath:dirpath withIntermediateDirectories:YES attributes:nil error:nil];
	}
	
	return [dirpath stringByAppendingPathComponent:aFilename];
	
}
@end
