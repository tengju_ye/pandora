//
//  OrderCancelTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/5.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
@class OrderCancelTableViewCell ;
@protocol OrderCancelTableViewCellDelegate<NSObject>
@optional
-(void)didTouchPay:(OrderCancelTableViewCell*)cell;
-(void)didTouchCancel:(OrderCancelTableViewCell*)cell;


@end
@interface OrderCancelTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIButton *payBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (nonatomic,strong) id<OrderCancelTableViewCellDelegate> delegate;

@end
