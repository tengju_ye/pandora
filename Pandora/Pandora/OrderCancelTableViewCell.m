//
//  OrderCancelTableViewCell.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/5.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "OrderCancelTableViewCell.h"

@implementation OrderCancelTableViewCell
- (IBAction)pay:(id)sender {
    if([self.delegate respondsToSelector:@selector(didTouchPay:)]){
        [self.delegate didTouchPay:self];
    }
}
- (IBAction)cancel:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didTouchCancel:)]) {
        [self.delegate didTouchCancel:self];
    }
}

- (void)awakeFromNib {
    [[self.payBtn layer] setCornerRadius:2.0];
    [self.payBtn setClipsToBounds:YES];
    [[self.cancelBtn layer] setCornerRadius:2.0];
    [self.cancelBtn setClipsToBounds:YES];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
