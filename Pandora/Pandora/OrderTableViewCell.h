//
//  OrderTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/5.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "BaseTableViewCell.h"
@class OrderTableViewCell;
@protocol OrderTableViewCellDelegate<NSObject>
-(void)didClickOrderBtn:(OrderTableViewCell*)cell;
@end
@interface OrderTableViewCell : BaseTableViewCell
@property(nonatomic,strong)id<OrderTableViewCellDelegate> delegate;
@property(nonatomic,strong)Order* order;
@end
