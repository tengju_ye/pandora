//
//  OrderTableViewCell.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/5.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "OrderTableViewCell.h"
#import "OrderServices.h"
@interface OrderTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@end


@implementation OrderTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [[self.btn layer] setCornerRadius:2.0];
    [self.btn setClipsToBounds:YES];
}
- (IBAction)clickBtn:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didClickOrderBtn:)]) {
        [self.delegate   didClickOrderBtn:self];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setOrder:(Order *)order{
    _order = order;
    [self.btn setHidden:NO];

    switch (order.order_status.integerValue) {
        case Order_Status_Confirmed:{
            [self.btn setHidden:YES];
        }
            break;
        case Order_Status_Delivered:{
            [self.btn setTitle:@"去确认" forState:UIControlStateNormal];
        }
            break;
        case Order_Status_Payed:{
            [self.btn setHidden:YES];
        }
            break;
        case Order_Status_Prepay:{
            [self.btn setTitle:@"去付款" forState:UIControlStateNormal];
        }
            break;
        default:{
            [self.btn setHidden:YES];
        }
            break;
    }
    
    NSMutableAttributedString* as = [[NSMutableAttributedString alloc] initWithString:@"状\t态: " attributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    [as appendAttributedString:[[NSAttributedString alloc] initWithString:order.order_status_name attributes:@{NSForegroundColorAttributeName:[UIColor redColor]}]];
    [self.stateLabel setAttributedText:as];
    
    as = [[NSMutableAttributedString alloc] initWithString:@"总\t价: " attributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    [as appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%.2f",_order.pay_amount.floatValue] attributes:@{NSForegroundColorAttributeName:[UIColor redColor]}]];
    [self.priceLabel setAttributedText:as];
    NSString* date= [APP dateStringWithDefaultFormatForStamp:_order.addtime.doubleValue];
    [self.timeLabel setText:[NSString stringWithFormat:@"下单时间: %@",date]];
}


@end
