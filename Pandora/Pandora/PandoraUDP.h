//
//  PandoraUDP.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/18.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncUdpSocket.h"

@interface PandoraUDP : NSObject<GCDAsyncUdpSocketDelegate>

-(instancetype)initWithWifiName:(NSString*)WifiName passWord:(NSString*)password;

-(void)sendCMDs:(void(^)(BOOL success)) complete;

@end
