//
//  PandoraUDP.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/18.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "PandoraUDP.h"
#import "UIAlertView+MyBlocks.h"

#define RemotePort 988
#define LocalPort 2000
#define MulticastHost  @"192.168.2.1"
/*
 UDP 对象标记
 */
#define TAG_SEND 77
#define TAG_RECEIVE 88

@interface PandoraUDP(){
    long tag;
    GCDAsyncUdpSocket* udpSocket;
}
@property(nonatomic,strong) NSArray* Cmd;

/**
 *  UDP 对象
 */
//@property (nonatomic, strong) GCDAsyncUdpSocket   *sendUdpSocket;
//@property(nonatomic,strong)GCDAsyncUdpSocket   *receiveMsgSocket;

@property(nonatomic,assign) BOOL canContinue;//是否可以继续发送指令

@property(nonatomic,strong) NSLock* lock;
@property(nonatomic,strong) NSString* wifiName;
@property(nonatomic,strong) NSString* password;
@property(nonatomic,strong) dispatch_semaphore_t semaphore;//信号量

@end

@implementation PandoraUDP

-(instancetype)init{
    if (self = [super init]) {
        self.canContinue = YES;
        [self setupSocket];
    }
    return self;
}


-(instancetype)initWithWifiName:(NSString *)WifiName passWord:(NSString *)password{
    if (self = [self init]) {
        self.wifiName = WifiName;
        self.password = password;
        if (self.password==nil) {
            self.password = @"";
        }
        _Cmd = nil;
        self.Cmd = self.Cmd;
    }
    return self;
}

-(void)sendCMDs:(void (^)(BOOL))complete{
//    [self sendCMDAndWait:@"123456AT+LKSTT"];
    [self testsend];
}




////通过UDP,发送消息
//-(void)sendCMDAndWait:(NSString*)cmd
//{
//    NSData *data = [cmd dataUsingEncoding:NSUTF8StringEncoding];
//    [self.sendUdpSocket sendData:data toHost:MulticastHost port:RemotePort withTimeout:-1 tag:tag++];
//}





-(NSArray *)Cmd{
    if (!_Cmd) {
        _Cmd  = @[@"123456AT+LKSTT", @"123456AT+WPRT=!0", @"123456AT+NIP=!0",
                  @"123456AT+ENCRY=!7", [NSString stringWithFormat:@"123456AT+SSID=!\"%@\"",self.wifiName],
                  [NSString stringWithFormat:@"123456AT+KEY=!1,0,\"%@\"",self.password],
                  @"123456AT+ATRM=!0,0,115.29.231.179,80", @"123456AT+PASS=!111222",
                  @"123456AT+Z" ];
    }
    return _Cmd;
}


#pragma mark - Getter



//-(GCDAsyncUdpSocket *)sendUdpSocket{
//    if (!_sendUdpSocket) {
//        dispatch_queue_t t = dispatch_queue_create("123", 0ull);
//        _sendUdpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:MAIN_QUEUE];
//   
//        //绑定端口
//        NSError *error = nil;
//        
//        if (![_sendUdpSocket bindToPort:LocalPort error:&error])
//        {
//            DLog(@"Error binding: %@", error);
//            return nil;
//        }
//        if (![_sendUdpSocket beginReceiving:&error])
//        {
//            DLog(@"Error receiving: %@", error);
//            return nil;
//        }
//        
//        if (error) {
//            [UIAlertView showAlertViewWithTitle:@"打开端口失败" message:@"请退出应用重试" cancelButtonTitle:@"好的" otherButtonTitles:nil onDismiss:^(int buttonIndex, UIAlertView *alertView) {
//                
//            } onCancel:^{
//                
//            }];
//        }
//
//    }
//    return _sendUdpSocket;
//}

-(NSLock *)lock{
    if (!_lock) {
        _lock = [[NSLock alloc] init];
    }
    return _lock;
}
-(dispatch_semaphore_t)semaphore{
    if (!_semaphore) {
        _semaphore = dispatch_semaphore_create(0);
    }
    return _semaphore;
}

#pragma mark - Setter
-(void)setCanContinue:(BOOL)canContinue{
    [self.lock lock];
    _canContinue = canContinue;
    [self.lock unlock];
}




#pragma mark - Delegate
#pragma mark -
#pragma mark UDP Delegate Methods
//- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag
//{
//    // You could add checks here
//}
//
//- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
//{
//    [self.sendUdpSocket close];
//    self.sendUdpSocket = nil;
//    // You could add checks here
//}
//
//
//-(void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext{
//    
//    NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//    DLog(@"%@",msg)
//    if (msg)
//    {
//        if ([msg containsString:@"OK"]) {
//            self.canContinue = YES;
//            dispatch_semaphore_signal(self.semaphore);
//        }
//    }
//    else
//    {
//        NSString *host = nil;
//        uint16_t port = 0;
//        [GCDAsyncUdpSocket getHost:&host port:&port fromAddress:address];
//        DLog(@"RECV: Unknown message from: %@:%hu", host, port);
//        //        [self logInfo:FORMAT(@"RECV: Unknown message from: %@:%hu", host, port)];
//    }
//    
//}
- (void)setupSocket
{
    udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_current_queue()];
    
    NSError *error = nil;
    
    if (![udpSocket bindToPort:0 error:&error])
    {
        return;
    }
    if (![udpSocket beginReceiving:&error])
    {
        return;
    }
    
}

- (void)testsend
{
    NSString *host = MulticastHost;
    if ([host length] == 0)
    {
        return;
    }
    
    int port = RemotePort;
    if (port <= 0 || port > 65535)
    {
        return;
    }
    
    NSString *msg = @"123456AT+LKSTT";
    if ([msg length] == 0)
    {
        return;
    }
    
    NSData *data = [msg dataUsingEncoding:NSUTF8StringEncoding];
    [udpSocket sendData:data toHost:host port:port withTimeout:-1 tag:tag];
    
    tag++;
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
    // You could add checks here
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
    // You could add checks here
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext
{
    NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if (msg)
    {
        DLog(@"%@",msg);
        //        [self logMessage:FORMAT(@"RECV: %@", msg)];
    }
    else
    {
        NSString *host = nil;
        uint16_t port = 0;
        [GCDAsyncUdpSocket getHost:&host port:&port fromAddress:address];
        
    }
}


//-(void)dealloc{
//    [self.sendUdpSocket close];
//}

@end
