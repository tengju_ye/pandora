//
//  PayFXForm.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/6.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXForms.h"
#define PAYWAY_ALIPAY_TITLE @"支付宝"
#define PAYWAY_WALLET_TITLE @"种植币"

@interface PayFXForm : NSObject<FXForm>
@property (nonatomic,strong) NSString* bankType;//当前支付方式
@property (nonatomic,strong) NSArray* options; //可选支付方式
@property (nonatomic,strong) NSString* orderNumber;
@property (nonatomic,strong) NSString* remain;
@property (nonatomic,strong) NSString* shouldPay;
@end
