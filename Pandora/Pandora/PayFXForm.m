//
//  PayFXForm.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/6.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "PayFXForm.h"
#import "CustomButtonCell.h"

@implementation PayFXForm
-(NSArray *)fields{
    NSMutableArray* array = [NSMutableArray array];
    if (self.options.count) {
        [array addObject: @{FXFormFieldTitle:@"支付方式",FXFormFieldKey: @"bankType",FXFormFieldType:FXFormFieldTypeOption, FXFormFieldAction:@"changeOption:",FXFormFieldOptions:self.options,FXFormFieldCell: [FXFormOptionPickerCell class]}];
    }
    [array addObject:@{FXFormFieldTitle:@"订单号:",FXFormFieldKey:@"orderNumber",FXFormFieldType:FXFormFieldTypeLabel}];
    
    if ([self.bankType isEqualToString:PAYWAY_WALLET_TITLE]) {
        [array addObject:@{FXFormFieldTitle:@"余额:",FXFormFieldKey:@"remain",FXFormFieldType:FXFormFieldTypeLabel}];
    }
    [array addObject:@{FXFormFieldTitle:@"应付:",FXFormFieldKey:@"shouldPay",FXFormFieldType:FXFormFieldTypeLabel}];

    return array;
}

-(NSArray *)extraFields{
    NSMutableArray* fields = [NSMutableArray array];
    [fields addObject:
     @{FXFormFieldHeader: @"",FXFormFieldCell: [CustomButtonCell class],FXFormFieldTitle: @"立即支付", FXFormFieldAction: @"submit:"}
     ];
    return fields;
}

@end
