//
//  PayOrderViewController.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/6.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayOrderViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableVIew;
@property (nonatomic,strong)NSString* orderID;
@property (nonatomic,strong)NSString* defaultPayway_name;
@property (nonatomic,strong)void (^completeBlock)(BOOL);
@end
