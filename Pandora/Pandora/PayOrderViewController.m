//
//  PayOrderViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/6.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "PayOrderViewController.h"
#import "PayFXForm.h"
#import "FXForms.h"
#import "Payway.h"
#import "PayServices.h"
#import "UIAlertView+MyBlocks.h"


@interface PayOrderViewController ()<FXFormControllerDelegate>
@property(nonatomic,strong) FXFormController* formController;
@property(nonatomic,strong) NSArray* payways;
@property(nonatomic,strong)GetPayInfoByOrderIdResponse_Alipay* response;
@end

@implementation PayOrderViewController

-(instancetype)init{
    if(self==[super init]){
        self.title = @"订单付款";
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    if (self == [super initWithNibName:nibNameOrNil bundle:nil]) {
        self.title = @"订单付款";
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self==[super initWithCoder:aDecoder]) {
        self.title = @"订单付款";
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self.navigationController view] setBackgroundColor:[UIColor whiteColor]];
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    self.formController = [[FXFormController alloc] init];
    self.formController.tableView = self.tableVIew;
    self.formController.delegate = self;
    if (!self.defaultPayway_name) {
        self.defaultPayway_name = PAYWAY_ALIPAY_TITLE;
    }
    [self webGetPayInfoByID];
}



-(void)webGetPayInfoByID{
    
    MBProgressHUD* hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    [hud setRemoveFromSuperViewOnHide:YES];
    [hud showAnimated:YES whileExecutingBlock:^{
        GetPayInfoByOrderIdResponse_Alipay* aliptResponse = [[PayServices instance] getPayInfoByOrderId_Alipay:self.orderID];
        self.payways = @[PAYWAY_ALIPAY_TITLE,PAYWAY_WALLET_TITLE];
        PayFXForm* form = [[PayFXForm alloc] init];
        form.options = self.payways;
        form.bankType = self.defaultPayway_name;
        form.orderNumber = self.orderID;
        form.remain = [NSString stringWithFormat:@"¥：%@元",aliptResponse.left_money];
        form.shouldPay = [NSString stringWithFormat:@"¥：%@元",aliptResponse.amount];
        self.response = aliptResponse;
        dispatch_async(MAIN_QUEUE, ^{
            self.formController.form = form;
            [self.tableVIew reloadData];
        });
    }];
    
}

- (void)submit:(UITableViewCell<FXFormFieldCell> *)cell
{
    //we can lookup the form from the cell if we want, like this:
    PayFXForm *form = (PayFXForm *)cell.field.form;
    
    if ([form.bankType isEqualToString:self.payways[0]]) {
        //支付宝
        [UIAlertView showAlertViewWithTitle:@"使用支付宝付钱" message:nil cancelButtonTitle:@"取消" otherButtonTitles:@[@"确认"] onDismiss:^(int buttonIndex, UIAlertView *alertView) {
            if (buttonIndex==0) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.response.link]];
                [self.navigationController popViewControllerAnimated:YES];
                if (self.completeBlock) {
                    self.completeBlock(YES);
                }
            }
        } onCancel:^{
            
        }];
    }else if ([form.bankType isEqualToString:self.payways[1]]){
        //种植币
        [UIAlertView showAlertViewWithTitle:@"使用种植币付钱" message:nil cancelButtonTitle:@"取消" otherButtonTitles:@[@"确认"] onDismiss:^(int buttonIndex, UIAlertView *alertView) {
            if (buttonIndex==0) {
                [APP runBlockInInitialQ:^{
                    DefaultResponse* form = [[PayServices instance] payOrderByWallet:self.orderID];
                    if ([form.code isEqualToString:OBJC_SUCCEED_CODE]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [SVProgressHUD showSuccessWithStatus:@"支付成功"];
                            [self.navigationController popViewControllerAnimated:YES];
                            if (self.completeBlock) {
                                self.completeBlock(YES);
                            }
                        });
                    }else{
                        
                    }
                }];

            }
        } onCancel:^{
            
        }];
    }else{
    
        [UIAlertView showAlertViewWithTitle:@"请选择支付方式" message:nil cancelButtonTitle:@"好的" otherButtonTitles:nil onDismiss:^(int buttonIndex, UIAlertView *alertView) {
            
        } onCancel:^{
        
        }];
    }
}

-(void)changeOption:(UITableViewCell<FXFormFieldCell> *)cell{
    self.formController.form = self.formController.form;
    [self.tableVIew reloadData];
}



@end
