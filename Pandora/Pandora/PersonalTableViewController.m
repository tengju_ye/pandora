//
//  PersonalTableViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/11/24.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "PersonalTableViewController.h"
#import "MainTabViewController.h"
#import "ProfileViewViewController.h"
#import "RechargeViewController.h"
#import "AddressTableViewController.h"
#import "AccountListTableViewController.h"
#import "UserServices.h"
#import "MyOrderTableViewController.h"
#import "MyFavTableViewController.h"
typedef enum:NSUInteger{
    PersonalItem_AllOrder,
    PersonalItem_Recharge,
    PersonalItem_MyProfile,
    PersonalItem_RechargeInfo,
    PersonalItem_FinanInfo,
    PersonalItem_MyFav,
    PersonalItem_Address,
    PersonalItem_Exit
} PersonalItem;
@interface PersonalTableViewController ()
@property (weak, nonatomic) IBOutlet UIButton *waitPayBtn;
@property (weak, nonatomic) IBOutlet UIButton *waitSendBtn;
@property (weak, nonatomic) IBOutlet UIButton *waitCheckBtn;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (nonatomic,strong) NSArray* datasources;
@end


@implementation PersonalTableViewController


-(instancetype)init{
    if (self=[super init]) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        UIImage* image =[UIImage imageNamed:@"self_icon"];
        [self.navigationController.tabBarItem setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        image = [UIImage imageNamed:@"self_icon_on"];
        [self.navigationController.tabBarItem setSelectedImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configView];

}


-(void)configView{
    [self.tableView setSeparatorColor:DEFAULT_GREEN];
    self.clearsSelectionOnViewWillAppear = YES;
    self.waitCheckBtn.layer.borderColor = DEFAULT_GREEN.CGColor;
    self.waitCheckBtn.layer.borderWidth = 1.0;
    self.waitPayBtn.layer.borderColor = DEFAULT_GREEN.CGColor;
    self.waitPayBtn.layer.borderWidth = 1.0;
    self.waitSendBtn.layer.borderColor = DEFAULT_GREEN.CGColor;
    self.waitSendBtn.layer.borderWidth = 1.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.datasources.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier" forIndexPath:indexPath];
    cell.textLabel.text = self.datasources[indexPath.row];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case PersonalItem_AllOrder:{
            MyOrderTableViewController* mvc = [[MyOrderTableViewController alloc] initWithNibName:@"MyOrderTableViewController" bundle:nil];
            [self.navigationController pushViewController:mvc animated:YES];
        }
            break;
        case PersonalItem_Recharge:{
            RechargeViewController* rvc = [[RechargeViewController alloc] initWithNibName:[RechargeViewController nibName] bundle:nil];
            [self.navigationController pushViewController:rvc animated:YES];
        }
            break;
        case PersonalItem_MyProfile:{
            ProfileViewViewController* pvc = [[ProfileViewViewController alloc] initWithNibName:[ProfileViewViewController nibName] bundle:nil];
            [self.navigationController pushViewController:pvc animated:YES];
        }
            break;
        case PersonalItem_RechargeInfo:{
            AccountListTableViewController* recharge = [[AccountListTableViewController alloc] initWithNibName:@"AccountListTableViewController" bundle:nil];
            recharge.onlyRecharge = YES;
            [self.navigationController pushViewController:recharge animated:YES];
            
        }
            break;
        case PersonalItem_FinanInfo:{
            AccountListTableViewController* recharge = [[AccountListTableViewController alloc] initWithNibName:@"AccountListTableViewController" bundle:nil];
            recharge.onlyRecharge = YES;
            [self.navigationController pushViewController:recharge animated:YES];
        }
            break;
        case PersonalItem_MyFav:{
            MyFavTableViewController* fav = [MyFavTableViewController new];
            fav.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:fav animated:YES];
        }
            break;
        case PersonalItem_Address:{
            AddressTableViewController* atc = [[AddressTableViewController alloc] init];
            [self.navigationController pushViewController:atc animated:YES];
        }
            break;
        case PersonalItem_Exit:{
            dispatch_async([[APP instance] initialQueue], ^{
                [[UserServices instance] logout];
            });
            [[APP instance] setSESSIONID:nil];
            [userDefaults removeObjectForKey:UD_KEY_ACCOUNT];
            [userDefaults removeObjectForKey:UD_KEY_PSW];

            [self.tabBarController setSelectedIndex:MainTabItem_MALL];
        }
            break;
        default:
            break;
    }
    

}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(NSArray *)datasources
{
    if (!_datasources) {
        _datasources = @[@"全部订单",@"充值",@"我的资料",@"充值明细",@"财务明细",@"我的收藏",@"地址管理",@"退出当前账号"];
    }
    return _datasources;
}

- (IBAction)pushToPrePay:(id)sender {
    MyOrderTableViewController* motvc = [MyOrderTableViewController getInstanceWithSameNibName];
    motvc.defaultShowItem = Order_SegIdx_UnPay;
    [self.navigationController pushViewController:motvc animated:YES];
}

- (IBAction)pushToPaid:(id)sender {
    MyOrderTableViewController* motvc = [MyOrderTableViewController getInstanceWithSameNibName];
    motvc.defaultShowItem = Order_SegIdx_UnDeliver;
    [self.navigationController pushViewController:motvc animated:YES];
}
- (IBAction)pushToUnConfirmed:(id)sender {
    MyOrderTableViewController* motvc = [MyOrderTableViewController getInstanceWithSameNibName];
    motvc.defaultShowItem = Order_SegIdx_UnCheck;
    [self.navigationController pushViewController:motvc animated:YES];
}


@end
