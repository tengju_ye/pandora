//
//  PieGraph.h
//  Pandora
//
//  Created by junchen on 12/5/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PieGraph : UIControl

@property (nonatomic) float percentage;
@property (nonatomic) UIColor *color;

- (void) showData;

@end
