//
//  PieGraph.m
//  Pandora
//
//  Created by junchen on 12/5/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "PieGraph.h"

#define PI 3.1415926

@implementation PieGraph


- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
	{
        self.backgroundColor = self.color;
    }
    return self;
}

-(void)setPercentage:(float)percentage{
    _percentage = percentage;
}

- (void) showData {
    
    float width = self.bounds.size.width;
    float height = self.bounds.size.height;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,width,height)];
    [imageView setBackgroundColor:self.color];
    
    UIBezierPath *path = [[UIBezierPath alloc]init];
    float angle = 2 * PI * self.percentage;
    float startAngle = -0.5 * PI;
    [path moveToPoint:CGPointMake(width / 2, 0)];
    [path addArcWithCenter:CGPointMake(width/2, height/2) radius:height/2 startAngle:startAngle endAngle:startAngle + angle clockwise:YES];
    [path addLineToPoint:CGPointMake(width/2, height/2)];
    [path closePath];
    [imageView.layer setMask:nil];
    
    CAShapeLayer *shape = [CAShapeLayer layer];
    shape.path = path.CGPath;
    imageView.layer.mask = shape;
    imageView.layer.masksToBounds =  NO;
    
    [imageView setClipsToBounds:YES];

    CGFloat circleWidth = width / 12;
    UIImageView *innerImage = [[UIImageView alloc] initWithFrame:CGRectMake(circleWidth, circleWidth, width - 2 * circleWidth, height - 2 * circleWidth)];
    [innerImage setBackgroundColor:[UIColor whiteColor]];
    innerImage.layer.masksToBounds = YES;
    innerImage.layer.cornerRadius = innerImage.bounds.size.width / 2;
    
    [self addSubview:imageView];
    [self addSubview:innerImage];
}

@end
