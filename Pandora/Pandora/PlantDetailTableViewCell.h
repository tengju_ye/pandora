//
//  PlantDetailTableViewCell.h
//  Pandora
//
//  Created by junchen on 12/5/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlantDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *state;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
