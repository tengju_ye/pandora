//
//  PlantItemTableViewCell.h
//  Pandora
//
//  Created by junchen on 12/4/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlantItemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *plantItemName;

@end
