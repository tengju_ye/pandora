//
//  PlantItemTableViewCell.m
//  Pandora
//
//  Created by junchen on 12/4/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "PlantItemTableViewCell.h"

@implementation PlantItemTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
