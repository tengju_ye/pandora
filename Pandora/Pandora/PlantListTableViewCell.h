//
//  PlantListTableViewCell.h
//  Pandora
//
//  Created by junchen on 12/4/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemList.h"

@interface PlantListTableViewCell : UITableViewCell <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) ItemList* itemList;

@property (weak, nonatomic) UIViewController *parentController;

@end
