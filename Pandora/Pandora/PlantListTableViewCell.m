//
//  PlantListTableViewCell.m
//  Pandora
//
//  Created by junchen on 12/4/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "PlantListTableViewCell.h"
#import "PlantItemTableViewCell.h"
#import "SeedStateViewController.h"

@implementation PlantListTableViewCell

- (void)awakeFromNib {
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:@"PlantItemTableViewCell" bundle:nil] forCellReuseIdentifier:@"plantItemTableCell"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.itemList.item_list count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PlantItemTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"plantItemTableCell" forIndexPath:indexPath];
    NSString *name = [[self.itemList.item_list objectAtIndex:[indexPath row]] item_name];
    cell.plantItemName.text = name;
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SeedStateViewController *newController = [[SeedStateViewController alloc] init];
    Item *item = [self.itemList.item_list objectAtIndex:[indexPath row]];
//    newController.itemId = item.item_id;
    newController.seedId = [item.item_id intValue];
    newController.name = item.item_name;
    [self.parentController.navigationController pushViewController:newController animated:YES];
}

@end
