//
//  PlantTableViewCell.h
//  Pandora
//
//  Created by junchen on 12/4/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlantTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *sortName;
@property (weak, nonatomic) IBOutlet UIImageView *sortImage;

@end
