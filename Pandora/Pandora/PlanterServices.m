//
//  PlanterServices.m
//  Pandora
//
//  Created by 小南宫的mac on 14/11/12.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "PlanterServices.h"
#import "GetUserPlanterListForm.h"
#import "GetUserPlanterListResponse.h"
@implementation PlanterServices
static PlanterServices* services = nil;
static dispatch_once_t pred = 0;
+(PlanterServices *)instance{
    dispatch_once( &pred, ^{
        services = [[self alloc] init];
    });
    return services;
}

-(BindNewPlanterResponse *)bindNewPlanter:(NSString *)serial_num{
    BindNewPlanterForm* form = [[BindNewPlanterForm alloc] init];
    form.serial_num = serial_num;
    NSString* baseurl = [[API shareInstance] BASE_API];
    __block BindNewPlanterResponse* response = nil;
    
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[BindNewPlanterResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    
    return response;
}

-(GetUserPlanterListResponse *)getUserPlanterList{
    GetUserPlanterListForm* form = [[GetUserPlanterListForm alloc] init];
    NSString* baseurl = [[API shareInstance] BASE_API];
    __block GetUserPlanterListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetUserPlanterListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

@end
