//
//  PlanterTableViewCell.h
//  Pandora
//
//  Created by junchen on 12/3/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwitchPlanterViewController.h"

@interface PlanterTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
@property (weak, nonatomic) IBOutlet UILabel *usingLabel;

@property (weak, nonatomic) SwitchPlanterViewController *parentController;
@property int planter_id;
@property int section;

@end
