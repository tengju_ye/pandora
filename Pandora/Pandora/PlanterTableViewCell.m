//
//  PlanterTableViewCell.m
//  Pandora
//
//  Created by junchen on 12/3/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "PlanterTableViewCell.h"
#import "PlanterServices.h"

@interface PlanterTableViewCell ()<UIAlertViewDelegate>

@property BOOL isOpened;

@end

@implementation PlanterTableViewCell

- (void)awakeFromNib {
    self.isOpened = NO;
    [self.selectBtn setBackgroundColor:DEFAULT_GREEN];
    [self.selectBtn setClipsToBounds:YES];
    [[self.selectBtn layer] setCornerRadius:2.0];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)selectPlanter:(id)sender {
    if([self.usingLabel isHidden]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"你确定要切换到该种植机吗" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
        alert.alertViewStyle = UIAlertViewStyleDefault;
        [alert show];
    } else {
        
    }
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        [MBProgressHUD showAddViewWhen:self.parentController.view when:^{
            [[PlanterServices instance] changeCurrentPlanter:self.planter_id];
            [self.parentController initDataSource];
        } complete:^{
             [self.parentController.tableView reloadData];
            [self.parentController pushTo:[[self.parentController tableView] indexPathForCell:self]];
        } text:@"切换中"];

    }
}

- (IBAction)openNameCell:(id)sender {
    if (self.isOpened) {
        [self.parentController closeCellAtSection:self.section];
        self.isOpened = NO;
    } else {
        [self.parentController openCellAtSection:self.section];
        self.isOpened = YES;
    }
}

@end
