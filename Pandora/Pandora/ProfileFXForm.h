//
//  ProfileFXForm.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/4.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXforms.h"
@interface ProfileFXForm : NSObject<FXForm>
@property(nonatomic,strong) NSString* name;
@property(nonatomic,strong) NSString* nickName;
@property(nonatomic,strong) NSString* mobile; //暂时先不显示
@property(nonatomic,strong) NSString* money;
@property(nonatomic,strong) NSString* level;
@property(nonatomic,strong) NSString* currentPlanter;
@end
