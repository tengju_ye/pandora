//
//  ProfileFXForm.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/4.
//  Copyright (c) 2014年 NG. All rights reserved.
//
/**
 
 @property(nonatomic,strong) NSString* name;
 @property(nonatomic,strong) NSString* nickName;
 @property(nonatomic,strong) NSString* mobile;
 @property(nonatomic,strong) NSString* money;
 @property(nonatomic,strong) NSString* level;
 @property(nonatomic,strong) NSString* currentPlanter;
 */

#import "ProfileFXForm.h"
#import "CustomButtonCell.h"
@implementation ProfileFXForm
-(NSArray *)fields{
    NSMutableArray* fields = [NSMutableArray array];
    [fields addObject:
     @{FXFormFieldHeader: @"",FXFormFieldKey: @"name", FXFormFieldType: FXFormFieldTypeText,FXFormFieldTitle: @"姓名",FXFormFieldPlaceholder:@"填写姓名",@"textLabel.color": [UIColor darkGrayColor]}
     ];
    
    [fields addObject:
     @{FXFormFieldKey:@"nickName",FXFormFieldTitle:@"昵称",FXFormFieldType:FXFormFieldTypeText,@"textLabel.color": [UIColor darkGrayColor]}
     ];
    
//    [fields addObject:
//     @{FXFormFieldKey:@"mobile",FXFormFieldTitle:@"绑定手机号码",FXFormFieldType:FXFormFieldTypeNumber,FXFormFieldPlaceholder:@"填写手机号码",@"textLabel.color": [UIColor darkGrayColor]}
//     ];
    
    
    [fields addObject:
     @{FXFormFieldHeader: @"",FXFormFieldKey:@"money",FXFormFieldTitle:@"金币数",FXFormFieldType:FXFormFieldTypeLabel,@"textLabel.color": [UIColor darkGrayColor]}
     ];
    
    
    [fields addObject:
     @{FXFormFieldType:FXFormFieldTypeLabel,FXFormFieldTitle: @"当前种植机器",FXFormFieldKey:@"currentPlanter",@"textLabel.color": [UIColor darkGrayColor]}
     ];
    


    [fields addObject:
     @{FXFormFieldType:FXFormFieldTypeOption,FXFormFieldTitle: @"地址管理", FXFormFieldAction: @"addressManage:",@"textLabel.color": [UIColor darkGrayColor],@"accessoryType":@(UITableViewCellAccessoryDisclosureIndicator)}
     ];
    
    
    return fields;
}

-(NSArray *)extraFields{
    NSMutableArray* fields = [NSMutableArray array];
    [fields addObject:
     @{FXFormFieldHeader: @"",FXFormFieldCell: [CustomButtonCell class],FXFormFieldTitle: @"提交", FXFormFieldAction: @"submit:"}
     ];
    return fields;
}

@end
