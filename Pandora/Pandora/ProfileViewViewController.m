//
//  ProfileViewViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/4.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "ProfileViewViewController.h"
#import "ProfileFXForm.h"
#import "UserServices.h"
#import "UserInfo.h"
#import "EditUserInfoForm.h"
#import "AddressTableViewController.h"
@interface ProfileViewViewController ()<FXFormControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *profileTableView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) FXFormController *formController;
@end

@implementation ProfileViewViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    if (self=[super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的资料";
    [[[self navigationController] navigationBar] setBackgroundColor:[UIColor whiteColor]];
    
    [MBProgressHUD showAddViewWhen:self.view when:^{
        [self webrequestGetProfile];
    } complete:^{
        [self.tableView reloadData];
    } text:@"请稍等"];
    
    
    self.formController = [[FXFormController alloc] init];
    self.formController.tableView = self.tableView;
    self.formController.delegate = self;
    self.formController.form = [[ProfileFXForm alloc] init];
    
    //header view
    UIImage* image = [UIImage imageNamed:@"greenh_bk"];
    CGSize size = image.size;
    CGFloat ratio = size.height / size.width;
    size.width = CGRectGetWidth(self.view.bounds);
    size.height = ratio * size.width/2;
    UIImageView* imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    [imageview setImage:image];

    [self.tableView setTableHeaderView:imageview];
    
    [self.tableView setSeparatorColor:DEFAULT_GREEN];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Fxform
-(void)addressManage:(UITableViewCell<FXFormFieldCell> *)cell
{
    AddressTableViewController* atvc = [[AddressTableViewController alloc] init];
    [self.navigationController pushViewController:atvc animated:YES];
}

- (void)submit:(UITableViewCell<FXFormFieldCell> *)cell
{
    ProfileFXForm* fx = (ProfileFXForm*)cell.field.form;
    __block BOOL tf = NO;
    [MBProgressHUD showAddViewWhen:self.view when:^{
        EditUserInfoResponse*response = [[UserServices instance] editUserInfo:fx.mobile realname:fx.name nickName:fx.nickName];
        if ([response.code isEqualToString:OBJC_SUCCEED_CODE]) {
            tf = YES;
        }
    } complete:^{
        if (tf) {
            [SVProgressHUD showSuccessWithStatus:@"修改成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
    } text:@"正在提交中"];
}


#pragma mark - Web Request
-(void)webrequestGetProfile
{
    GetUserInfoResponse* resposne =  [[UserServices instance] getUserInfo];
    ProfileFXForm*form = [[ProfileFXForm alloc] init];
    UserInfo* info = resposne.data;
    form.name = info.realname;
    form.nickName = info.nickname;
    form.mobile = info.mobile;
    form.money = info.left_money;
    form.level = info.rank_name;
    form.currentPlanter = info.current_planter_name;
    dispatch_async(MAIN_QUEUE, ^{
        self.formController.form = form;
    });
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
