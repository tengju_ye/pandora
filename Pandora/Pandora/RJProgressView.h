//
//  RJProgressView.h
//  BossSystem
//
//  Created by hzyd-wlj on 14-6-24.
//  Copyright (c) 2014年 hzyd-wlj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RJProgressView : UIView
@property(nonatomic,assign) CGFloat precent;  //0~1.0的数值
@property (nonatomic, strong) UIColor *color;
@end
