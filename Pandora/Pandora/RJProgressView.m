//
//  RJProgressView.m
//  BossSystem
//
//  Created by hzyd-wlj on 14-6-24.
//  Copyright (c) 2014年 hzyd-wlj. All rights reserved.
//

#import "RJProgressView.h"

@interface RJProgressView ()
@property(nonatomic,strong) CAShapeLayer *backgroundLayer;
@property(nonatomic,strong) CAShapeLayer *circleLayer;
@end

@implementation RJProgressView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
//    if (!self.backgroundLayer) {
//        self.backgroundLayer = [self circleLayerRect:rect];
//    }
//    self.backgroundLayer.strokeColor = [UIColor whiteColor].CGColor;
//    self.backgroundLayer.lineWidth = 12;
//    [self.layer addSublayer:self.backgroundLayer];
    
    
    if (!self.circleLayer) {
        self.circleLayer = [self circleLayerRect:rect];
    }
    self.circleLayer.lineWidth = 4;
    [self.layer addSublayer:self.circleLayer];
    self.circleLayer.strokeEnd = self.precent;
}

- (CAShapeLayer *)circleLayerRect:(CGRect)rect
{
    CGFloat radius = CGRectGetWidth(rect)/2;
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    circleLayer.backgroundColor = [UIColor clearColor].CGColor;
    circleLayer.frame = rect;
    circleLayer.fillColor = [UIColor clearColor].CGColor;
    circleLayer.strokeColor = self.color.CGColor;
    circleLayer.lineWidth = 4;
    circleLayer.lineCap = kCALineCapRound;
    circleLayer.lineJoin = kCALineJoinRound;
    circleLayer.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(CGRectGetWidth(self.frame)/2, CGRectGetHeight(self.frame)/2) radius:radius startAngle:-M_PI_2 endAngle:-M_PI_2 + (M_PI * 2) clockwise:YES].CGPath;
    circleLayer.strokeEnd = 1.0;
    return circleLayer;
}

- (void)setPrecent:(CGFloat)precent
{
    _precent = precent;
    _circleLayer.strokeEnd = _precent;
}
@end
