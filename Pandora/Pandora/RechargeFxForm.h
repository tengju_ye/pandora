//
//  RechargeFxForm.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/4.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FXForms.h"
@interface RechargeFxForm : NSObject<FXForm>
@property (nonatomic,strong) NSNumber* money;
@property (nonatomic,strong) NSString* bankType;//充值类型
@property (nonatomic,strong) NSArray* options;
@end
