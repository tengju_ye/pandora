//
//  RechargeFxForm.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/4.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "RechargeFxForm.h"
#import "CustomButtonCell.h"
@implementation RechargeFxForm

-(NSArray *)fields{
    
    NSMutableArray* array = [NSMutableArray array];
    [array addObject:@{FXFormFieldKey: @"money", FXFormFieldType:FXFormFieldTypeInteger,FXFormFieldTitle: @"充值金额(元)",FXFormFieldPlaceholder:@"请输入充值金额"}];
    if (self.options.count) {
        [array addObject: @{FXFormFieldTitle:@"支付方式",FXFormFieldKey: @"bankType", FXFormFieldOptions:self.options}];
    }

    return array;
}

-(NSArray *)extraFields{
    NSMutableArray* fields = [NSMutableArray array];
    [fields addObject:
     @{FXFormFieldHeader: @"",FXFormFieldCell: [CustomButtonCell class],FXFormFieldTitle: @"立即支付", FXFormFieldAction: @"submit:"}
     ];
    return fields;
}

@end
