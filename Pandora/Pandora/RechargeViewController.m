//
//  RechargeViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/4.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "RechargeViewController.h"
#import "RechargeFxForm.h"
#import "FXForms.h"
#import "PayServices.h"
@interface RechargeViewController ()<FXFormControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) FXFormController* formController;
@property (nonatomic,strong) NSArray* payways;

@end

@implementation RechargeViewController

-(instancetype)init{
    if(self==[super init]){
        self.title = @"充值";
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    if (self == [super initWithNibName:nibNameOrNil bundle:nil]) {
        self.title = @"充值";
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self==[super initWithCoder:aDecoder]) {
        self.title = @"充值";
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self.navigationController view] setBackgroundColor:[UIColor whiteColor]];
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    self.formController = [[FXFormController alloc] init];
    self.formController.tableView = self.tableView;
    self.formController.delegate = self;
    [self webGetChargeOptions];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
}

-(void)webGetChargeOptions{
    MBProgressHUD* hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    [hud setRemoveFromSuperViewOnHide:YES];
    [hud showAnimated:YES whileExecutingBlock:^{
        GetPaywayListResponse* response = [[PayServices instance] getPaywayList];
        NSMutableArray* arr = [NSMutableArray array];
        for (Payway* pw in response.data) {
            //只支持支付宝
            if ([pw.pay_tag isEqualToString:@"alipay"]) {
                [arr addObject:pw];
            }
        }
        self.payways = arr;

        NSMutableArray* options = [NSMutableArray array];
        for (Payway* payway in self.payways) {
            [options addObject:payway.pay_name];
        }
        RechargeFxForm* form = [[RechargeFxForm alloc] init];
        form.options = options;
        dispatch_async(MAIN_QUEUE, ^{
            self.formController.form = form;
            [self.tableView reloadData];
        });
    }];
}

-(void)webGetChargeLink:(NSNumber*)money type:(Payway*)payway{
    [MBProgressHUD showAddViewWhen:self.view when:^{
       GetRechargeInfoResponse_Alipay* alipay =  [[PayServices instance]getRechargeInfo_Alipay:money.floatValue payway_id:payway.payway_id.intValue];
        dispatch_async(MAIN_QUEUE, ^{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:alipay.link]];
            [self.navigationController popViewControllerAnimated:YES];
        });
    } text:@"请稍等"];
}


- (void)submit:(UITableViewCell<FXFormFieldCell> *)cell
{
    //we can lookup the form from the cell if we want, like this:
    RechargeFxForm *form = (RechargeFxForm *)cell.field.form;
    
    if (form.money.doubleValue>0) {
        for (Payway* payway in self.payways) {
            if ([payway.pay_name isEqualToString:form.bankType]) {
                [self webGetChargeLink:form.money type:payway];
                break;
            }
        }
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"金额不合法" message:nil delegate:nil cancelButtonTitle:@"重新输入" otherButtonTitles:nil, nil];
        [alert show];
    
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
