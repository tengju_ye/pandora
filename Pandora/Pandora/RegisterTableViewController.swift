//
//  RegisterTableViewController.swift
//  Pandora
//
//  Created by 小南宫的mac on 14/11/15.
//  Copyright (c) 2014年 NG. All rights reserved.
//

import UIKit

class RegisterTableViewController: UITableViewController {
    

    @IBOutlet weak var verifyTF: UITextField!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var getVerifyBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    
    private let userServices:UserServices = UserServices.instance()
    
    var loginMode:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.view.frame.size);
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    //    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    //        // #warning Potentially incomplete method implementation.
    //        // Return the number of sections.
    //        return 0
    //    }
    //
    //    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //        // #warning Incomplete method implementation.
    //        // Return the number of rows in the section.
    //        return 0
    //    }
    
    @IBAction func submit(sender: AnyObject) {
        let mobile = self.mobileTF.text
        let vericode = self.verifyTF.text;
        
        if(countElements(mobile)==0||countElements(vericode)==0){
            SVProgressHUD.showErrorWithStatus("信息不完整，请填写完整");
            return
        }
        
        
        let services = UserServices.instance()
        let hud = MBProgressHUD(view: self.view)
        hud.labelText = "请稍等"
        self.view.addSubview(hud);
        hud.showAnimated(true, whileExecutingBlock: { () -> Void in
            
            if(self.loginMode){
                let response =  services.login(mobile, password: nil, verify_code: vericode);
                if(response != nil){
                    if(response.code != nil){
                        if(response.code == "0"){
                            hud.labelText = "操作成功";
                            sleep(UInt32(1))
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                println("sessid = \(response.phpsessid)")
                                APP.instance().SESSIONID = response.phpsessid
                                NSUserDefaults.standardUserDefaults().setObject(mobile, forKey: "username")
                                NSUserDefaults.standardUserDefaults().setObject(response.password, forKey: "password")
                                NSUserDefaults.standardUserDefaults().synchronize()
                                self.dismissViewControllerAnimated(true, completion: nil)
                            })
                        }
                    }
                }
            
            }else{
                let response =  services.Register(mobile, verify_code: vericode);
                if(response != nil){
                    if(response.code != nil){
                        if(response.code == "0"){
                            hud.labelText = "操作成功";
                            sleep(UInt32(1))
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                println("sessid = \(response.phpsessid)")
                                APP.instance().SESSIONID = response.phpsessid
                                NSUserDefaults.standardUserDefaults().setObject(mobile, forKey: "username")
                                NSUserDefaults.standardUserDefaults().setObject(response.password, forKey: "password")
                                NSUserDefaults.standardUserDefaults().synchronize()
                                self.dismissViewControllerAnimated(true, completion: nil)
                            })
                        }
                    }
                }
            }

        });
    }
    
    @IBAction func getVerifyCode(sender: AnyObject) {
        let mobile = self.mobileTF.text
        
        let services = UserServices.instance()
        let hud = MBProgressHUD(view: self.view)
        hud.labelText = "请稍等"
        self.view.addSubview(hud);
        hud.removeFromSuperViewOnHide = true
        hud.showAnimated(true, whileExecutingBlock: { () -> Void in
            let response =  services.sendVerifyCode(mobile);
            if(response != nil){
                if(response.code != nil){
                    if(response.code == "0"){
                        hud.labelText = "获取成功";
                        sleep(UInt32(1))
                    }
                }
            }
        });
    }
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell
    
    // Configure the cell...
    
    return cell
    }
    */
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
    
}
