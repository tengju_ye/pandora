//
//  RightMenuViewController.h
//  Pandora
//
//  Created by 小南宫的mac on 14/11/24.
//  Copyright (c) 2014年 NG. All rights reserved.
//


#define kPersonalIdx    0
#define kMallIdx        1
#define kCartIdx        2
#define kMyPlantIdx     3
#define kPlantChoiceIdx 4
#define kBindNewPlanterIdx 5
#define kSwitchPlnaterIdx 6
#define kAboutIdx       7
#define kHelpIdx        8
#define kInitialIdx      9
#define kAuthManage     10
enum Main4TabItem:NSInteger{
    Main4TabItemMall = 0,
    Main4TabItemSearch = 1,
    Main4TabItemCart = 2,
    Main4TabItemPersonal = 3,
    Main4TabItemHelp = 4
};



@interface RightMenuViewController : UITableViewController
-(void)selectedIdx:(int)idx;
-(void)setIdxToMyPlantAndPushIfExist;//推入到我的植物,并且推入到当前种植的植物
@end
