//
//  RightMenuViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/11/24.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "RightMenuViewController.h"
#import "MyPlantViewController.h"
#import "ChoosePlantViewController.h"
#import "SwitchPlanterViewController.h"
#import "BindPlanterViewController.h"
#import "AboutViewController.h"
#import "HelpTableViewController.h"
#import "InitialViewController.h"
#import "AuthPlantersTableViewController.h"
@interface RightMenuViewController ()
@end

@implementation RightMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setTableFooterView:[UIView new]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    void (^finishBlock)(BOOL finish) = nil;
    switch (indexPath.row) {
        case kPersonalIdx:
            delegate.main4tabbarController.selectedIndex = Main4TabItemPersonal;
            if (self.mm_drawerController.centerViewController != delegate.main4tabbarController) {
                self.mm_drawerController.centerViewController = delegate.main4tabbarController;
            }
            break;
        case kMallIdx:
            delegate.main4tabbarController.selectedIndex = Main4TabItemMall;
            if (self.mm_drawerController.centerViewController != delegate.main4tabbarController) {
                self.mm_drawerController.centerViewController = delegate.main4tabbarController;
            }
            break;
        case kCartIdx:
            delegate.main4tabbarController.selectedIndex = Main4TabItemCart;
            if (self.mm_drawerController.centerViewController != delegate.main4tabbarController) {
                self.mm_drawerController.centerViewController = delegate.main4tabbarController;
            }
            break;
        case kMyPlantIdx:
        {
            
            MyPlantViewController *viewController = [[MyPlantViewController alloc] initWithNibName:@"MyPlantViewController" bundle:nil];
            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:viewController];
            self.mm_drawerController.centerViewController = nav;
        }
            break;
        case kPlantChoiceIdx:
        {
            
            ChoosePlantViewController *viewController = [[ChoosePlantViewController alloc] initWithNibName:@"ChoosePlantViewController" bundle:nil];
            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:viewController];
            self.mm_drawerController.centerViewController = nav;
        }
            break;
        case kBindNewPlanterIdx:
        {
            
            BindPlanterViewController *viewController = [[BindPlanterViewController alloc] initWithNibName:@"BindPlanterViewController" bundle:nil];
            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:viewController];
            self.mm_drawerController.centerViewController = nav;
        }
            break;
        case kSwitchPlnaterIdx:
        {
            
            SwitchPlanterViewController *viewController = [[SwitchPlanterViewController alloc] initWithNibName:@"SwitchPlanterViewController" bundle:nil];
            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:viewController];
            self.mm_drawerController.centerViewController = nav;
        }
            break;
        case kAboutIdx:
        {
            
            AboutViewController *viewController = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:viewController];
            self.mm_drawerController.centerViewController = nav;
        }
            break;
        case kHelpIdx:
        {
            HelpTableViewController *viewController = [[HelpTableViewController alloc] init];
            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:viewController];
            self.mm_drawerController.centerViewController = nav;
        }
            break;
        case kInitialIdx:{
            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:[InitialViewController getInstanceWithSameNibName]];
            self.mm_drawerController.centerViewController = nav;
        }
        case kAuthManage:{
            UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:[AuthPlantersTableViewController new]];
            self.mm_drawerController.centerViewController = nav;
        }
        default:
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.mm_drawerController closeDrawerAnimated:YES completion:finishBlock];
}


-(void)selectedIdx:(int)idx{
    [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
}

-(void)setIdxToMyPlantAndPushIfExist{
    MyPlantViewController *viewController = [[MyPlantViewController alloc] initWithNibName:@"MyPlantViewController" bundle:nil];
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    viewController.pushToValidIfExist = YES;
    self.mm_drawerController.centerViewController = nav;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
