//
//  SearchResultTableViewController.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/7.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultTableViewController : UITableViewController
@property(nonatomic,assign) NSUInteger nextFirstRow;


//query info
@property (nonatomic,strong) NSString* classID;
@property (nonatomic,strong) NSString* sortID;
@property (nonatomic,strong) NSString* classTag;
@property (nonatomic,strong) NSString* item_name;
@property (nonatomic,assign) BOOL shouldSearch;//是否进入页面的时候直接进行搜索相关条件，不如不设置则会等到下拉才触发

@property (nonatomic,strong) NSArray* aclasses;//if set , segments will be shown
@property (nonatomic,assign) NSUInteger selectedIndex;//current selected segments
@property (nonatomic,strong) NSMutableArray* datasource;//if set, table view will only show the data in the datasource
@end
