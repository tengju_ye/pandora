//
//  SearchResultTableViewController.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/7.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "SearchResultTableViewController.h"
#import "HotKeyTableViewCell.h"
#import "ItemServices.h"
#import "ItemRevealController.h"
#import "HMSegmentedControl.h"
#import "AClass.h"
#import "tools.h"
static NSString* const identifier = @"identifier";
@interface SearchResultTableViewController ()<UISearchBarDelegate>
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableDictionary* segDicts;//aclassid->items
@property (nonatomic,strong) HMSegmentedControl* segControl;
@property (nonatomic,strong) GetItemListResponse* response;
@end

@implementation SearchResultTableViewController


-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    if (self=[super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"商品列表页";
    [self.tableView registerNib:[HotKeyTableViewCell nib] forCellReuseIdentifier:identifier];
    self.searchBar.text = self.item_name;
    BOOL notRefresh = NO;
    if (self.item_name != nil || self.shouldSearch) {
        [self searchResult:self.item_name];
        notRefresh = YES;
    }
    self.clearsSelectionOnViewWillAppear = YES;
    if (self.aclasses.count!=0) {
        UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 88)];
        
        NSMutableArray* titles = [NSMutableArray array];
        
        for (AClass* aclass in self.aclasses) {
            [titles addObject:aclass.class_name];
        }
        
        self.segControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 44, CGRectGetWidth(self.view.bounds), 44)];
        CGRect searchRect = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 44);
        self.searchBar.frame = searchRect;
        [view addSubview:self.searchBar];
        [view addSubview:self.segControl];
        

        [self.segControl setSectionTitles:titles];
        [self.segControl setSelectionIndicatorColor:[UIColor redColor]];
        [self.segControl setSelectionIndicatorLocation:HMSegmentedControlSelectionIndicatorLocationDown];
        __weak SearchResultTableViewController* ws = self;

        [self.segControl setIndexChangeBlock:^(NSInteger index) {
            ws.selectedIndex = index;
        }];
        
        [self.tableView setTableHeaderView:view];
    }else{
        [self.tableView setTableHeaderView:self.searchBar];
    }
    
    
    __weak SearchResultTableViewController* ws = self;
    [self.tableView addFooterWithCallback:^{
        [APP runBlockInInitialQ:^{
            [ws webrequestOfNextPage];
            [APP runBlockInMainQ:^{
                [ws.tableView footerEndRefreshing];
                [ws.tableView reloadData];
            }];
        }];
    }];
    
    if (self.aclasses.count!=0&&self.datasource.count==0&&!notRefresh) {
        [self.segControl setSelectedSegmentIndex:0];
        ws.selectedIndex = 0;
    }
    [tools changeSearchCancelText:self.searchBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)searchResult:(NSString*)key{
    [MBProgressHUD showAddViewWhen:self.view when:^{
        self.nextFirstRow = 0;
        self.shouldSearch = YES;
        GetItemListResponse* response =  [[ItemServices instance] getItemList:0 class_id:self.classID?self.classID.intValue:-1 sort_id:self.sortID?self.sortID.intValue:-1 class_tag:self.classTag?self.classTag:nil item_name:key];
        self.datasource = [response.item_list mutableCopy];
        self.nextFirstRow = response.nextFirstRow;
        self.response = response;
    } complete:^{
        [self.tableView reloadData];
    } text:@"正在查询"];
}

-(void)webrequestOfNextPage{
//    if (self.searchBar.text.length!=0) {
        //部分搜索模式
    if (self.datasource.count<self.response.total_num&&self.shouldSearch == YES) {
        GetItemListResponse* response =  [[ItemServices instance] getItemList:0 class_id:self.classID?self.classID.intValue:-1 sort_id:self.sortID?self.sortID.intValue:-1 class_tag:self.classTag?self.classTag:nil item_name:self.searchBar.text];
        [self.datasource addObjectsFromArray:response.item_list];
        self.nextFirstRow = response.nextFirstRow;
    }

//    }else{
//        //商品列表模式, 不作处理
//        
//    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (self.datasource.count!=0) {
        return self.datasource.count;
    }else{
        AClass* aclass = [self.aclasses objectAtIndex:self.selectedIndex];
        NSArray* datasource = self.segDicts[aclass.class_id];
        return [datasource count];
    }
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (self.datasource.count==0) {
        return nil;
    }
    if (self.searchBar.text.length!=0&&self.response) {
        return [NSString stringWithFormat:@"找到相关商品%d件",self.response.total_num];
    }
    return [NSString stringWithFormat:@"找到相关商品%d件",self.datasource.count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HotKeyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    if (self.datasource.count!=0) {
        [cell    setItem:self.datasource[indexPath.row]];
    }else{
        AClass* aclass = self.aclasses[self.selectedIndex];
        [cell    setItem:[self.segDicts[aclass.class_id] objectAtIndex:indexPath.row]];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ItemRevealController* irc = [ItemRevealController getInstanceWithSameNibName];
    
    if (self.datasource.count!=0) {
        irc.item = self.datasource[indexPath.row];
    }else{
        AClass* aclass = self.aclasses[self.selectedIndex];
        irc.item = [self.segDicts[aclass.class_id] objectAtIndex:indexPath.row];
    }
    
    [self.navigationController pushViewController:irc animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 101;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    searchBar.text = nil;
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    if (searchBar.text.length!=0) {
        [self searchResult:searchBar.text];
    }
}

-(void)setSelectedIndex:(NSUInteger)selectedIndex{
    _selectedIndex = selectedIndex;
    self.shouldSearch = NO;
    [MBProgressHUD showAddViewWhen:self.view when:^{
        [self refreshCurrentSelectedClass];
    } complete:^{
        [self.tableView reloadData];
    } text:@"请稍等"];
}

-(void)refreshCurrentSelectedClass{
    AClass* aclass = [self.aclasses objectAtIndex:self.selectedIndex];
    dispatch_async([[APP instance] initialQueue], ^{
        GetItemListGroupBySortResponse* response = [[ItemServices instance] getItemListGroupBySort:aclass.class_id];
        if (response.data.count!=0) {
            NSArray* itemLists = response.data;
            NSMutableArray* datasource = [NSMutableArray array];
            for (ItemList* itemlist in itemLists) {
                [datasource addObjectsFromArray:itemlist.item_list];
            }
            [self.segDicts setObject:datasource forKey:aclass.class_id];
            self.datasource = nil;
        }
        dispatch_async(MAIN_QUEUE, ^{
            [self.tableView reloadData];
        });
    });
    
}


-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.segControl.frame = CGRectMake(0, 44, CGRectGetWidth(self.view.bounds), 44);
}

-(NSMutableDictionary *)segDicts{
    if (!_segDicts) {
        _segDicts = [NSMutableDictionary dictionary];
    }
    return _segDicts;
}

-(NSMutableArray *)datasource{
    if (!_datasource) {
        _datasource = [NSMutableArray array];
    }
    return _datasource;
}

@end
