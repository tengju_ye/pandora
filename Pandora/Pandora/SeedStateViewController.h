//
//  SeedStateViewController.h
//  Pandora
//
//  Created by junchen on 12/5/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PieGraph.h"
#import "HMSegmentedControl.h"
#import "RJProgressView.h"

@interface SeedStateViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (weak, nonatomic) IBOutlet UIImageView *centerImage;
@property (weak, nonatomic) IBOutlet UIImageView *centerCircle;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *stateImageList;
@property (weak, nonatomic) IBOutlet UIButton *yesBtn;
@property (weak, nonatomic) IBOutlet UIButton *noBtn;
@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *actionBtnList;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *actionLabelLiat;
@property (weak, nonatomic) IBOutlet RJProgressView *temperatureGraph;
@property (weak, nonatomic) IBOutlet RJProgressView *humidityGraph;
@property (weak, nonatomic) IBOutlet RJProgressView *sunLightGraph;

@property (weak, nonatomic) IBOutlet UIImageView *messageCircle;
@property (weak, nonatomic) IBOutlet UILabel *downMessage;
@property (weak, nonatomic) IBOutlet UILabel *centerMessage;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *humidityLabel;
@property (weak, nonatomic) IBOutlet UILabel *sunLightLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *diyBtn;
@property (weak, nonatomic) IBOutlet UIButton *waterSetBtn;
@property (weak, nonatomic) IBOutlet UISwitch *riskBtn;

//@property int ton;
//@property int toff;

@property NSString *planterSeedId;
//@property NSString *itemId;
@property int seedId;
@property NSString *name;




- (void) updateData;

@end
