//
//  SeedStateViewController.m
//  Pandora
//
//  Created by junchen on 12/5/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "SeedStateViewController.h"
#import "PlanterServices.h"
#import "PlanterSeedServices.h"
#import "ItemServices.h"
#import "DIYViewController.h"
#import "ChangeSettingViewController.h"
#import "UIViewController+MyCate.h"
#import "tools.h"
#import "UIAlertView+MyBlocks.h"
#import "RWBlurPopover.h"
#import "RightMenuViewController.h"
#define MAX_ILLU 60000
#define MAX_HUMI 100
#define MAX_TEM 50
#define Mapping1 [@(0),]
@interface SeedStateViewController ()

@property BOOL water;
@property BOOL light;
@property BOOL fan;
@property BOOL heat;
@property int seedStateId;
@property (nonatomic,strong) NSString *riskMode;


@property (strong, nonatomic) NSArray *messageList;
@property (nonatomic,assign) NSUInteger waterInterval;
@property (nonatomic,assign) NSUInteger waterTime;
@property (nonatomic,assign) BOOL getDataSuccess;


@property (strong,nonatomic) NSMutableDictionary* seedStates;//所有的职务状态 (nsstring)state - > seedstate
@property (strong, nonatomic) NSMutableDictionary *centerImageList;//所有植物状态的图片(nnsstring)state -> image

@property (nonatomic,strong) NSString* state;//当前状态
@property (weak, nonatomic) IBOutlet UILabel *riskLabel;

//当前种植机的状态
@property (nonatomic,assign)CGFloat currentTemperature;
@property (nonatomic,assign)CGFloat currentHumidity;
@property (nonatomic,assign)CGFloat currentLight;


//是否在线
@property (nonatomic,assign) BOOL isOnline;
@property (nonatomic,assign)IBOutlet UIImageView* onlineImageView;
@end

@implementation SeedStateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我种植的植物";
    self.getDataSuccess = YES;
    [tools addRightBarItem:self];
    
    [[self.navigationController view] setBackgroundColor:[UIColor whiteColor]];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setAutomaticallyAdjustsScrollViewInsets:YES];
    
    self.temperatureGraph.color = RGBCOLOR(231, 34, 37);
    self.humidityGraph.color = RGBCOLOR(0, 162, 233);
    self.sunLightGraph.color = RGBCOLOR(242, 148, 57);
    
    [self.scrollView setContentSize:CGSizeMake(CGRectGetWidth(self.view.bounds), 500)];
    self.centerImage.layer.masksToBounds = YES;
    self.centerImage.layer.cornerRadius = self.centerImage.bounds.size.width / 2;
    
    //config btn
    [self.diyBtn setClipsToBounds:YES];
    self.diyBtn.layer.cornerRadius = 10.0;
    self.diyBtn.layer.borderColor = DEFAULT_GREEN.CGColor;
    self.diyBtn.layer.borderWidth = 1.0;
    
    [self.waterSetBtn setClipsToBounds:YES];
    self.waterSetBtn.layer.cornerRadius = 10.0;
    self.waterSetBtn.layer.borderColor = DEFAULT_GREEN.CGColor;
    self.waterSetBtn.layer.borderWidth = 1.0;
    
    
    MBProgressHUD* hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    hud.removeFromSuperViewOnHide = YES;
    hud.labelText = @"请稍等";
    [hud showAnimated:YES whileExecutingBlock:^{
        [self webGetInit];
    } completionBlock:^{
        if (!self.getDataSuccess) {
            [APP runBlockInMainQ:^{
                [UIAlertView  showAlertViewWithTitle:@"获取植物状态失败" message:@"请选择其他植物" cancelButtonTitle:@"好的" otherButtonTitles:nil onDismiss:^(int buttonIndex, UIAlertView *alertView) {
                    
                } onCancel:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }];
        }else{
            [self showData];
        }
    }];
    
}




-(void)webGetInit{
    [self updateData];
    [self updateState];
    self.centerImageList = self.centerImageList;//获取image
}

- (NSArray *) messageList {
    if (!_messageList) {
        _messageList = @[@"种子期：埋于松紧适中的土壤中",
                         @"请问种子发芽了吗？",
                         @"请问长出叶子了吗？",
                         @"请问看到很多叶子了吗？",
                         @"请问叶子很茂盛了吗？",
                         @"请问看到果实了吗？",
                         @"请问果实变多了吗？",
                         @"请问果实结了很多吗？",
                         @"请问叶子开始脱落了吗？",
                         @"请问果实成熟了吗？",
                         @"恭喜～收获啦"];
    }
    return _messageList;
}


- (NSMutableDictionary *) centerImageList {
    if (!_centerImageList) {
        _centerImageList = [NSMutableDictionary dictionary];
    }
    return _centerImageList;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//机器状态
-(void)updateDataIfDiy{
    if (self.riskMode.boolValue) {
        [MBProgressHUD showAddViewWhen:self.view when:^{
            [self updateData];
        } complete:^{
            [self showData];
        } text:@""];
    }
    
}



- (void) updateData {
    [self updatePlanterInfo];    
    //获取所有状态
    GetSeedStateListResponse *response_1 = [[ItemServices instance] getSeedStateList:self.seedId is_diy:[self.riskMode isEqualToString:@"1"]];
    [self.seedStates removeAllObjects];
    for (SeedState* state in response_1.data) {
        [self.seedStates setObject:state forKey:state.state];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://smartplant.pandorabox.mobi%@", state.img_path]];
        if (![self.centerImageList objectForKey:state.state]) {
            NSData *data = [NSData dataWithContentsOfURL:url];
            if (data) {
                [self.centerImageList setObject:data forKey:state.state];
            }
        }
    }
}

//更新显示种植机当前各项参数
- (void)updatePlanterInfo {
    //获取种植机器的状态
    GetPlanterInfoResponse *getPlanterInfoResponse = [[PlanterServices instance] getPlanterInfo];
    
    //冒险模式
    NSString *riskMode = getPlanterInfoResponse.is_risk_mode;
    self.riskMode = riskMode;
    [APP runBlockInMainQ:^{
        self.riskBtn.on = riskMode.boolValue;
    }];
    
    //浇水，灌溉时间
    self.waterTime = getPlanterInfoResponse.ton.intValue;
    self.waterInterval = getPlanterInfoResponse.toff.intValue;
    
    //set corresponding cycle value
    self.currentHumidity = getPlanterInfoResponse.humidity.intValue;
    self.currentLight = getPlanterInfoResponse.illuminance.intValue;
    self.currentTemperature = getPlanterInfoResponse.outside_temperature.intValue;
    
    
    NSString *boxState = getPlanterInfoResponse.box_state;
    if([boxState characterAtIndex:0] == '0') {
        self.water = NO;
    } else {
        self.water = YES;
    }
    if([boxState characterAtIndex:1] == '0') {
        self.light = NO;
    } else {
        self.light = YES;
    }
    if([boxState characterAtIndex:2] == '0') {
        self.fan = NO;
    } else {
        self.fan = YES;
    }
    if([boxState characterAtIndex:3] == '0') {
        self.heat = NO;
    } else {
        self.heat = YES;
    }
    
    self.isOnline = getPlanterInfoResponse.online;
}

//获取植物当前状态
- (void) updateState {
    if (self.planterSeedId != nil) {
        GetPlanterSeedInfoResponse *getPlanterSeedInfoResponse = [[PlanterSeedServices instance] getPlanterSeedInfo:[self.planterSeedId intValue]];
        self.state = getPlanterSeedInfoResponse.state;
    } else {
        self.state = nil;
    }
    DLog(@"%@",self.state);
}


//根据获取状态，展示数据
- (void) showData {
    if ([self.riskMode isEqualToString:@"1"]) {
        [self.riskBtn setOn:YES];
    } else {
        [self.riskBtn setOn:NO];
    }
    self.nameLabel.text = self.name;
    self.descriptionLabel.text = [NSString stringWithFormat:@"这是%@种植的一个详细过程，请选择开始状态", self.name];
    if (self.water) {
        [[self.actionBtnList objectAtIndex:0] setImage:[UIImage imageNamed:@"waterOn"] forState:UIControlStateNormal];
    } else {
        [[self.actionBtnList objectAtIndex:0] setImage:[UIImage imageNamed:@"waterOff"] forState:UIControlStateNormal];
    }
    if (self.light) {
        [[self.actionBtnList objectAtIndex:1] setImage:[UIImage imageNamed:@"lightOn"] forState:UIControlStateNormal];
    } else {
        [[self.actionBtnList objectAtIndex:1] setImage:[UIImage imageNamed:@"lightOff"] forState:UIControlStateNormal];
    }
    if (self.fan) {
        [[self.actionBtnList objectAtIndex:2] setImage:[UIImage imageNamed:@"fanOn"] forState:UIControlStateNormal];
    } else {
        [[self.actionBtnList objectAtIndex:2] setImage:[UIImage imageNamed:@"fanOff"] forState:UIControlStateNormal];
    }
    if (self.heat) {
        [[self.actionBtnList objectAtIndex:3] setImage:[UIImage imageNamed:@"heatOn"] forState:UIControlStateNormal];
    } else {
        [[self.actionBtnList objectAtIndex:3] setImage:[UIImage imageNamed:@"heatOff"] forState:UIControlStateNormal];
    }
    
    
    
    NSString* firstState = nil;
    
    //均匀分布
    NSInteger count = self.seedStates.count;
    NSMutableOrderedSet * mappingArray  = [NSMutableOrderedSet orderedSet];
    int total = 9;//总个数
    [mappingArray removeAllObjects];
    
    //计算需要哪些图片
    if (count==1) {
        mappingArray = [@[@0] mutableCopy];
    }else if(count==2){
        mappingArray = [@[@0,@8] mutableCopy];
    }else{
        int start = 0;
        int end = 8;
        int interval = 1;
        interval = (total-1)/(count-2+1);
        while (start<end&&mappingArray.count<count) {
            [mappingArray addObject:@(start)];
            if (mappingArray.count==count) {
                break;
            }
            [mappingArray addObject:@(end)];
            if (mappingArray.count==count) {
                break;
            }
            start += interval;
            end -= interval;
        }
        
        if (mappingArray.count!=count) {
            [mappingArray addObject:@(start)];
        }
        
    }
    
    [mappingArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2];
    }];
    

    NSMutableDictionary* mappImages = [NSMutableDictionary dictionary];//state -> show image view
    
    
    for (UIImageView* iv in self.stateImageList) {
        [iv setHidden:YES];
    }
    
    for (int i = 8; i >=0; i--) {
        //设置状态图片
        [[self.stateImageList objectAtIndex:i] setImage: [UIImage imageNamed:[NSString stringWithFormat:@"phaseGreen%d", (i+1)]]];
        
        
        
        //是否存在该状态
        if (self.seedStates[@(i+1).stringValue]) {
            if (firstState==nil) {
                firstState = @(i+1).stringValue;
            }
            
            NSUInteger imageIdx = [mappingArray.lastObject integerValue];
            [mappingArray removeObject:@(imageIdx)];
            //存在该状态，则该状态对应的图片
            [[self.stateImageList objectAtIndex:imageIdx] setHidden:NO];
            [mappImages setObject:[self.stateImageList objectAtIndex:imageIdx]  forKey:@(i+1).stringValue];
        }
        
    }
    
    
    //还没有开始种植
    if (self.state==nil) {
        [self.diyBtn setHidden:YES];
        [self.waterSetBtn setHidden:YES];
        [self.scrollView setScrollEnabled:NO];
        
        [self.startBtn setHidden:NO];
        [self.yesBtn setHidden:YES];
        [self.noBtn setHidden:YES];
        [self.riskBtn setHidden:YES];
        [self.riskLabel setHidden:YES];
        
        
        [self.downMessage setHidden:YES];
        [self.centerMessage setHidden:NO];
        [self.messageCircle setImage:[UIImage imageNamed:@"circleMessageCenter"]];
        
        
        //中间显示第一个状态说明and image
        if (firstState) {
            NSData* data = [self.centerImageList objectForKey:firstState];
            if (data) {
                [self.centerImage setImage:[UIImage imageWithData:data]];
            }
            self.centerMessage.text = [self.messageList objectAtIndex:(firstState.integerValue)];
        }
        
        
    } else {
        //开始种植了,显示当前状态
        
        //控件的显示
        [self.scrollView setScrollEnabled:YES];
        [self.diyBtn setHidden:NO];
        [self.waterSetBtn setHidden:NO];
        [self.startBtn setHidden:YES];
        [self.yesBtn setHidden:NO];
        [self.noBtn setHidden:NO];
        [self.downMessage setHidden:NO];
        [self.centerMessage setHidden:YES];
        [self.riskBtn setHidden:NO];
        [self.riskLabel setHidden:NO];
        
        //具体描述
        self.downMessage.text = [self.messageList objectAtIndex:(self.state.integerValue)];
        
        NSData* data = [self.centerImageList objectForKey:self.state];
        if (data) {
            [self.centerImage setImage:[UIImage imageWithData:data]];
        }
        
        [self.messageCircle setImage:[UIImage imageNamed:@"circleMessageDown"]];
        
        
        [mappImages enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            NSString* state = (NSString*)key;
            UIImageView* iv = (UIImageView*)obj;
            //state 转换成图片的次序
            int imgidx = (int)[self.stateImageList indexOfObject:obj];
            if ([state isEqualToString:self.state]) {
                [iv setImage:[UIImage imageNamed:[NSString stringWithFormat:@"phaseGreen%d", imgidx+1]]];
                [self.centerCircle setImage:[UIImage imageNamed:[NSString stringWithFormat:@"radian%d",  imgidx+1]]];

            }else{
                [iv setImage:[UIImage imageNamed:[NSString stringWithFormat:@"phaseGray%d", imgidx+1]]];
            }
            
        }];
        

        
        
    }
}

- (IBAction)changeSeedState:(id)sender {
    NSInteger tag = ((UIButton *)sender).tag;
    __block BOOL reloadData = YES;
    
    NSArray* keys = [[self.seedStates allKeys] sortedArrayUsingSelector:@selector(compare:)];
    NSInteger index = [keys indexOfObject:self.state];
    NSString* nextState = nil;
    if (index!=NSNotFound&&index<(keys.count-1)) {
        nextState =keys[index+1];
    }
    NSString* beforeState = nil;
    
    if (index!=NSNotFound&&index>0) {
        beforeState =keys[index-1];
    }
    
    [MBProgressHUD showAddViewWhen:self.view when:^{
        if (tag==10 && [self.state isEqualToString:keys.lastObject]&&self.state) {
            [APP runBlockInMainQ:^{
                self.downMessage.text = @"恭喜～收获啦";
            }];
            reloadData = NO;
        }else if (tag == 10 && self.state!=nil&&nextState!=nil) {
            //yes
            [[PlanterSeedServices instance] setPlanterSeedState:[self.planterSeedId intValue] state:nextState.intValue seed_id:self.seedId];
            [self updateState];
            
            
        } else if (tag == 11 && beforeState!=nil) {
            //no
            [[PlanterSeedServices instance] setPlanterSeedState:[self.planterSeedId intValue] state:beforeState.intValue seed_id:self.seedId];
            self.state = beforeState ;
        }
        [self updatePlanterInfo];
    } complete:^{
        if (reloadData) {
            [self showData];
        }
    }  text:@"请稍等"];
    
}

- (IBAction)start:(id)sender {
    
    [MBProgressHUD showAddViewWhen:self.view when:^{
        PlantNewSeedResponse *response;
        if (self.state == nil) {
            NSArray* array = [[self.seedStates allKeys] sortedArrayUsingSelector:@selector(compare:)];
            SeedState* state = [self.seedStates objectForKey:array.firstObject];
            response = [[PlanterSeedServices instance] plantNewSeed:self.seedId seed_state_id:self.seedStateId state:state.state.intValue];
        } else {
            response = [[PlanterSeedServices instance] plantNewSeed:self.seedId seed_state_id:self.seedStateId state:self.state.intValue];
            
        }
        
        self.planterSeedId = [NSString stringWithFormat:@"%d", response.planter_seed_id];
        [self updateData];
        [self updateState];
    } complete:^{
        [self showData];
    } text:@"请稍等"];
    
    
}

- (IBAction)chooseState:(id)sender {
  
}

- (IBAction)changeBoxState:(id)sender {
    
    if (!self.riskBtn.isOn) {
        return;
    }
    
    
    NSInteger tag = ((UIButton *)sender).tag;
    if (tag == 20) {
        if (self.water) {
            [[PlanterServices instance] setBoxState:1 state:0];
        } else {
            [[PlanterServices instance] setBoxState:1 state:1];
        }
    } else if (tag == 21) {
        if (self.light) {
            [[PlanterServices instance] setBoxState:2 state:0];
        } else {
            [[PlanterServices instance] setBoxState:2 state:1];
        }
    } else if (tag == 22) {
        if (self.fan) {
            [[PlanterServices instance] setBoxState:3 state:0];
        } else {
            [[PlanterServices instance] setBoxState:3 state:1];
        }
    } else {
        if (self.heat) {
            [[PlanterServices instance] setBoxState:4 state:0];
        } else {
            [[PlanterServices instance] setBoxState:4 state:1];
        }
    }
    [self updateData];
    [self showData];
}

- (IBAction)startDIY:(id)sender {
    DIYViewController *newController = [[DIYViewController alloc] init];
    newController.planterSeedId = self.planterSeedId;
    newController.seedId = [NSString stringWithFormat:@"%d",self.seedId ];
    newController.initialState = self.seedStates[self.state];
    [self.navigationController pushViewController:newController animated:YES];
}



- (IBAction)changeWaterSetting:(id)sender {
    ChangeSettingViewController *newController = [ChangeSettingViewController getInstanceWithSameNibName];
    newController.ton = (int)self.waterTime;
    newController.toff = (int)self.waterInterval;
    newController.time.text = @(self.waterTime).stringValue;
    newController.period.text = @(self.waterInterval).stringValue;
    newController.parentController = self;
    
    [RWBlurPopover showContentViewController:newController insideViewController:self];
}



- (IBAction)riskMode:(id)sender {
    [MBProgressHUD showAddViewWhen:self.view when:^{
        SetModeResponse* response = [[PlanterServices instance] setMode:self.riskBtn.isOn?@"1":@"0"];
        if ([response.code isEqualToString:OBJC_SUCCEED_CODE]) {
            self.riskMode = self.riskBtn.isOn?@"1":@"0";
        }
        [self updateData];
    } complete:^{
        [self showData];
    }  text:@"切换中"];
}

- (IBAction)jumpToMall:(id)sender {
    [(RightMenuViewController*)[[self mm_drawerController] rightDrawerViewController] selectedIdx:kMallIdx];
}



#pragma mark - getter
-(NSMutableDictionary *)seedStates{
    if (!_seedStates) {
        _seedStates = [NSMutableDictionary dictionary];
    }
    return _seedStates;
}


#pragma mark - setter

-(void)setIsOnline:(BOOL)isOnline{
    _isOnline  = isOnline;
    self.onlineImageView.highlighted = _isOnline;
}

-(void)setCurrentHumidity:(CGFloat)currentHumidity{
    _currentHumidity = currentHumidity;
    [APP runBlockInMainQ:^{
        self.humidityLabel.text = [NSString stringWithFormat:@"%d%%",(int)currentHumidity];
        self.humidityGraph.precent = currentHumidity/MAX_HUMI;
    }];
}

-(void)setCurrentLight:(CGFloat)currentLight{
    _currentLight = currentLight;
    [APP runBlockInMainQ:^{
        self.sunLightLabel.text = [NSString stringWithFormat:@"%dlx", (int)currentLight];
        self.sunLightGraph.precent = currentLight/MAX_ILLU;
    }];
}

-(void)setCurrentTemperature:(CGFloat)currentTemperature{
    _currentTemperature = currentTemperature;
    [APP runBlockInMainQ:^{
        self.temperatureLabel.text = [NSString stringWithFormat:@"%d℃", (int)currentTemperature];
        self.temperatureGraph.precent = currentTemperature/MAX_TEM;
    }];
}

-(void)setRiskMode:(NSString *)riskMode{
    _riskMode = riskMode;
    
    [APP runBlockInMainQ:^{
        if (_riskMode.boolValue) {
            self.riskLabel.text = @"手动模式";
        }else{
            self.riskLabel.text = @"自动模式";
        }
    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
