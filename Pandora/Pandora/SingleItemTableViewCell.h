//
//  SingleItemTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/2.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "Item.h"
@class SingleItemTableViewCell;
@protocol SingleItemTableViewCellDelegate<NSObject>
-(void)singleItemCell:(SingleItemTableViewCell*)cell didClickItem:(Item*)item;
@end
@interface SingleItemTableViewCell : BaseTableViewCell
@property(nonatomic,strong)Item* item;
@property(nonatomic,strong)id<SingleItemTableViewCellDelegate>delegate;
@property (weak, nonatomic) IBOutlet UIView *cellDetailContentView;

@end
