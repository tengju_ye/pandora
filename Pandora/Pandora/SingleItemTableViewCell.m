//
//  SingleItemTableViewCell.m
//  Pandora
//
//  Created by 小南宫的mac on 4/2/2.
//  Copyright (c) 204年 NG. All rights reserved.
//

#import "SingleItemTableViewCell.h"
#import "UIButton+WebCache.h"
@interface SingleItemTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *itemImageview;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *sellLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (nonatomic,strong) NSLayoutConstraint* aspectConstraint;
@end
@implementation SingleItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.sellLabel.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)prepareForReuse{
    [super prepareForReuse];
    self.aspectConstraint = nil;
}

-(void)willMoveToSuperview:(UIView *)newSuperview{
    
}

-(void)setItem:(Item *)item{
    _item = item;
    
    self.aspectConstraint = [NSLayoutConstraint constraintWithItem:self.itemImageview attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.itemImageview attribute:NSLayoutAttributeHeight multiplier:1 constant:0];
    
    NSURL* url = _item?[NSURL URLWithString:_item.base_pic relativeToURL:[NSURL URLWithString:OBJC_IMGBASE_URL]]:nil;
    
    [self.itemImageview setBackgroundColor:[UIColor clearColor]];
    [self.itemImageview sd_setImageWithURL:url];

    self.titleLabel.text = _item.item_name;
    [self.sellLabel setText:[NSString stringWithFormat:@"销量:%@",_item.sales_num]];

    NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithString:@"价格: "];
    [string appendAttributedString:[[NSAttributedString alloc] initWithString:_item.mall_price attributes:@{NSForegroundColorAttributeName:[UIColor redColor]}]];
    [self.priceLabel setAttributedText:string];
}

- (IBAction)btnTouched:(id)sender {
    if ([self.delegate respondsToSelector:@selector(singleItemCell:didClickItem:)]) {
        [self.delegate singleItemCell:self didClickItem:self.item];
    }
}


-(void)setAspectConstraint:(NSLayoutConstraint *)aspectConstraint{
    if (_aspectConstraint) {
        [self.itemImageview removeConstraint:_aspectConstraint];
    }
    
    _aspectConstraint = aspectConstraint;
    
    if (_aspectConstraint) {
        [self.itemImageview addConstraint:aspectConstraint];
    }
}



@end
