//
//  SkuCellTableViewCell.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/7.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "Sku.h"
#import "TextStepperField.h"
@interface SkuCellTableViewCell : BaseTableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *prop1;
@property (weak, nonatomic) IBOutlet UILabel *prop2;
@property (weak, nonatomic) IBOutlet UILabel *remain;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet TextStepperField *amountTF;
@property (nonatomic,strong) Sku* sku;
@property (nonatomic,assign) BOOL noSkuMode;//如果是没有sku的模式，则标题从左边开始
@end
