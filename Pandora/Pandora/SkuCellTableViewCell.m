//
//  SkuCellTableViewCell.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/7.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "SkuCellTableViewCell.h"

@implementation SkuCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.amountTF.Current = 0;
    self.amountTF.Minimum = 0;
    // Initialization code
//    self.amountTF.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setSku:(Sku *)sku{
    _sku = sku;
    if (_sku) {
//        self.stepper.value = _sku.currentValue.integerValue;
//        [self   vc:self.stepper];
//        self.stepper.hidden = NO;
        self.amountTF.hidden = NO;
        self.amountLabel.hidden = YES;
        self.prop1.text = _sku.property_name1;
        self.prop2.text = _sku.property_name2;
        self.remain.text = _sku.sku_stock;
        self.price.text = _sku.sku_price;
        self.amountTF.Current = _sku.currentValue.integerValue;
    }else{
//        self.stepper.hidden = YES;
        self.amountTF.hidden = YES;
        self.amountLabel.hidden = NO;
    }

}
- (IBAction)vc:(id)sender {
    if(self.amountTF.Current<=0)
        self.amountTF.Current = 0 ;
    self.sku.currentValue = @(self.amountTF.Current);
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    int i = string.intValue;
    self.amountTF.Current = i;
    self.sku.currentValue = @(self.amountTF.Current);
    return YES;
}





@end
