//
//  SwitchPlanterViewController.h
//  Pandora
//
//  Created by junchen on 12/3/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwitchPlanterViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void) initDataSource;
- (void) openCellAtSection:(int)section;
- (void) closeCellAtSection:(int)section;
- (void) pushTo:(NSIndexPath*)indexPath;
@end
