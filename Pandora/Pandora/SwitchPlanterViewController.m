//
//  SwitchPlanterViewController.m
//  Pandora
//
//  Created by junchen on 12/3/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "SwitchPlanterViewController.h"
#import "PlanterServices.h"
#import "PlanterTableViewCell.h"
#import "EditPlanterNameTableViewCell.h"
#import "tools.h"
#import "SeedStateViewController.h"
#import "UserServices.h"
#import "PlanterSeedServices.h"
#import "RightMenuViewController.h"

@interface SwitchPlanterViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong)  NSArray<UserPlanter> *dataSource;
@property NSMutableArray *cellIsOpened;
@property NSString *currentPlantId;

@end

@implementation SwitchPlanterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"切换种植机";
    [tools addRightBarItem:self];
    [self initDataSource];
    self.cellIsOpened = [[NSMutableArray alloc] init];
    for(int i = 0; i < [self.dataSource count]; i++) {
        [self.cellIsOpened addObject:@"1"];
    }
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:@"PlanterTableViewCell" bundle:nil] forCellReuseIdentifier:@"planterCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EditPlanterNameTableViewCell" bundle:nil] forCellReuseIdentifier:@"editPlanterNameCell"];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initDataSource {
    
    GetUserPlanterListResponse* getUserPlanterListResponse = [[PlanterServices instance] getUserPlanterList];
    if([getUserPlanterListResponse.code isEqualToString:OBJC_SUCCEED_CODE]) {
        self.dataSource = getUserPlanterListResponse.data;
    }
    
    GetUserInfoResponse *response = [[UserServices instance] getUserInfo];
    self.currentPlantId = response.data.current_planter_id;

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.dataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.cellIsOpened objectAtIndex:section] intValue];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserPlanter *planter = [self.dataSource objectAtIndex:[indexPath section]];
    if ([indexPath row] == 0) {
        PlanterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"planterCell" forIndexPath:indexPath];
        NSString *planterId = planter.planter_id;
        NSString *name = planter.planter_name;
        [cell.selectBtn setTitle:[NSString stringWithFormat:@"名称: %@", name] forState:UIControlStateNormal];
        if([planterId isEqualToString:self.currentPlantId]) {
            [cell.usingLabel setHidden:NO];
        } else {
            [cell.usingLabel setHidden:YES];
        }
        cell.parentController = self;
        cell.planter_id = [planter.planter_id intValue];
        cell.section = (int)[indexPath section];
        return cell;
    } else {
        EditPlanterNameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"editPlanterNameCell" forIndexPath:indexPath];
        cell.planter_id = [planter.planter_id intValue];
        cell.parentController = self;
        return cell;
    }
}

- (void)openCellAtSection:(int)section {
    [self.cellIsOpened setObject:@"2" atIndexedSubscript:section];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:section];
    [self.tableView insertRowsAtIndexPaths:[[NSArray alloc] initWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationMiddle];
    //[self.tableView reloadData];
}

- (void) closeCellAtSection:(int)section {
    [self.cellIsOpened setObject:@"1" atIndexedSubscript:section];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:section];
    [self.tableView deleteRowsAtIndexPaths:[[NSArray alloc] initWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationMiddle];
    //[self.tableView reloadData];
}


-(void)pushTo:(NSIndexPath *)indexPath{
    
    [MBProgressHUD showAddViewWhen:self.view when:^{
        //存在当前种植机,并且有种植号,如果是正常登录状态则，推出后到主控界面
        GetPlanterInfoResponse* infoResponse = [[PlanterServices instance] getPlanterInfo];
        if (infoResponse) {
            SeedStateViewController *newController = [SeedStateViewController getInstanceWithSameNibName];
            newController.planterSeedId = infoResponse.planter_seed_id;
            newController.seedId = infoResponse.seed_id.intValue;
            newController.name = infoResponse.planter_name;
            [APP runBlockInMainQ:^{
                [self.navigationController pushViewController:newController animated:YES];
            }];
        }else{
            [APP runBlockInMainQ:^{
                [SVProgressHUD showWithStatus:@"当前没有种植植物，请选择植物" maskType:SVProgressHUDMaskTypeClear];
                [(RightMenuViewController*)[self.mm_drawerController rightDrawerViewController] selectedIdx:kPlantChoiceIdx];
            }];
        }
        
    } text:@"稍等"];

    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
