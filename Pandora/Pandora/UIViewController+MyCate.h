//
//  UIViewController+MyCate.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/6.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIViewController(MyCate)
+(NSString*)nibName;
+(instancetype)getInstanceWithSameNibName;
@end
