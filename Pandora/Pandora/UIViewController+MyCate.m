//
//  UIViewController+MyCate.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/6.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "UIViewController+MyCate.h"

@implementation UIViewController(MyCate)
+(NSString *)nibName{
    return NSStringFromClass([self class]);
}
+(instancetype)getInstanceWithSameNibName{
    id instance = [[[self class] alloc] initWithNibName:[self nibName] bundle:nil];
    return instance;
}
@end
