//
//  UIViewController+ShowRight.h
//  Pandora
//
//  Created by junchen on 12/6/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIViewController(ShowRight)
-(void)showRight:(id)sender;

@end
