//
//  UIViewController+ShowRight.m
//  Pandora
//
//  Created by junchen on 12/6/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "UIViewController+ShowRight.h"

@implementation UIViewController(ShowRight)
-(void)showRight:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}
@end
