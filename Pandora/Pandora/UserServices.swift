////
////  UserServices.swift
////  Pandora
////
////  Created by 小南宫的mac on 14/11/12.
////  Copyright (c) 2014年 NG. All rights reserved.
////
//
//import UIKit
//
//class UserServices: NSObject {
//    
//    class var shareInstance : UserServices{
//        struct Singleton{
//            static let instance = UserServices()
//        }
//        return   Singleton.instance
//    }
//    
//    
//    func register(un:String,pwd:String,mobile:String,nick:String,verify:String)->Bool{
//        let manager = HttpManager.defaultManager()
//        
//        var tf:Bool = false
//        let rf = RegisterForm()
//        rf.username = un
//        rf.password = pwd
//        rf.mobile = mobile
//        rf.nickname = nick
//        rf.verify_code=verify
//        
//        manager.postSecretRequest(API.shareInstance.BASE_API, model: rf.toDictionary(), complete: { (succeed:Bool!, dict:AnyObject!, response:NSURLResponse!) -> Void in
//            var error:NSError?
//            var loginRes = RegisterResponse(dictionary: dict as NSDictionary , error: &error)
//            let httpres = response as NSHTTPURLResponse
//            let headers = httpres.allHeaderFields;
//            APP.shareInstance.SESSIONID = headers["PHPSESSID"] as String
//            tf = succeed
//            }, sync: true)
//        return tf
//    }
//    
//    func login(usernm:String,passwd:String)->Bool{
//        let manager = HttpManager.defaultManager()
//        let form = LoginForm()
//        form.username = usernm
//        form.password = passwd
//        var error: NSError? = nil
//        var tf = false;
//        manager.postSecretRequest(API.shareInstance.BASE_API, model: form.toDictionary(), complete: { (succeed:Bool!, dict:AnyObject!, response:NSURLResponse!) -> Void in
//            var error:NSError?
//            let loginRes = LoginResponse(dictionary: dict as NSDictionary , error: &error)
//            let httpres = response as NSHTTPURLResponse
//            let headers = httpres.allHeaderFields as Dictionary;
//            APP.shareInstance.SESSIONID = headers["PHPSESSID"] as String
//            tf = succeed
//            }, sync: true)
//        return tf;
//    }
//    
//    
//    func getUserInfo()->GetUserInfoResponse?{
//        let form = GetUserInfoForm()
//        let manager = HttpManager.defaultManager()
//        var returnInfo:GetUserInfoResponse? = nil
//        manager.postSecretRequest(API.shareInstance.BASE_API, model: form.toDictionary(), complete: { (succeed:Bool!, dict:AnyObject!, response:NSURLResponse!) -> Void in
//            var error:NSError?
//            let response = GetUserInfoResponse(dictionary: dict as NSDictionary, error: &error)
//            returnInfo = response;
//            }, sync: true)
//        return returnInfo
//    }
//    
//    
//    func editUserInfo(nickname:String,realname:String)->EditUserInfoResponse?{
//        let form = EditUserInfoForm()
//        form.nickname = nickname;
//        form.realname = realname;
//        
//        let manager = HttpManager.defaultManager()
//        var returninfo:EditUserInfoResponse? = nil
//        manager.postSecretRequest(API.shareInstance.BASE_API, model: form.toDictionary(), complete: { (succeed:Bool!, dict:AnyObject!, response:NSURLResponse!) -> Void in
//            var error:NSError?
//            let response = EditUserInfoResponse(dictionary: dict as NSDictionary, error: &error)
//            returninfo = response;
//            }, sync: true)
//        return returninfo
//    }
//    
//    
//    func checkMobileRegistered(mobile:String)->CheckMobileRegisteredResponse?{
//        let form = CheckMobileRegisteredForm()
//        form.mobile = mobile
//        let manager = HttpManager.defaultManager()
//        var returninfo:CheckMobileRegisteredResponse? = nil
//        manager.postSecretRequest(API.shareInstance.BASE_API, model: form.toDictionary(), complete: { (succeed:Bool!, dict:AnyObject!, response:NSURLResponse!) -> Void in
//            var error:NSError?
//            let response = CheckMobileRegisteredResponse(dictionary: dict as NSDictionary, error: &error)
//            returninfo = response;
//            }, sync: true)
//        return returninfo
//    }
//    
//    func sendVerifyCode(mobile:String)->SendVerifyCodeResponse?{
//        let form = SendVerifyCodeForm()
//        form.mobile = mobile
//        let manager = HttpManager.defaultManager()
//        var returninfo:SendVerifyCodeResponse? = nil
//        manager.postSecretRequest(API.shareInstance.BASE_API, model: form.toDictionary(), complete: { (succeed:Bool!, dict:AnyObject!, response:NSURLResponse!) -> Void in
//            var error:NSError?
//            let response = SendVerifyCodeResponse(dictionary: dict as NSDictionary, error: &error)
//            returninfo = response;
//            }, sync: true)
//        return returninfo
//    }
//    
//}
