//
//  GetAccountListForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "DefaultForm.h"
#define API_NAME_GETACCOUNTLISTFORM @"plant.account.getAccountList"
@interface GetAccountListForm : DefaultForm
@property (strong, nonatomic) NSString* opt;
@property (assign, nonatomic) int firstRow;
@end
