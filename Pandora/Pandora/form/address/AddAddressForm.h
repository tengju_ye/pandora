//
//  AddAddressForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_AddAddress @"plant.address.addAddress"
@interface AddAddressForm : DefaultForm

@property (strong, nonatomic) NSString* realname;
@property (strong, nonatomic) NSString* mobile;
@property (assign, nonatomic) int province_id;
@property (assign, nonatomic) int city_id;
@property (assign, nonatomic) int area_id;
@property (strong, nonatomic) NSString* address;

@end
