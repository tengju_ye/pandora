//
//  CalExpressFeeForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_CalExpressFee @"plant.address.calExpressFee"

@interface CalExpressFeeForm : DefaultForm
@property (assign, nonatomic) int city_id;
@property (assign, nonatomic) int total_weight;
@property (assign, nonatomic) float total_amount;
@end
