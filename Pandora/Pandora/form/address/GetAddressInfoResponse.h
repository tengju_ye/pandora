//
//  GetAddressInfoResponse.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/6.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "DefaultResponse.h"
#import "Address.h"
@interface GetAddressInfoResponse : DefaultResponse
@property(nonatomic,strong) Address* data;
@end
