//
//  GetAreaListForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetAreaList @"plant.address.getAreaList"

@interface GetAreaListForm : DefaultForm

@property (assign, nonatomic) int city_id;

@end
