//
//  GetCityListForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetCityList @"plant.address.getCityList"

@interface GetCityListForm : DefaultForm

@property (assign, nonatomic) int province_id;

@end
