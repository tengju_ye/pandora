//
//  GetProvinceListForm.m
//  Pandora
//
//  Created by wang on 14-11-22.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "GetProvinceListForm.h"

@implementation GetProvinceListForm
-(instancetype)init{
    if (self = [super init]) {
        self.api_name = APINAME_GetProvinceList;
    }
    return self;
}
@end
