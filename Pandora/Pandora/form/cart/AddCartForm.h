//
//  AddCartForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_AddCart @"plant.cart.addCart"

@interface AddCartForm : DefaultForm

@property (assign, nonatomic) int item_id;
@property (strong, nonatomic) NSString* item_str;

@end
