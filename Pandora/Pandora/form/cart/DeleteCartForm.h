//
//  DeleteCartForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_DeleteCart @"plant.cart.deleteCart"

@interface DeleteCartForm : DefaultForm

@property (assign, nonatomic) int shopping_cart_id;

@end
