//
//  GetCartListByIdsForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetCartList @"plant.cart.getCartList"

@interface GetCartListByIdsForm : DefaultForm

@property (strong, nonatomic) NSString* shopping_car_ids;

@end
