//
//  GetCartListForm.m
//  Pandora
//
//  Created by wang on 14-11-22.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "GetCartListForm.h"

@implementation GetCartListForm
-(instancetype)init{
    if (self = [super init]) {
        self.api_name = APINAME_GetCartList;
    }
    return self;
}
@end
