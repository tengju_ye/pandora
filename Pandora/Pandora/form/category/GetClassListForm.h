//
//  GetClassListForm.h
//  Pandora
//
//  Created by wang on 14-11-22.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "DefaultForm.h"
#define APINAME_GetClassList @"plant.category.getClassList"

@interface GetClassListForm : DefaultForm

@end
