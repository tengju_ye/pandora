//
//  GetSortListByClassIdForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetSortListByClassId @"plant.category.getSortListByClassId"

@interface GetSortListByClassIdForm : DefaultForm

@property (assign, nonatomic) int class_id;

@end
