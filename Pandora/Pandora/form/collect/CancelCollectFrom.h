//
//  CancelCollectFrom.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_CancelCollect @"plant.collect.cancelCollect"
@interface CancelCollectFrom : DefaultForm

@property (assign, nonatomic) int item_id;

@end
