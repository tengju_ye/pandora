//
//  GetCollectListForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetCollectList @"plant.collect.getCollectList"

@interface GetCollectListForm : DefaultForm

@property (assign, nonatomic) int firstRow;

@end
