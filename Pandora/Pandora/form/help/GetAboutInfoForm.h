//
//  GetAboutInfoForm.h
//  Pandora
//
//  Created by wang on 14-11-22.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "DefaultForm.h"
#define GETABOUTINFO_API_NAME @"plant.help.getAboutInfo"
@interface GetAboutInfoForm : DefaultForm

@end
