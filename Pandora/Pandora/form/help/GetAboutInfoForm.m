

//
//  GetAboutInfoForm.m
//  Pandora
//
//  Created by wang on 14-11-22.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "GetAboutInfoForm.h"

@implementation GetAboutInfoForm
-(instancetype)init{
    if (self=[super init]) {
        self.api_name = GETABOUTINFO_API_NAME;
    }
    return self;
}
@end
