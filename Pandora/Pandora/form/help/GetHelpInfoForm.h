//
//  GetHelpInfoForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define GETHELPINFO_API_NAME @"plant.help.getHelpInfo"
@interface GetHelpInfoForm : DefaultForm

@property (assign, nonatomic) int help_id;

@end
