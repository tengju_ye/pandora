//
//  GetItemInfoForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#define APINAME_GetItemInfo @"plant.item.getItemInfo"

@interface GetItemInfoForm : DefaultForm

@property (assign, nonatomic) int item_id;

@end
