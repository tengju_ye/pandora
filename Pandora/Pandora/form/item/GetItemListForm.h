//
//  GetItemListForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetItemList @"plant.item.getItemList"

@interface GetItemListForm : DefaultForm

@property (strong, nonatomic) NSNumber<Optional>* firstRow;
@property (strong, nonatomic) NSNumber<Optional>* class_id;
@property (strong, nonatomic) NSNumber<Optional>* sort_id;
@property (strong, nonatomic) NSString<Optional>* class_tag;
@property (strong, nonatomic) NSString<Optional>* item_name;

@end
