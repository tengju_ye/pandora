//
//  GetItemListGroupBySortForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetItemListGroupBySort @"plant.item.getItemListGroupBySort"
@interface GetItemListGroupBySortForm : DefaultForm

@property (assign, nonatomic) NSString *class_id;

@end
