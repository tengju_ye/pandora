//
//  GetItemPhotoListForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetItemPhotoList @"plant.item.getItemPhotoList"

@interface GetItemPhotoListForm : DefaultForm

@property (assign, nonatomic) int item_id;

@end
