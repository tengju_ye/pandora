//
//  GetItemSkuForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetItemSku @"plant.item.getItemSku"

@interface GetItemSkuForm : DefaultForm

@property (assign, nonatomic) int item_id;

@end
