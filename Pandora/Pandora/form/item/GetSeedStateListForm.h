//
//  GetSeedStateListForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetSeedStateList @"plant.item.getSeedStateList"

@interface GetSeedStateListForm : DefaultForm

@property (assign, nonatomic) int item_id;
@property (nonatomic,strong) NSString<Optional>* is_diy;

@end
