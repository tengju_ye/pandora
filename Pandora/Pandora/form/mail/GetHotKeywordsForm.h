//
//  GetHotKeywordsForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetHotKeywords @"plant.mall.getHotKeywords"

@interface GetHotKeywordsForm : DefaultForm

@property (assign, nonatomic) int firstRow;

@end
