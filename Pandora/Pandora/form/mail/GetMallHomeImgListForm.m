

//
//  GetHomeImgListForm.m
//  Pandora
//
//  Created by wang on 14-11-22.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "GetMallHomeImgListForm.h"

@implementation GetMallHomeImgListForm
-(instancetype)init{
    if (self = [super init]) {
        self.api_name = APINAME_GetMallHomeImgList;
    }
    return self;
}
@end
