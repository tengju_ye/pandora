//
//  AddOrderForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_AddOrder @"plant.order.addOrder"

@interface AddOrderForm : DefaultForm

@property (assign, nonatomic) int user_address_id;
@property (strong, nonatomic) NSString* shopping_cart_ids;
@property (strong, nonatomic) NSString* number_str;
@property (assign, nonatomic) int payway_id;
@property (strong, nonatomic) NSString* payway_name;
@property (strong, nonatomic) NSString* user_remark;

@end
