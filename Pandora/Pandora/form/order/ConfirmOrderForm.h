//
//  ConfirmOrderForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_ConfirmOrder @"plant.order.confirmOrder"

@interface ConfirmOrderForm : DefaultForm

@property (strong, nonatomic) NSString* order_id;

@end
