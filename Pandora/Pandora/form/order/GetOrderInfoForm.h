//
//  GetOrderInfoForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetOrderInfo @"plant.order.getOrderInfo"

@interface GetOrderInfoForm : DefaultForm

@property (strong, nonatomic) NSString* order_id;

@end
