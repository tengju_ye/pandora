//
//  GetOrderInfoForm.m
//  x
//
//  Created by wang on 14-11-3.
//
//

#import "GetOrderInfoForm.h"

@implementation GetOrderInfoForm
-(instancetype)init{
    if (self = [super init]) {
        self.api_name = APINAME_GetOrderInfo;
    }
    return self;
}
@end
