//
//  GetOrderListForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetOrderList @"plant.order.getOrderList"

@interface GetOrderListForm : DefaultForm

@property (assign, nonatomic) NSNumber<Optional>* order_status;
//@property (assign, nonatomic) int firstRow;

@end
