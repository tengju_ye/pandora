//
//  GetPauInfoByOrderIdForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetPayInfoByOrderId @"plant.pay.getPayInfoByOrderId"

@interface GetPayInfoByOrderIdForm : DefaultForm

@property (strong, nonatomic) NSString* order_id;

@end
