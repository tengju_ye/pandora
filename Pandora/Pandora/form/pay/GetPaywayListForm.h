//
//  GetPaywayListForm.h
//  Pandora
//
//  Created by wang on 14-11-22.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "DefaultForm.h"
#define APINAME_GetPaywayList @"plant.pay.getPaywayList"

@interface GetPaywayListForm : DefaultForm

@end
