
//
//  GetPaywayListForm.m
//  Pandora
//
//  Created by wang on 14-11-22.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "GetPaywayListForm.h"

@implementation GetPaywayListForm
-(instancetype)init{
    if (self = [super init]) {
        self.api_name = APINAME_GetPaywayList;
    }
    return self;
}
@end
