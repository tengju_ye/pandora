//
//  GetRechargeInfoForm.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_GetRechargeInfo @"plant.pay.getRechargeInfo"

@interface GetRechargeInfoForm : DefaultForm

@property (assign, nonatomic) float amount;
@property (assign, nonatomic) int payway_id;

@end
