//
//  PayOrderByWalletForm.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/6.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "DefaultForm.h"
#define APINAME_PayOrderByWallet @"plant.pay.payOrderByWallet"
@interface PayOrderByWalletForm : DefaultForm
@property(nonatomic,strong) NSString* order_id;
@end
