//
//  BindNewPlanterForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define BINDNEWPLANTER_API_NAME @"plant.planter.bindNewPlanter"
@interface BindNewPlanterForm : DefaultForm

@property (strong, nonatomic) NSString* serial_num;

@end
