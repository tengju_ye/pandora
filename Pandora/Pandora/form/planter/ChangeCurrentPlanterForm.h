//
//  ChangeCurrentPlanterForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define CHANGECURRENTPLANTER_API_NAME @"plant.planter.changeCurrentPlanter"

@interface ChangeCurrentPlanterForm : DefaultForm

@property (assign, nonatomic) int planter_id;

@end
