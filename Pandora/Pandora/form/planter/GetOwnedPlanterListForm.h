//
//  GetOwnedPlanterListForm.h
//  Pandora
//
//  Created by 小南宫的mac on 15/2/11.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "DefaultForm.h"
#define APINAME_GetOwnedPlanterList @"plant.planter.getOwnedPlanterList"

@interface GetOwnedPlanterListForm : DefaultForm

@end
