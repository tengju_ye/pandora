//
//  GetPlanterAuthListForm.m
//  Pandora
//
//  Created by 小南宫的mac on 15/2/11.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "GetPlanterAuthListForm.h"

@implementation GetPlanterAuthListForm
-(instancetype)init{
    if (self = [super init]) {
        self.api_name = APINAME_GetPlanterAuthList;
    }
    return self;
}
@end
