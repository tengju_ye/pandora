//
//  GetPlanterInfoForm.m
//  Pandora
//
//  Created by wang on 14-11-22.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "GetPlanterInfoForm.h"

@implementation GetPlanterInfoForm
-(instancetype)init{
    if (self==[super init]) {
        self.api_name = GETPLANTERINFO_API_NAME;
    }
    return self;
}
@end
