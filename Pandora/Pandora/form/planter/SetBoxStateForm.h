//
//  SetBoxStateForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define SETBOXSTATE_API_NAME @"plant.planter.setBoxState"

@interface SetBoxStateForm : DefaultForm

@property (assign, nonatomic) int serial;
@property (assign, nonatomic) int state;

@end
