//
//  SetModeForm.h
//  Pandora
//
//  Created by junchen on 12/6/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "DefaultForm.h"
#define SETMODE_API_NAME @"plant.planter.setMode"
@interface SetModeForm : DefaultForm

@property (strong, nonatomic) NSString* mode;

@end
