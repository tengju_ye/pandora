//
//  SetModeForm.m
//  Pandora
//
//  Created by junchen on 12/6/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "SetModeForm.h"

@implementation SetModeForm
-(instancetype)init{
    if (self==[super init]) {
        self.api_name = SETMODE_API_NAME;
    }
    return self;
}
@end
