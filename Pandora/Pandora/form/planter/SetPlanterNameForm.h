//
//  SetPlanterNameForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define SETPLANTERNAME_API_NAME @"plant.planter.setPlanterName"

@interface SetPlanterNameForm : DefaultForm

@property (assign, nonatomic) int planter_id;
@property (strong, nonatomic) NSString* planter_name;

@end
