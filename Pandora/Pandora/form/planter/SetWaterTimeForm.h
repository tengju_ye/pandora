//
//  SetWaterTimeForm.h
//  Pandora
//
//  Created by junchen on 12/6/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "DefaultForm.h"
#define SETWATERTIME_API_NAME @"plant.planter.setWaterTime"
@interface SetWaterTimeForm : DefaultForm

@property (strong, nonatomic) NSString* ton;
@property (strong, nonatomic) NSString* toff;

@end
