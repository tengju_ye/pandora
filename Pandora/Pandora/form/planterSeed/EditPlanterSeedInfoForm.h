//
//  EditPlanterSeedInfoForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define EDITPLANTERSEEDINFO_API_NAME @"plant.planterSeed.editPlanterSeedInfo"
@interface EditPlanterSeedInfoForm : DefaultForm

@property (assign, nonatomic) int planter_seed_id;
@property (assign, nonatomic) int state;
@property (assign, nonatomic) int seed_id;
@property (assign, nonatomic) int seed_state_id;
@property (assign, nonatomic) float t_num;
@property (assign, nonatomic) float h_num;
@property (assign, nonatomic) float i_num;

@end
