//
//  GetPlanterSeedInfoForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define GETPLANTERSEEDINFO_API_NAME @"plant.planterSeed.getPlanterSeedInfo"
@interface GetPlanterSeedInfoForm : DefaultForm

@property (assign, nonatomic) int planter_seed_id;

@end
