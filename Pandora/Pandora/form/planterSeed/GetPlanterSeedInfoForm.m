//
//  GetPlanterSeedInfoForm.m
//  x
//
//  Created by wang on 14-11-2.
//
//

#import "GetPlanterSeedInfoForm.h"

@implementation GetPlanterSeedInfoForm
-(instancetype)init{
    if (self=[super init]) {
        self.api_name = GETPLANTERSEEDINFO_API_NAME;
    }
    return self;
}
@end
