//
//  GetPlanterSeedListForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define GETPLANTERSEEDLIST_API_NAME @"plant.planterSeed.getPlanterSeedList"
typedef enum {
    PlanterState_ING = 1,
    PlanterState_DONE = 2
}PlanterState;
@interface GetPlanterSeedListForm : DefaultForm
@property (assign, nonatomic) int state;
@property (assign, nonatomic) int firstRow;

@end
