//
//  GetPlanterSeedListForm.m
//  x
//
//  Created by wang on 14-11-2.
//
//

#import "GetPlanterSeedListForm.h"

@implementation GetPlanterSeedListForm
-(instancetype)init{
    if (self=[super init]) {
        self.api_name = GETPLANTERSEEDLIST_API_NAME;
    }
    return self;
}
@end
