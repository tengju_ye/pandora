//
//  GetPlanterSeedLogListForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define GETPLANTERSEEDLOGLIST_API_NAME @"plant.planterSeed.getPlanterSeedLogList"

@interface GetPlanterSeedLogListForm : DefaultForm

@property (assign, nonatomic) int planter_seed_id;

@end
