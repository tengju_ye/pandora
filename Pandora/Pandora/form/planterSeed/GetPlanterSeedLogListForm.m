//
//  GetPlanterSeedLogListForm.m
//  x
//
//  Created by wang on 14-11-2.
//
//

#import "GetPlanterSeedLogListForm.h"

@implementation GetPlanterSeedLogListForm
-(instancetype)init{
    if (self=[super init]) {
        self.api_name = GETPLANTERSEEDLOGLIST_API_NAME;
    }
    return self;
}
@end
