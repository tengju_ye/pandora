//
//  GetPlanterSeedStateForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define GETPLANTERSEEDSTATE_API_NAME @"plant.planterSeed.getPlanterSeedState"
@interface GetPlanterSeedStateForm : DefaultForm

@property (assign, nonatomic) int planter_seed_id;
@property (assign, nonatomic) int seed_state_id;

@end
