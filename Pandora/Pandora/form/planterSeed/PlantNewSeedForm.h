//
//  PlantNewSeedForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define PLANTNEWSEEDFORM_API_NAME @"plant.planterSeed.plantNewSeed"

@interface PlantNewSeedForm : DefaultForm

@property (assign, nonatomic) int seed_id;
@property (assign, nonatomic) int seed_state_id;
@property (assign, nonatomic) int state;

@end
