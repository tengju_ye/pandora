//
//  RecoverPlanterSeedStateForm.h
//  Pandora
//
//  Created by 小南宫的mac on 14/12/18.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "DefaultForm.h"
#define APINAME_Recover @"plant.planterSeed.recoverPlanterSeedState"

@interface RecoverPlanterSeedStateForm : DefaultForm
@property(nonatomic,strong) NSString* planter_seed_id;//	int	从我种植的植物中选择的植物ID	是
@property(nonatomic,strong) NSString* state;//	int	植物状态，1-9	是
@property(nonatomic,strong) NSString* seed_id;//	int	植物id	是
@end
