//
//  SetPlanterSeedStateForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define SETPLANTERSEEDSTATE_API_NAME @"plant.planterSeed.setPlanterSeedState"
@interface SetPlanterSeedStateForm : DefaultForm

@property (assign, nonatomic) int planter_seed_id;
@property (assign, nonatomic) int state;
@property (assign, nonatomic) int seed_id;

@end
