//
//  UserCheckMobileRegisteredForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface CheckMobileRegisteredForm : DefaultForm

@property (strong, nonatomic) NSString* mobile;

@end
