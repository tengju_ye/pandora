//
//  UserEditUserInfoForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define APINAME_EditUserInfo @"plant.user.editUserInfo"

@interface EditUserInfoForm : DefaultForm
@property (strong, nonatomic) NSString* mobile;
@property (strong, nonatomic) NSString* realname;
@property (strong, nonatomic) NSString* nickname;

@end
