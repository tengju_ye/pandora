//
//  GetUserInfoForm.m
//  Pandora
//
//  Created by 小南宫的mac on 14/11/12.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "GetUserInfoForm.h"

@implementation GetUserInfoForm
-(instancetype)init{
    if (self = [super init]) {
        self.api_name = GETUSERINFO_API_NAME;
    }
    return self;
}
@end
