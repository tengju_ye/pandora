//
//  UserLoginForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "DefaultForm.h"
#define LOGIN_API_NAME @"plant.user.signin"
@interface LoginForm : DefaultForm
@property (strong, nonatomic) NSString* mobile;
@property (strong, nonatomic) NSString<Optional>* password;
@property (strong, nonatomic) NSString<Optional>* verify_code;
@end
