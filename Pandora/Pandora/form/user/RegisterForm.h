//
//  Register.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#define REGISTER_API_NAME @"plant.user.signup"
@interface RegisterForm : DefaultForm
@property (strong, nonatomic) NSString* mobile;
@property (strong, nonatomic) NSString* verify_code;
@end
