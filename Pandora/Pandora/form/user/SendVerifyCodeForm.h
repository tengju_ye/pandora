//
//  UserSendVerifyCodeForm.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#define API_SENDVERIFYCODE @"plant.user.sendVerifyCode"
@interface SendVerifyCodeForm : DefaultForm

@property (strong, nonatomic) NSString* mobile;

@end
