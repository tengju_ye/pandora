//
//  SetPasswordForm.h
//  Pandora
//
//  Created by 小南宫的mac on 15/1/17.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "DefaultForm.h"
#define APINAME_SetPassword @"plant.user.setPassword"

@interface SetPasswordForm : DefaultForm
@property (nonatomic,strong) NSString* old_password;
@property (nonatomic,strong) NSString* password;
@end
