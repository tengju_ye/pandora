//
//  DefaultResponse.h
//  Pandora
//
//  Created by 小南宫的mac on 14/11/27.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "JSONModel.h"

@interface DefaultResponse: JSONModel
@property (strong, nonatomic) NSString* code;
@property (strong,nonatomic) NSString<Optional>* error_msg;
@end
