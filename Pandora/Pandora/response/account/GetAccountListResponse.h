//
//  GetAccountListResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Account.h"
#import "DefaultResponse.h"
@interface GetAccountListResponse : DefaultResponse
@property (strong, nonatomic) NSString* total_num;
@property (assign, nonatomic) int nextFirstRow;
@property (strong, nonatomic) NSArray<Account>* account_list;
@end
