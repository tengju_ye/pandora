

//
//  GetAccountListResponse.m
//  x
//
//  Created by wang on 14-11-3.
//
//

#import "GetAccountListResponse.h"

@implementation GetAccountListResponse

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.total_num": @"total_num",
                                 @"data.nextFirstRow": @"nextFirstRow",
                                 @"data.account_list": @"account_list",}];
}

@end
