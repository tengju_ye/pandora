//
//  Account.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@protocol Account@end
@interface Account : JSONModel
@property (strong, nonatomic) NSString* remark;
@property (strong, nonatomic) NSString* addtime;
@property (strong, nonatomic) NSString* amount_before_pay;
@property (strong, nonatomic) NSString* amount_after_pay;
@property (strong, nonatomic) NSString* amount_in;
@end
