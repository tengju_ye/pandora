//
//  CalExpressFeeResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface CalExpressFeeResponse: DefaultResponse

@property (strong, nonatomic) NSString* city_name;
@property (assign, nonatomic) double express_fee;
@end
