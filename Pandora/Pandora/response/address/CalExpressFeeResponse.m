


//
//  CalExpressFeeResponse.m
//  x
//
//  Created by wang on 14-11-3.
//
//

#import "CalExpressFeeResponse.h"

@implementation CalExpressFeeResponse

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.city_name": @"city_name",
                                 @"data.express_fee": @"express_fee"}];
}

@end
