//
//  EditAddressResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface EditAddressResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;

@end
