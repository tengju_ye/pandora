//
//  GetAddressListResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Address.h"

@interface GetAddressListResponse: DefaultResponse

@property (strong, nonatomic) NSArray<Address>* data;

@end
