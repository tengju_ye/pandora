//
//  GetProvinceListResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Province.h"


@interface GetProvinceListResponse: DefaultResponse
@property (strong, nonatomic) NSArray<Province>* data;
@end
