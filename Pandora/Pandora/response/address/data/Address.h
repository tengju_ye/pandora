//
//  Address.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"


@protocol Address <NSObject>
@end
@interface Address : JSONModel
@property (strong, nonatomic) NSString* user_address_id;
@property (strong, nonatomic) NSString* mobile;
@property (strong, nonatomic) NSString* address;
@property (strong, nonatomic) NSString* realname;
@property (strong, nonatomic) NSString* area_string;
@property (strong, nonatomic) NSString* province_id;
@property (strong, nonatomic) NSString* city_id;
@property (strong, nonatomic) NSString* area_id;

@property (strong,nonatomic) NSString<Ignore>* province_name;
@property (strong,nonatomic) NSString<Ignore>* city_name;
@property (strong,nonatomic) NSString<Ignore>* area_name;

@end
