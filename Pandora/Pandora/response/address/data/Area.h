//
//  Area.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
@protocol Area @end
@interface Area : JSONModel

@property (strong, nonatomic) NSString* area_id;
@property (strong, nonatomic) NSString* area_name;

@end
