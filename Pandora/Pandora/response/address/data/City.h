//
//  City.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@protocol City @end

@interface City : JSONModel

@property (strong, nonatomic) NSString* city_id;
@property (strong, nonatomic) NSString* city_name;

@end
