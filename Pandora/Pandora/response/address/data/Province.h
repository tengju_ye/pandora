//
//  Province.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@protocol Province@end
@interface Province : JSONModel

@property (strong, nonatomic) NSString* province_id;
@property (strong, nonatomic) NSString* province_name;

@end
