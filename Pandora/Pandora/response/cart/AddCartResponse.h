//
//  AddCartResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface AddCartResponse: DefaultResponse

@property (strong, nonatomic) NSString* shopping_cart_id_list;
@property (strong, nonatomic) NSString* number_list;

@end
