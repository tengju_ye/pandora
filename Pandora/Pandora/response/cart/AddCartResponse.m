//
//  AddCartResponse.m
//  x
//
//  Created by wang on 14-11-3.
//
//

#import "AddCartResponse.h"

@implementation AddCartResponse

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.shopping_cart_id_list": @"shopping_cart_id_list",
                                 @"data.number_list": @"number_list"}];
}

@end
