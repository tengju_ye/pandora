//
//  DeleteCartResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>

@interface DeleteCartResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;

@end
