//
//  GetCartListByIdsResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "CartByIds.h"
#import "Cart.h"
@interface GetCartListByIdsResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSArray<Cart>* data;

@end
