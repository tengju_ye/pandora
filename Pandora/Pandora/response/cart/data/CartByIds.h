//
//  CartByIds.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Cart.h"
@protocol CartByIds @end

@interface CartByIds : JSONModel

@property (strong, nonatomic) NSString* shopping_cart_id;
@property (strong, nonatomic) NSString* user_id;
@property (strong, nonatomic) NSString* item_id;
@property (strong, nonatomic) NSString* mall_price;
@property (strong, nonatomic) NSString* real_price;
@property (strong, nonatomic) NSString* total_price;
@property (strong, nonatomic) NSString* item_sku_price_id;
@property (strong, nonatomic) NSString* number;
@property (strong, nonatomic) NSString* item_name;
@property (strong, nonatomic) NSString* property;
@property (strong, nonatomic) NSString* small_pic;
@property (strong, nonatomic) NSString* weight;
@property (strong, nonatomic) NSString* addtime;

-(Cart*)getCart;
@end
