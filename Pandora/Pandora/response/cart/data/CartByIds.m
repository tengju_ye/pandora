

//
//  CartByIds.m
//  x
//
//  Created by wang on 14-11-3.
//
//

#import "CartByIds.h"

@implementation CartByIds
-(Cart *)getCart{
    Cart* cart = [[Cart alloc] init];
    cart.shopping_cart_id = self.shopping_cart_id;
    cart.item_id  = self.item_id;
    cart.mall_price = self.mall_price;
    cart.real_price = self.real_price;
    cart.number = self.number;
    cart.total_price = self.total_price;
    cart.item_sku_price_id = self.item_sku_price_id;
    cart.item_name = self.item_name;
    cart.property = self.property;
    cart.small_pic = self.small_pic;
    cart.perWeight = @(self.weight.integerValue);
    return cart;
}
@end
