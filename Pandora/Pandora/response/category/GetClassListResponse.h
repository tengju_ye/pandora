//
//  GetClassListResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "AClass.h"


@interface GetClassListResponse : DefaultResponse
@property (strong, nonatomic) NSArray<AClass>* data;
@end
