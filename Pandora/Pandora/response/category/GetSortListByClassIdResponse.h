//
//  GetSortListByClassIdResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Sort.h"

@interface GetSortListByClassIdResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSArray<Sort>* data;

@end
