//
//  Class.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
@protocol AClass @end

@interface AClass : JSONModel

@property (strong, nonatomic) NSString* class_id;
@property (strong, nonatomic) NSString* class_name;
@property (strong, nonatomic) NSString* class_tag;
@property (strong, nonatomic) NSString* serial;
@property (strong, nonatomic) NSString* isuse;

@end
