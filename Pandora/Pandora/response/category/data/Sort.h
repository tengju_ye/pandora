//
//  Sort.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@protocol Sort@end
@interface Sort : JSONModel

@property (strong, nonatomic) NSString* sort_id;
@property (strong, nonatomic) NSString* class_id;
@property (strong, nonatomic) NSString* sort_name;
@property (strong, nonatomic) NSString* sort_tag;
@property (strong, nonatomic) NSString* serial;
@property (strong, nonatomic) NSString* isuse;

@end
