//
//  GetCollectListResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Collect.h"

@interface GetCollectListResponse: DefaultResponse
@property (strong, nonatomic) NSString* total_num;
@property (assign, nonatomic) int nextFirstRow;
@property (strong, nonatomic) NSArray<Collect>* collect_list;

@end
