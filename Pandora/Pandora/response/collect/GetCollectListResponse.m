//
//  GetCollectListResponse.m
//  x
//
//  Created by wang on 14-11-3.
//
//

#import "GetCollectListResponse.h"

@implementation GetCollectListResponse

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.total_num": @"total_num",
                                 @"data.nextFirstRow": @"nextFirstRow",
                                 @"data.collect_list": @"collect_list"}];
}

@end
