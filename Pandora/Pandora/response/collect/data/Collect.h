//
//  Collect.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@protocol Collect@end
@interface Collect : JSONModel

@property (strong, nonatomic) NSString* collect_id;
@property (strong, nonatomic) NSString* user_id;
@property (strong, nonatomic) NSString* item_id;
@property (strong, nonatomic) NSString* addtime;
@property (strong, nonatomic) NSString* item_name;
@property (strong, nonatomic) NSString* mall_price;
@property (strong, nonatomic) NSString* small_pic;
@property (strong, nonatomic) NSString* status;

@end
