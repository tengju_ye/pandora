//
//  GetAboutInfoResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface GetAboutInfoResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSString* article_id;
@property (strong, nonatomic) NSString* addtime;
@property (strong, nonatomic) NSString* help_title;
@property (strong, nonatomic) NSString* path_img;
@property (strong, nonatomic) NSString* help_contents;

@end
