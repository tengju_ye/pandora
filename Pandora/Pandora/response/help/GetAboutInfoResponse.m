



//
//  GetAboutInfoResponse.m
//  x
//
//  Created by wang on 14-11-3.
//
//

#import "GetAboutInfoResponse.h"

@implementation GetAboutInfoResponse

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.article_id": @"article_id",
                                 @"data.addtime": @"addtime",
                                 @"data.help_title": @"help_title",
                                 @"data.path_img": @"path_img",
                                 @"data.help_contents": @"help_contents"}];
}

@end
