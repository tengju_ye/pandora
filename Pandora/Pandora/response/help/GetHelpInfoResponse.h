//
//  GetHelpInfoResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface GetHelpInfoResponse: DefaultResponse

@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSString* addtime;
@property (strong, nonatomic) NSString* help_contents;

@end
