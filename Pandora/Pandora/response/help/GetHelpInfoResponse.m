

//
//  GetHelpInfoResponse.m
//  x
//
//  Created by wang on 14-11-3.
//
//

#import "GetHelpInfoResponse.h"

@implementation GetHelpInfoResponse


+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.title": @"title",
                                 @"data.addtime": @"addtime",
                                 @"data.help_contents": @"help_contents"}];
}

@end
