//
//  GetHelpListResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Help.h"

@interface GetHelpListResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSArray<Help>* data;

@end
