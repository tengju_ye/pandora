//
//  Article.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@protocol Article@end
@interface Article : JSONModel

@property (strong, nonatomic) NSString* help_id;
@property (strong, nonatomic) NSString* title;

@end
