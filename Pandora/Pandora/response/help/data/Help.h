//
//  Help.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Article.h"

@protocol Help@end
@interface Help : JSONModel

@property (strong, nonatomic) NSString* help_sort_id;
@property (strong, nonatomic) NSString* help_sort_name;
@property (strong, nonatomic) NSArray<Article>* article_list;

@end
