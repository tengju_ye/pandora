//
//  GetItemInfoResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Item.h"
@interface GetItemInfoResponse: DefaultResponse
@property(nonatomic,strong) Item* data;
//@property (strong, nonatomic) NSString* code;
//@property (strong, nonatomic) NSString* item_name;
//@property (strong, nonatomic) NSString* t_desc;
//@property (strong, nonatomic) NSString* h_desc;
//@property (strong, nonatomic) NSString* i_desc;
//@property (strong, nonatomic) NSString* item_sn;
//@property (strong, nonatomic) NSString* has_sku;
//@property (strong, nonatomic) NSString* base_pic;
//@property (strong, nonatomic) NSString* mall_price;
//@property (strong, nonatomic) NSString* market_price;
//@property (strong, nonatomic) NSString* stock;
//@property (strong, nonatomic) NSString* weight;
//@property (strong, nonatomic) NSString* sales_num;
//@property (strong, nonatomic) NSString* is_seed;

@end
