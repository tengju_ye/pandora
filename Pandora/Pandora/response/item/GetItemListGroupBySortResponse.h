//
//  GetItemListGroupBySortResponse.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "ItemList.h"

@interface GetItemListGroupBySortResponse: DefaultResponse
@property (strong, nonatomic) NSArray<ItemList>* data;
@end
