//
//  GetItemListResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Item.h"
/**
 
 "current_class_id": "10",
 "total_num": "10",
 "nextFirstRow": 6,
 */
@interface GetItemListResponse: DefaultResponse

@property (strong, nonatomic) NSArray<Item>* item_list;
@property (assign, nonatomic) NSInteger current_class_id;
@property (assign, nonatomic) NSInteger total_num;
@property (assign, nonatomic) NSInteger nextFirstRow;

@end
