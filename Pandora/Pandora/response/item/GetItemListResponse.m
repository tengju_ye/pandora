//
//  GetItemListResponse.m
//  x
//
//  Created by wang on 14-11-3.
//@property (strong, nonatomic) NSArray<Item>* itemlist;
//@property (assign, nonatomic) NSInteger current_class_id;
//@property (assign, nonatomic) NSInteger total_num;
//@property (assign, nonatomic) NSInteger nextFirstRow;
//

#import "GetItemListResponse.h"

@implementation GetItemListResponse
+(JSONKeyMapper *)keyMapper{
    
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"code": @"code",
                                                       @"data.current_class_id": @"current_class_id",
                                                       @"data.total_num": @"total_num",
                                                       @"data.item_list": @"item_list",                                                       @"data.nextFirstRow": @"nextFirstRow"

                                                       }];

}
@end
