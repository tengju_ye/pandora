//
//  GetItemPhotoListResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>

@interface GetItemPhotoListResponse: DefaultResponse

@property (strong, nonatomic) NSArray* data;

@end
