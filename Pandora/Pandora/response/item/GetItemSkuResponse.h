//
//  GetItemSkuResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Sku.h"

@interface GetItemSkuResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSArray<Sku>* sku_info;
@property (strong, nonatomic) NSString<Optional>* sku_name1;
@property (strong, nonatomic) NSString<Optional>* sku_name2;

@end
