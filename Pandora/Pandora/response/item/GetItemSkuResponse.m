//
//  GetItemSkuResponse.m
//  x
//
//  Created by wang on 14-11-3.
//
//

#import "GetItemSkuResponse.h"

@implementation GetItemSkuResponse

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{@"code": @"code",
                                 @"data.sku_info" :@"sku_info",
                                 @"data.sku_name1": @"sku_name1",
                                 @"data.sku_name2": @"sku_name2"}];
}
@end
