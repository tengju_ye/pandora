//
//  Item.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
@protocol Item @end

@interface Item : JSONModel

@property (strong, nonatomic) NSString<Optional>* item_id;
@property (strong, nonatomic) NSString<Optional>* item_name;
@property (strong, nonatomic) NSString<Optional>* title;
@property (strong, nonatomic) NSString<Optional>* keywords;
@property (strong, nonatomic) NSString<Optional>* description;
@property (strong, nonatomic) NSString<Optional>* t_desc;
@property (strong, nonatomic) NSString<Optional>* h_desc;
@property (strong, nonatomic) NSString<Optional>* i_desc;
@property (strong, nonatomic) NSString<Optional>* item_sn;
@property (strong, nonatomic) NSString<Optional>* has_sku;
@property (strong, nonatomic) NSString<Optional>* item_type_id;
@property (strong, nonatomic) NSString<Optional>* base_pic;
@property (strong, nonatomic) NSString<Optional>* cost_price;
@property (strong, nonatomic) NSString<Optional>* market_price;
@property (strong, nonatomic) NSString<Optional>* mall_price;
@property (strong, nonatomic) NSString<Optional>* class_id;
@property (strong, nonatomic) NSString<Optional>* sort_id;
@property (strong, nonatomic) NSString<Optional>* stock;
@property (strong, nonatomic) NSString<Optional>* stock_alarm;
@property (strong, nonatomic) NSString<Optional>* weight;
@property (strong, nonatomic) NSString<Optional>* is_delivery_free;
@property (strong, nonatomic) NSString<Optional>* sales_num;
@property (strong, nonatomic) NSString<Optional>* clickdot;
@property (strong, nonatomic) NSString<Optional>* addtime;
@property (strong, nonatomic) NSString<Optional>* serial;
@property (strong, nonatomic) NSString<Optional>* isuse;
@property (strong, nonatomic) NSString<Optional>* small_img;


//item from search
@property (strong, nonatomic) NSString<Optional>* status;
@property (strong, nonatomic) NSString<Optional>* middle_img;
@property (strong, nonatomic) NSString<Optional>* link_item;

@end
