//
//  ItemList.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Item.h"
@protocol ItemList @end

@interface ItemList : JSONModel

@property (strong, nonatomic) NSString* sort_id;
@property (strong, nonatomic) NSString* sort_name;
@property (strong, nonatomic) NSString* sort_tag;
@property (strong, nonatomic) NSArray<Item>* item_list;

//辅助数据结构,把item_list 两两切分
@property (nonatomic,strong) NSArray<Ignore>* pairItemlist;
@end
