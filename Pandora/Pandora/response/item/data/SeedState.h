//
//  SeedState.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "SeedState.h"
@protocol SeedState @end
@interface SeedState : JSONModel

@property (strong, nonatomic) NSString* seed_state_id;
@property (strong, nonatomic) NSString* state;
@property (strong, nonatomic) NSString* outside_temperature;
@property (strong, nonatomic) NSString* humidity;
@property (strong, nonatomic) NSString* illuminance_limit;
@property (strong, nonatomic) NSString* img_path;

@end
