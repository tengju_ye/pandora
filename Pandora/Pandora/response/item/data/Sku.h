//
//  Sku.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@protocol Sku @end
@interface Sku : JSONModel

@property (strong, nonatomic) NSString* item_sku_price_id;
@property (strong, nonatomic) NSString* item_id;
@property (strong, nonatomic) NSString* item_sn;
@property (strong, nonatomic) NSString* sku_price;
@property (strong, nonatomic) NSString* sku_stock;
@property (strong, nonatomic) NSString* serial;
@property (strong, nonatomic) NSString* property_value1;
@property (strong, nonatomic) NSString* property_value2;
@property (strong, nonatomic) NSString* isuse;
@property (strong, nonatomic) NSString* property_name1;
@property (strong, nonatomic) NSString* property_name2;

@property (nonatomic,strong) NSNumber<Optional>* currentValue;//not from json,default is 1
@end
