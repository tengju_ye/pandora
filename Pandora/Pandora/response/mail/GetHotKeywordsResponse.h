//
//  GetHotKeywordsResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "DefaultResponse.h"
@interface GetHotKeywordsResponse : DefaultResponse
@property (strong, nonatomic) NSString* data;
@end
