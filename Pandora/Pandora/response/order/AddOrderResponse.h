//
//  AddOrderResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface AddOrderResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSString* data;

@end
