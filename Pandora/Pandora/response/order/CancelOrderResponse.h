//
//  CancelOrderResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface CancelOrderResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;

@end
