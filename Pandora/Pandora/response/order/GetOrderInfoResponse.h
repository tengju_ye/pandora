//
//  GetOrderInfoResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "OrderItem.h"

@interface GetOrderInfoResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSString* order_id;
@property (strong, nonatomic) NSString* order_sn;
@property (strong, nonatomic) NSString* order_status;
@property (strong, nonatomic) NSString* pay_amount;
@property (strong, nonatomic) NSString* consignee;
@property (strong, nonatomic) NSString* express_company_name;
@property (strong, nonatomic) NSString* express_number;
@property (strong, nonatomic) NSString* express_fee;
@property (strong, nonatomic) NSString* payway_name;
@property (strong, nonatomic) NSString* addtime;
@property (strong, nonatomic) NSString* pay_time;
@property (strong, nonatomic) NSString* user_remark;
@property (strong, nonatomic) NSString* order_status_name;
@property (strong, nonatomic) NSString* area_string;
@property (strong, nonatomic) NSString* shop_name;
@property (strong, nonatomic) NSArray<OrderItem>* order_item_list;

@end
