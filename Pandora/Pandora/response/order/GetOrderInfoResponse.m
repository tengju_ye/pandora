
//
//  GetOrderInfoResponse.m
//  x
//
//  Created by wang on 14-11-3.
//
//

#import "GetOrderInfoResponse.h"

@implementation GetOrderInfoResponse

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.order_id": @"order_id",
                                 @"data.order_sn": @"order_sn",
                                 @"data.order_status": @"order_status",
                                 @"data.pay_amount": @"pay_amount",
                                 @"data.consignee": @"consignee",
                                 @"data.express_company_name": @"express_company_name",
                                 @"data.express_number": @"express_number",
                                 @"data.express_fee": @"express_fee",
                                 @"data.payway_name": @"payway_name",
                                 @"data.addtime": @"addtime",
                                 @"data.pay_time": @"pay_time",
                                 @"data.user_remark": @"user_remark",
                                 @"data.order_status_name": @"order_status_name",
                                 @"data.area_string": @"area_string",
                                 @"data.shop_name": @"shop_name",
                                 @"data.order_item_list": @"order_item_list"}];
}

@end
