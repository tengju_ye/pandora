//
//  GetOrderListResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Order.h"

@interface GetOrderListResponse: DefaultResponse

@property (assign, nonatomic) int pre_pay;
@property (assign, nonatomic) int delivered;
@property (assign, nonatomic) int confirmed;
@property (strong, nonatomic) NSString* NextFirstRow;
@property (strong, nonatomic) NSArray<Order,Optional>* order_list;
@property (strong, nonatomic) NSString* total;
@property (strong, nonatomic) NSString* pre_pay_order_num;
@property (strong, nonatomic) NSString* pre_deliver_order_num;
@property (strong, nonatomic) NSString* pre_confirm_order_num;

@end
