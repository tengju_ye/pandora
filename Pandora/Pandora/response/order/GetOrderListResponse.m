


//
//  GetOrderListResponse.m
//  x
//
//  Created by wang on 14-11-3.
//
//

#import "GetOrderListResponse.h"

@implementation GetOrderListResponse

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.pre_pay": @"pre_pay",
                                 @"data.delivered": @"delivered",
                                 @"data.confirmed": @"confirmed",
                                 @"data.NextFirstRow": @"NextFirstRow",
                                 @"data.order_list": @"order_list",
                                 @"data.total": @"total",
                                 @"data.pre_pay_order_num": @"pre_pay_order_num",
                                 @"data.pre_deliver_order_num": @"pre_deliver_order_num",
                                 @"data.pre_confirm_order_num": @"pre_confirm_order_num"}];
}

@end
