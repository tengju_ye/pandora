//
//  Order.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@protocol Order@end

@interface Order : JSONModel

@property (strong, nonatomic) NSString* order_status;
@property (strong, nonatomic) NSString* order_id;
@property (strong, nonatomic) NSString* pay_amount;
@property (strong, nonatomic) NSString* addtime;
@property (strong, nonatomic) NSString* order_status_name;

@end
