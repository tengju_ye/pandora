//
//  OrderItem.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@protocol OrderItem@end

@interface OrderItem : JSONModel

@property (strong, nonatomic) NSString* order_id;
@property (strong, nonatomic) NSString* item_id;
@property (strong, nonatomic) NSString* item_name;
@property (strong, nonatomic) NSString* item_sn;
@property (strong, nonatomic) NSString* item_sku_price_id;
@property (strong, nonatomic) NSString* property;
@property (strong, nonatomic) NSString* number;
@property (strong, nonatomic) NSString* real_price;
@property (strong, nonatomic) NSString* small_pic;

@end
