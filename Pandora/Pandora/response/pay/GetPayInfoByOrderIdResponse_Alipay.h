//
//  GetPayInfoByOrderIdResponse_Alipay.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface GetPayInfoByOrderIdResponse_Alipay : DefaultResponse

@property (strong, nonatomic) NSString* link;
@property (strong, nonatomic) NSString<Ignore>* qr_pay_mode_link;
@property (strong, nonatomic) NSString* amount;
@property (strong, nonatomic) NSString* pay_tag;
@property (strong, nonatomic) NSString* left_money;
@property (strong, nonatomic) NSString* system_money_name;

@end
