
//
//  GetPayInfoByOrderIdResponse_Unionpay.m
//  x
//
//  Created by wang on 14-11-3.
//
//

#import "GetPayInfoByOrderIdResponse_Unionpay.h"

@implementation GetPayInfoByOrderIdResponse_Unionpay

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.parameter.action_url": @"action_url",
                                 @"data.parameter.v_mid": @"v_mid",
                                 @"data.parameter.v_oid": @"v_oid",
                                 @"data.parameter.v_amout": @"v_amout",
                                 @"data.parameter.v_money_type": @"v_money_type",
                                 @"data.parameter.v_url": @"v_url",
                                 @"data.parameter.v_md5info": @"v_md5info",
                                 @"data.amount": @"amount",
                                 @"data.pay_tag": @"pay_tag",
                                 @"data.left_money": @"left_money",
                                 @"data.system_money_name": @"system_money_name"}];
}


@end
