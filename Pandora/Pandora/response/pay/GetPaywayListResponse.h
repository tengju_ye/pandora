//
//  GetPaywayListResponse.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "Payway.h"
@interface GetPaywayListResponse: DefaultResponse
@property (strong, nonatomic) NSArray<Payway>* data;
@end
