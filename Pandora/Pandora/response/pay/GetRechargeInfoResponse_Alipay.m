


//
//  GetRechargeInfoResponse_Alipay.m
//  x
//
//  Created by wang on 14-11-3.
//
//

#import "GetRechargeInfoResponse_Alipay.h"

@implementation GetRechargeInfoResponse_Alipay

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.link": @"link",
                                 @"data.qr_pay_mode_link": @"qr_pay_mode_link",
                                 @"data.amount": @"amount",
                                 @"data.pay_tag": @"pay_tag",
                                 @"data.left_money": @"left_money",
                                 @"data.system_money_name": @"system_money_name"}];
}

@end
