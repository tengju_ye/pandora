//
//  GetRechargeInfoResponse_Unionpay.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface GetRechargeInfoResponse_Unionpay : JSONModel

@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSString* action_url;
@property (strong, nonatomic) NSString* v_mid;
@property (strong, nonatomic) NSString* v_oid;
@property (assign, nonatomic) int v_mount;
@property (strong, nonatomic) NSString* v_moneytype;
@property (strong, nonatomic) NSString* v_url;
@property (strong, nonatomic) NSString* v_md5info;
@property (strong, nonatomic) NSString* amount;
@property (strong, nonatomic) NSString* pay_tag;
@property (strong, nonatomic) NSString* left_money;
@property (strong, nonatomic) NSString* system_money_name;


@end
