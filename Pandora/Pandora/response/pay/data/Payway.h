//
//  Payway.h
//  x
//
//  Created by wang on 14-11-3.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
@protocol Payway @end

@interface Payway : JSONModel

@property (strong, nonatomic) NSString* payway_id;
@property (strong, nonatomic) NSString* pay_tag;
@property (strong, nonatomic) NSString* pay_name;

@end
