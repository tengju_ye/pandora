//
//  GetOwnedPlanterListResponse.h
//  Pandora
//
//  Created by 小南宫的mac on 15/2/11.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "DefaultResponse.h"

@protocol GetOwnedPlanterPlanter @end

@interface GetOwnedPlanterPlanter : JSONModel
@property (nonatomic,strong) NSString* planter_id;
@property (nonatomic,strong) NSString<Optional>* planter_name;
@end

@interface GetOwnedPlanterListResponse : DefaultResponse
@property (nonatomic,strong) NSArray<GetOwnedPlanterPlanter>* data;
@end
