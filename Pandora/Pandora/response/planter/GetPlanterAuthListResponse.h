//
//  GetPlanterAuthListResponse.h
//  Pandora
//
//  Created by 小南宫的mac on 15/2/11.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "DefaultResponse.h"
/**
 ":[{"nickname":"\u53e4\u8463","mobile":"15258805454"},{"nickname":"\u53f0\u864e","mobile":"15258805454"},{"
 **/

@protocol GetPlanterAuthListData@end

@interface GetPlanterAuthListData : JSONModel
@property (nonatomic,strong) NSString<Optional>* nickname;
@property (nonatomic,strong) NSString<Optional>* mobile;
@end

@interface GetPlanterAuthListResponse : DefaultResponse
@property (nonatomic,strong) NSArray<Optional,GetPlanterAuthListData>* data;
@end
