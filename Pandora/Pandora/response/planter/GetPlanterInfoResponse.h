//
//  GetPlanterInfoResponse.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface GetPlanterInfoResponse: DefaultResponse
@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSString* planter_name;
@property (strong, nonatomic) NSString* box_state;
@property (strong, nonatomic) NSString* outside_temperature;
@property (strong, nonatomic) NSString* humidity;
@property (strong, nonatomic) NSString* illuminance;
@property (strong, nonatomic) NSString* alarm;
@property (strong, nonatomic) NSString* state;
@property (strong, nonatomic) NSString* seed_id;
@property (strong, nonatomic) NSString* planter_seed_id;
@property (strong, nonatomic) NSString* last_visit_time;
@property (strong, nonatomic) NSString* is_risk_mode;
@property (strong, nonatomic) NSString* toff;
@property (strong, nonatomic) NSString* ton;
@property (assign, nonatomic) BOOL online;
@end
