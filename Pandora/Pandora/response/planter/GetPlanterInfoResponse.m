//
//  GetPlanterInfoResponse.m
//  x
//
//  Created by wang on 14-11-2.
//
//

#import "GetPlanterInfoResponse.h"

@implementation GetPlanterInfoResponse

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.planter_name": @"planter_name",
                                 @"data.box_state": @"box_state",
                                 @"data.outside_temperature": @"outside_temperature",
                                 @"data.humidity": @"humidity",
                                 @"data.illuminance": @"illuminance",
                                 @"data.alarm": @"alarm",
                                 @"data.state": @"state",
                                 @"data.seed_id": @"seed_id",
                                 @"data.planter_seed_id": @"planter_seed_id",
                                 @"data.last_visit_time": @"last_visit_time",
                                 @"data.is_risk_mode": @"is_risk_mode",
                                 @"data.toff": @"toff",
                                 @"data.ton": @"ton",@"data.online": @"online"}];
}

@end
