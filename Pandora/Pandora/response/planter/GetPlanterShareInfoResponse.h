//
//  GetPlanterShareInfoResponse.h
//  Pandora
//
//  Created by 小南宫的mac on 15/2/11.
//  Copyright (c) 2015年 NG. All rights reserved.
//

#import "DefaultResponse.h"

@interface GetPlanterShareInfoResponse : DefaultResponse

@property(nonatomic,strong) NSString<Optional>* link;
@property(nonatomic,strong) NSString<Optional>* title;
@property(nonatomic,strong) NSString<Optional>* content;
@property(nonatomic,strong) NSString<Optional>* img;

@end
