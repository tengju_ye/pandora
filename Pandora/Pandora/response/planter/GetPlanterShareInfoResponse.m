//
//  GetPlanterShareInfoResponse.m
//  Pandora
//
//  Created by 小南宫的mac on 15/2/11.
//  Copyright (c) 2015年 NG. All rights reserved.
//

/**
 @property(nonatomic,strong) NSString<Optional>* link;
 @property(nonatomic,strong) NSString<Optional>* title;
 @property(nonatomic,strong) NSString<Optional>* content;
 @property(nonatomic,strong) NSString<Optional>* img;
 
 **/

#import "GetPlanterShareInfoResponse.h"

@implementation GetPlanterShareInfoResponse
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"data.link": @"link",
                                 @"data.title": @"title",@"data.content": @"content",@"data.img": @"img"}];
}

@end

