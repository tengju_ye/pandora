//
//  GetUserPlanterListResponse.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "UserPlanter.h"

@interface GetUserPlanterListResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSArray<UserPlanter>* data;

@end
