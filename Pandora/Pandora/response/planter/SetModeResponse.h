//
//  SetModeResponse.h
//  Pandora
//
//  Created by junchen on 12/6/14.
//  Copyright (c) 2014 NG. All rights reserved.
//

#import "DefaultResponse.h"

@interface SetModeResponse : DefaultResponse

@end
