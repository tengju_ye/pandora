//
//  SetPlanterNameResponse.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface SetPlanterNameResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;

@end
