//
//  UserPlanter.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@protocol UserPlanter@end
@interface UserPlanter : JSONModel

@property (strong, nonatomic) NSString* planter_id;
@property (strong, nonatomic) NSString* planter_name;

@end
