//
//  GetPlanterSeedInfoResponse.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface GetPlanterSeedInfoResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSString* seed_id;
@property (strong, nonatomic) NSString* state;

@end
