//
//  GetPlanterSeedInfoResponse.m
//  x
//
//  Created by wang on 14-11-2.
//
//

#import "GetPlanterSeedInfoResponse.h"

@implementation GetPlanterSeedInfoResponse

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.seed_id": @"seed_id",
                                 @"data.state": @"state"}];
}

@end
