//
//  GetPlanterSeedListResponse.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "PlanterSeed.h"

@interface GetPlanterSeedListResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSString* total_num;
@property (assign, nonatomic) int nextFirstRow;
@property (strong, nonatomic) NSArray<PlanterSeed>* planter_seed_list;

@end
