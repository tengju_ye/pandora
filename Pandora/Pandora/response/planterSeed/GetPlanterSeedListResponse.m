//
//  GetPlanterSeedListResponse.m
//  x
//
//  Created by wang on 14-11-2.
//
//

#import "GetPlanterSeedListResponse.h"

@implementation GetPlanterSeedListResponse

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.total_num": @"total_num",
                                 @"data.nextFirstRow": @"nextFirstRow",
                                 @"data.planter_seed_list": @"planter_seed_list"}];
}

@end
