//
//  GetPlanterSeedLogListResponse.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "PlanterSeedLog.h"

@interface GetPlanterSeedLogListResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSArray<PlanterSeedLog>* data;

@end
