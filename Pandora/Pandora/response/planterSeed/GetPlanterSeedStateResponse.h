//
//  GetPlanterSeedStateResponse.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface GetPlanterSeedStateResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;
@property (strong, nonatomic) NSString* outside_temperature;
@property (strong, nonatomic) NSString* humidity;
@property (strong, nonatomic) NSString* illuminance_limit;
@property (strong, nonatomic) NSString* state;

@end
