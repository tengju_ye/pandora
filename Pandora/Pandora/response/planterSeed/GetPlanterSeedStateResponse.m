//
//  GetPlanterSeedStateResponse.m
//  x
//
//  Created by wang on 14-11-2.
//
//

#import "GetPlanterSeedStateResponse.h"

@implementation GetPlanterSeedStateResponse

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.outside_temperature": @"outside_temnperature",
                                 @"data.humidity": @"humidity",
                                 @"data.illuminace_limit": @"illuminance_limit",
                                 @"data.state": @"state"}];
}

@end
