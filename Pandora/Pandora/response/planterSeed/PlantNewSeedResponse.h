//
//  PlantNewSeedResponse.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>

@interface PlantNewSeedResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;
@property (assign, nonatomic) int planter_seed_id;

@end
