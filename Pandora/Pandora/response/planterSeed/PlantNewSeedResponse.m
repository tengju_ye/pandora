//
//  PlantNewSeedResponse.m
//  x
//
//  Created by wang on 14-11-2.
//
//

#import "PlantNewSeedResponse.h"

@implementation PlantNewSeedResponse

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.planter_seed_id": @"planter_seed_id"}];
}

@end
