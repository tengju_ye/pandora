//
//  RecoverPlanterSeedStateResponse.h
//  Pandora
//":{"outside_temperature":"24.00","humidity":"64","illuminance_limit":"650"
//  Created by 小南宫的mac on 14/12/18.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "DefaultResponse.h"

@interface RecoverPlanterSeedStateResponse : DefaultResponse
@property (nonatomic,strong) NSString* outside_temperature;
@property (nonatomic,strong) NSString* humidity;
@property (nonatomic,strong) NSString* illuminance_limit;

@end
