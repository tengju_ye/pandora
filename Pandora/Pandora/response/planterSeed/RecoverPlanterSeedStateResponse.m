//
//  RecoverPlanterSeedStateResponse.m
//  Pandora
//
//  Created by 小南宫的mac on 14/12/18.
//  Copyright (c) 2014年 NG. All rights reserved.
//

#import "RecoverPlanterSeedStateResponse.h"

@implementation RecoverPlanterSeedStateResponse
+(JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"code": @"code",
                                 @"data.outside_temperature": @"outside_temperature",
                                 @"data.humidity": @"humidity", @"data.illuminance_limit": @"illuminance_limit"}];
}
@end
