//
//  SetPlanterSeedStateResponse.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>

@interface SetPlanterSeedStateResponse: DefaultResponse

@property (strong, nonatomic) NSString* code;

@end
