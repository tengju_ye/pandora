//
//  PlanterSeed.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@protocol PlanterSeed@end
@interface PlanterSeed : JSONModel

@property (strong, nonatomic) NSString* planter_seed_id;
@property (strong, nonatomic) NSString* item_id;
@property (strong, nonatomic) NSString* state;
@property (strong, nonatomic) NSString* plant_time;
@property (strong, nonatomic) NSString* item_name;
@property (strong, nonatomic) NSString* base_pic;

@end
