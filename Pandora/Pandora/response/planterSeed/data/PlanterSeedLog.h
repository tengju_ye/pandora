//
//  PlanterSeedLog.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@protocol PlanterSeedLog@end

@interface PlanterSeedLog : JSONModel

@property (strong, nonatomic) NSString* addtime;
@property (strong, nonatomic) NSString* state;

@end
