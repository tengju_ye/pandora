//
//  GetUserInfoResponse.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"
#import "UserInfo.h"

@interface GetUserInfoResponse: DefaultResponse
@property (strong, nonatomic) UserInfo* data;
@end
