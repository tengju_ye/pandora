//
//  LoginResponse.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "DefaultResponse.h"
@interface LoginResponse : DefaultResponse
@property(nonatomic,strong) NSString<Ignore>* phpsessid;//services中调用成功以后手动设置
@property(nonatomic,strong) NSString* password;

@end
