//
//  UserLoginResponse.m
//  x
//
//  Created by wang on 14-11-2.
//
//

#import "LoginResponse.h"
@implementation LoginResponse
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"data.password": @"password"
                                 }];
}

@end
