//
//  RegisterResponse.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface RegisterResponse: DefaultResponse
@property (strong, nonatomic) NSString* code;
@property (strong,nonatomic) NSString<Optional>* password;
@property (strong,nonatomic) NSString<Optional>* error_msg;
@property(nonatomic,strong) NSString<Ignore>* phpsessid;//services中调用成功以后手动设置
@end
