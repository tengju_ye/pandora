//
//  UserRegisterResponse.m
//  x
//
//  Created by wang on 14-11-2.
//
//

#import "RegisterResponse.h"

@implementation RegisterResponse
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc]
            initWithDictionary:@{
                                 @"data.password": @"password"
                                 }];
}

@end
