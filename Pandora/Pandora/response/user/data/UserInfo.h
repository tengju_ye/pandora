//
//  UserInfoData.h
//  x
//
//  Created by wang on 14-11-2.
//
//

#import <UIKit/UIKit.h>
#import "JSONModel.h"

@interface UserInfo : JSONModel

@property (strong, nonatomic) NSString<Optional>* nickname;
@property (strong, nonatomic) NSString<Optional>* left_money;
@property (strong, nonatomic) NSString<Optional>* user_rank_id;
@property (strong, nonatomic) NSString<Optional>* realname;
@property (strong, nonatomic) NSString<Optional>* mobile;
@property (strong, nonatomic) NSString<Optional>* current_planter_id;
@property (strong, nonatomic) NSString<Optional>* rank_name;
@property (strong, nonatomic) NSString<Optional>* current_planter_name;

@end
