#import <Foundation/Foundation.h>
#import "GetAccountListForm.h"
#import "GetAccountListResponse.h"

@interface AccountServices:NSObject
-(GetAccountListResponse*)getAccountList:(NSString*)type firstRow:(int)firstRow;
+(AccountServices*)instance;

@end