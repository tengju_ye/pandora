#import "AccountServices.h"


@implementation AccountServices

-(GetAccountListResponse*)getAccountList:(NSString*)type firstRow:(int)firstRow{
    GetAccountListForm* form = [[GetAccountListForm alloc] init];
    form.opt = type;
    form.firstRow = firstRow;
    NSString* baseurl = [[API shareInstance] BASE_API];
    __block GetAccountListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetAccountListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

static AccountServices* services = nil;
static dispatch_once_t pred = 0;
+(AccountServices *)instance{
    dispatch_once( &pred, ^{
        services = [[self alloc] init];
    });
    return services;
}

@end