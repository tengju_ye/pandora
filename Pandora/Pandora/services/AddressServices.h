#import <Foundation/Foundation.h>
#import "AddAddressForm.h"
#import "AddAddressResponse.h"
#import "CalExpressFeeForm.h"
#import "CalExpressFeeResponse.h"
#import "EditAddressForm.h"
#import "EditAddressResponse.h"
#import "GetAreaListForm.h"
#import "GetAreaListResponse.h"
#import "GetCityListForm.h"
#import "GetCityListResponse.h"
#import "GetAddressListResponse.h"
#import "GetAddressListForm.h"
#import "GetProvinceListResponse.h"
#import "GetProvinceListForm.h"

@interface AddressServices:NSObject
-(GetAddressListResponse*)getAddressList;


-(GetProvinceListResponse*)getProvinceList;
-(AddAddressResponse*)addAddress:(NSString*)realname mobile:(NSString*)mobile province_id:(int)province_id city_id:(int)city_id area_id:(int)area_id address:(NSString*)address ;
-(CalExpressFeeResponse*)calExpressFee:(int)city_id total_weight:(int)total_weight total_amount:(float)total_amount ;
-(EditAddressResponse*)editAddress:(NSString*)realname mobile:(NSString*)mobile province_id:(int)province_id city_id:(int)city_id area_id:(int)area_id address:(NSString*)address ofUserAddressID:(NSString*)idx ;
-(GetAreaListResponse*)getAreaList:(int)city_id ;
-(GetCityListResponse*)getCityList:(int)province_id ;
+(AddressServices*)instance;

@end