#import "AddressServices.h"


@implementation AddressServices

-(AddAddressResponse*)addAddress:(NSString*)realname mobile:(NSString*)mobile province_id:(int)province_id city_id:(int)city_id area_id:(int)area_id address:(NSString*)address {
    AddAddressForm* form = [[AddAddressForm alloc] init];
    form.realname = realname;
    form.mobile = mobile;
    form.province_id = province_id;
    form.city_id = city_id;
    form.area_id = area_id;
    form.address = address;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block AddAddressResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[AddAddressResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(CalExpressFeeResponse*)calExpressFee:(int)city_id total_weight:(int)total_weight total_amount:(float)total_amount {
    CalExpressFeeForm* form = [[CalExpressFeeForm alloc] init];
    form.city_id = city_id;
    form.total_weight = total_weight;
    form.total_amount = total_amount;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block CalExpressFeeResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[CalExpressFeeResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(EditAddressResponse*)editAddress:(NSString*)realname mobile:(NSString*)mobile province_id:(int)province_id city_id:(int)city_id area_id:(int)area_id address:(NSString*)address ofUserAddressID:(NSString*)idx {
    EditAddressForm* form = [[EditAddressForm alloc] init];
    form.address_id = idx;
    form.realname = realname;
    form.mobile = mobile;
    form.province_id = province_id;
    form.city_id = city_id;
    form.area_id = area_id;
    form.address = address;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block EditAddressResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[EditAddressResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetAreaListResponse*)getAreaList:(int)city_id {
    GetAreaListForm* form = [[GetAreaListForm alloc] init];
    form.city_id = city_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetAreaListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetAreaListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetCityListResponse*)getCityList:(int)province_id {
    GetCityListForm* form = [[GetCityListForm alloc] init];
    form.province_id = province_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetCityListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetCityListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}

static AddressServices* services = nil;
static dispatch_once_t pred = 0;
+(AddressServices *)instance{
    dispatch_once( &pred, ^{
        services = [[self alloc] init];
    });
    return services;
}

-(GetAddressListResponse*)getAddressList {
    GetAddressListForm* form = [[GetAddressListForm alloc]init];
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetAddressListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetAddressListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

-(GetProvinceListResponse*)getProvinceList {
    GetProvinceListForm* form = [[GetProvinceListForm alloc]init];
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetProvinceListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetProvinceListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

@end