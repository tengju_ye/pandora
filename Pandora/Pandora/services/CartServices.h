#import <Foundation/Foundation.h>
#import "AddCartForm.h"
#import "AddCartResponse.h"
#import "DeleteCartForm.h"
#import "DeleteCartResponse.h"
#import "GetCartListByIdsForm.h"
#import "GetCartListByIdsResponse.h"
#import "GetCartListResponse.h"
#import "GetCartListForm.h"
#import "GetCartTotalNumResponse.h"
#import "GetCartTotalNumForm.h"

@interface CartServices:NSObject
-(GetCartTotalNumResponse*)getCartTotalNum;
-(GetCartListResponse*)getCartList;
-(AddCartResponse*)addCart:(int)item_id item_str:(NSString*)item_str ;
-(DeleteCartResponse*)deleteCart:(int)shopping_cart_id ;
-(GetCartListByIdsResponse*)getCartListByIds:(NSString*)shopping_car_ids ;
+(CartServices*)instance;

@end