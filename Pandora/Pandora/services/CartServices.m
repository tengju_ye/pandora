#import "CartServices.h"


@implementation CartServices

-(AddCartResponse*)addCart:(int)item_id item_str:(NSString*)item_str {
    AddCartForm* form = [[AddCartForm alloc] init];
    form.item_id = item_id;
    form.item_str = item_str;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block AddCartResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[AddCartResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(DeleteCartResponse*)deleteCart:(int)shopping_cart_id {
    DeleteCartForm* form = [[DeleteCartForm alloc] init];
    form.shopping_cart_id = shopping_cart_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block DeleteCartResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[DeleteCartResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetCartListByIdsResponse*)getCartListByIds:(NSString*)shopping_car_ids {
    GetCartListByIdsForm* form = [[GetCartListByIdsForm alloc] init];
    form.shopping_car_ids = shopping_car_ids;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetCartListByIdsResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetCartListByIdsResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}

static CartServices* services = nil;
static dispatch_once_t pred = 0;
+(CartServices *)instance{
    dispatch_once( &pred, ^{
        services = [[self alloc] init];
    });
    return services;
}

-(GetCartTotalNumResponse*)getCartTotalNum {
    GetCartTotalNumForm* form = [[GetCartTotalNumForm alloc]init];
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetCartTotalNumResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetCartTotalNumResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}
-(GetCartListResponse*)getCartList {
    GetCartListForm* form = [[GetCartListForm alloc]init];
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetCartListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetCartListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

@end