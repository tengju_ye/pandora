#import <Foundation/Foundation.h>
#import "GetSortListByClassIdForm.h"
#import "GetSortListByClassIdResponse.h"
#import "GetClassListResponse.h"
#import "GetClassListForm.h"

@interface CategoryServices:NSObject
-(GetSortListByClassIdResponse*)getSortKListByClassId:(int)class_id ;
-(GetClassListResponse*)getClassList;
+(CategoryServices*)instance;
@end