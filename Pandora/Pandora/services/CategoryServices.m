#import "CategoryServices.h"


@implementation CategoryServices

-(GetSortListByClassIdResponse*)getSortKListByClassId:(int)class_id {
    GetSortListByClassIdForm* form = [[GetSortListByClassIdForm alloc] init];
    form.class_id = class_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetSortListByClassIdResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetSortListByClassIdResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}

static CategoryServices* services = nil;
static dispatch_once_t pred = 0;
+(CategoryServices *)instance{
    dispatch_once( &pred, ^{
        services = [[self alloc] init];
    });
    return services;
}

-(GetClassListResponse*)getClassList {
    GetClassListForm* form = [[GetClassListForm alloc]init];
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetClassListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetClassListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

@end