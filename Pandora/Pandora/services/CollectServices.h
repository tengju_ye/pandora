#import <Foundation/Foundation.h>
#import "AddCollectForm.h"
#import "AddCollectResponse.h"
#import "CancelCollectFrom.h"
#import "CancelCollectFrom.h"
#import "GetCollectListForm.h"
#import "GetCollectListResponse.h"

@interface CollectServices:NSObject
-(AddCollectResponse*)addCollect:(int)item_id ;
-(CancelCollectFrom*)cancelCollectFrom:(int)item_id ;
-(GetCollectListResponse*)getCollectList:(int)firstRow ;
+(CollectServices*)instance;

@end