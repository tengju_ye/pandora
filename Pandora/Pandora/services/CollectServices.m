#import "CollectServices.h"


@implementation CollectServices

-(AddCollectResponse*)addCollect:(int)item_id {
    AddCollectForm* form = [[AddCollectForm alloc] init];
    form.item_id = item_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block AddCollectResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[AddCollectResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(CancelCollectFrom*)cancelCollectFrom:(int)item_id {
    CancelCollectFrom* form = [[CancelCollectFrom alloc] init];
    form.item_id = item_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block CancelCollectFrom* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[CancelCollectFrom alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetCollectListResponse*)getCollectList:(int)firstRow {
    GetCollectListForm* form = [[GetCollectListForm alloc] init];
    form.firstRow = firstRow;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetCollectListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetCollectListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}

static CollectServices* services = nil;
static dispatch_once_t pred = 0;
+(CollectServices *)instance{
    dispatch_once( &pred, ^{
        services = [[self alloc] init];
    });
    return services;
}

@end