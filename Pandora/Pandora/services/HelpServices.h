#import <Foundation/Foundation.h>
#import "GetHelpInfoForm.h"
#import "GetHelpInfoResponse.h"
#import "GetAboutInfoResponse.h"
#import "GetAboutInfoForm.h"
#import "GetHelpListResponse.h"
#import "GetHelpListForm.h"

@interface HelpServices:NSObject
-(GetHelpInfoResponse*)getHelpInfo:(int)help_id;
-(GetAboutInfoResponse*)getAboutInfo;
-(GetHelpListResponse*)getHelpList;
+(HelpServices*)instance;

@end