#import "HelpServices.h"


@implementation HelpServices

-(GetHelpInfoResponse*)getHelpInfo:(int)help_id {
    GetHelpInfoForm* form = [[GetHelpInfoForm alloc] init];
    form.help_id = help_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetHelpInfoResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetHelpInfoResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}

static HelpServices* services = nil;
static dispatch_once_t pred = 0;
+(HelpServices *)instance{
    dispatch_once( &pred, ^{
        services = [[self alloc] init];
    });
    return services;
}

-(GetAboutInfoResponse*)getAboutInfo {
    GetAboutInfoForm* form = [[GetAboutInfoForm alloc]init];
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetAboutInfoResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetAboutInfoResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

-(GetHelpListResponse*)getHelpList {
    GetHelpListForm* form = [[GetHelpListForm alloc]init];
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetHelpListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetHelpListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

@end