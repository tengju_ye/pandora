#import <Foundation/Foundation.h>
#import "GetItemDetailsForm.h"
#import "GetItemDetailsResponse.h"
#import "GetItemInfoForm.h"
#import "GetItemInfoResponse.h"
#import "GetItemListForm.h"
#import "GetItemListResponse.h"
#import "GetItemListGroupBySortForm.h"
#import "GetItemListGroupBySortResponse.h"
#import "GetItemPhotoListForm.h"
#import "GetItemPhotoListResponse.h"
#import "GetItemSkuForm.h"
#import "GetItemSkuResponse.h"
#import "GetSeedStateListForm.h"
#import "GetSeedStateListResponse.h"

@interface ItemServices:NSObject
-(GetItemDetailsResponse*)getItemDetails:(int)item_id ;
-(GetItemInfoResponse*)getItemInfo:(int)item_id ;
-(GetItemListResponse*)getItemList:(int)firstRow class_id:(int)class_id sort_id:(int)sort_id class_tag:(NSString*)class_tag item_name:(NSString*)item_name ;


-(GetItemListGroupBySortResponse*)getItemListGroupBySort:(NSString *)class_id ;
-(GetItemPhotoListResponse*)getItemPhotoList:(int)item_id ;
-(GetItemSkuResponse*)getItemSku:(int)item_id ;
-(GetSeedStateListResponse*)getSeedStateList:(int)item_id ;
-(GetSeedStateListResponse*)getSeedStateList:(int)item_id is_diy:(BOOL)is_diy;


+(ItemServices*)instance;
@end