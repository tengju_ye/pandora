#import "ItemServices.h"


@implementation ItemServices
static ItemServices* services = nil;
static dispatch_once_t pred = 0;
+(ItemServices *)instance{
    dispatch_once( &pred, ^{
        services = [[self alloc] init];
    });
    return services;
}
-(GetItemDetailsResponse*)getItemDetails:(int)item_id {
    GetItemDetailsForm* form = [[GetItemDetailsForm alloc] init];
    form.item_id = item_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetItemDetailsResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetItemDetailsResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetItemInfoResponse*)getItemInfo:(int)item_id {
    GetItemInfoForm* form = [[GetItemInfoForm alloc] init];
    form.item_id = item_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetItemInfoResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetItemInfoResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetItemListResponse*)getItemList:(int)firstRow class_id:(int)class_id sort_id:(int)sort_id class_tag:(NSString*)class_tag item_name:(NSString*)item_name {
    GetItemListForm* form = [[GetItemListForm alloc] init];
    if (firstRow>0) {
        form.firstRow = @(firstRow);
    }
    if (class_id>0) {
        form.class_id = @(class_id);
    }
    
    if (sort_id>0) {
        form.sort_id = @(sort_id);
    }

    form.class_tag = class_tag;
    form.item_name = item_name;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetItemListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetItemListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetItemListGroupBySortResponse*)getItemListGroupBySort:(NSString *)class_id {
    GetItemListGroupBySortForm* form = [[GetItemListGroupBySortForm alloc] init];
    form.class_id = class_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetItemListGroupBySortResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetItemListGroupBySortResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetItemPhotoListResponse*)getItemPhotoList:(int)item_id {
    GetItemPhotoListForm* form = [[GetItemPhotoListForm alloc] init];
    form.item_id = item_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetItemPhotoListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetItemPhotoListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetItemSkuResponse*)getItemSku:(int)item_id {
    GetItemSkuForm* form = [[GetItemSkuForm alloc] init];
    form.item_id = item_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetItemSkuResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetItemSkuResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetSeedStateListResponse*)getSeedStateList:(int)item_id {
    GetSeedStateListForm* form = [[GetSeedStateListForm alloc] init];
    form.item_id = item_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetSeedStateListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetSeedStateListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}

-(GetSeedStateListResponse *)getSeedStateList:(int)item_id is_diy:(BOOL)is_diy{
    GetSeedStateListForm* form = [[GetSeedStateListForm alloc] init];
    form.item_id = item_id;
    form.is_diy = @(is_diy).stringValue;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetSeedStateListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetSeedStateListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}



@end