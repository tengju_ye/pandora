//
//  MallServices.h
//  Pandora
//
//  Created by wang on 14-11-22.
//  Copyright (c) 2014年 NG. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "GetHotKeywordsForm.h"
#import "GetHotKeywordsResponse.h"
#import "GetMallHomeImgListForm.h"
#import "GetMallHomeImgListResponse.h"

@interface MallServices : NSObject
-(GetHotKeywordsResponse*)getHotKeywords:(int)firstRow;
-(GetMallHomeImgListResponse*)getMallHomeImg;
+(MallServices*)instance;
@end
