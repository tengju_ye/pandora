#import "MallServices.h"


@implementation MallServices
static MallServices* services = nil;
static dispatch_once_t pred = 0;
+(MallServices *)instance{
    dispatch_once( &pred, ^{
        services = [[self alloc] init];
    });
    return services;
}

-(GetHotKeywordsResponse*)getHotKeywords:(int)firstRow {
    GetHotKeywordsForm* form = [[GetHotKeywordsForm alloc] init];
    form.firstRow = firstRow;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetHotKeywordsResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetHotKeywordsResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
    
}

-(GetMallHomeImgListResponse*)getMallHomeImg {
    GetMallHomeImgListForm* form = [[GetMallHomeImgListForm alloc]init];
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetMallHomeImgListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetMallHomeImgListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}


@end


