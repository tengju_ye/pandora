#import <Foundation/Foundation.h>
#import "AddOrderForm.h"
#import "AddOrderResponse.h"
#import "CancelOrderForm.h"
#import "CancelOrderResponse.h"
#import "ConfirmOrderForm.h"
#import "ConfirmOrderResponse.h"
#import "GetOrderInfoForm.h"
#import "GetOrderInfoResponse.h"
#import "GetOrderListForm.h"
#import "GetOrderListResponse.h"

typedef enum:NSUInteger {
    Order_Status_All = -1,
    Order_Status_Prepay=0,
    Order_Status_Payed = 1,
    Order_Status_Delivered = 3,
    Order_Status_Confirmed = 8
}   Order_Status;

@interface OrderServices:NSObject
-(AddOrderResponse*)addOrder:(int)user_address_id shopping_cart_ids:(NSString*)shopping_cart_ids number_str:(NSString*)number_str payway_id:(int)payway_id payway_name:(NSString*)payway_name user_remark:(NSString*)user_remark ;
-(CancelOrderResponse*)cancelOrder:(NSString*)order_id ;
-(ConfirmOrderResponse*)confirmOrder:(NSString*)order_id ;
-(GetOrderInfoResponse*)getOrderInfo:(NSString*)order_id ;
-(GetOrderListResponse*)getOrderList:(Order_Status)order_status firstRow:(int)firstRow ;
+(OrderServices*)instance;
@end