#import "OrderServices.h"


@implementation OrderServices
static OrderServices* services = nil;
static dispatch_once_t pred = 0;
+(OrderServices *)instance{
    dispatch_once( &pred, ^{
        services = [[self alloc] init];
    });
    return services;
}
-(AddOrderResponse*)addOrder:(int)user_address_id shopping_cart_ids:(NSString*)shopping_cart_ids number_str:(NSString*)number_str payway_id:(int)payway_id payway_name:(NSString*)payway_name user_remark:(NSString*)user_remark {
    AddOrderForm* form = [[AddOrderForm alloc] init];
    form.user_address_id = user_address_id;
    form.shopping_cart_ids = shopping_cart_ids;
    form.number_str = number_str;
    form.payway_id = payway_id;
    form.payway_name = form.payway_name;
    form.user_remark = user_remark;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block AddOrderResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result,     NSURLResponse *fullResponse) {
        NSError* error;
        response = [[AddOrderResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(CancelOrderResponse*)cancelOrder:(NSString*)order_id {
    CancelOrderForm* form = [[CancelOrderForm alloc] init];
    form.order_id = order_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block CancelOrderResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[CancelOrderResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(ConfirmOrderResponse*)confirmOrder:(NSString*)order_id {
    ConfirmOrderForm* form = [[ConfirmOrderForm alloc] init];
    form.order_id = order_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block ConfirmOrderResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[ConfirmOrderResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetOrderInfoResponse*)getOrderInfo:(NSString*)order_id {
    GetOrderInfoForm* form = [[GetOrderInfoForm alloc] init];
    form.order_id = order_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetOrderInfoResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetOrderInfoResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetOrderListResponse*)getOrderList:(Order_Status)order_status firstRow:(int)firstRow {
    GetOrderListForm* form = [[GetOrderListForm alloc] init];
    if (order_status!=Order_Status_All) {
        form.order_status = @(order_status);
    }else{
        form.order_status = nil;
    }
//    form.firstRow = firstRow;

    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetOrderListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetOrderListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}



@end