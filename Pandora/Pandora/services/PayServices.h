#import <Foundation/Foundation.h>
#import "GetPayInfoByOrderIdResponse_Alipay.h"
#import "GetPayInfoByOrderIdResponse_Unionpay.h"
#import "GetPayInfoByOrderIdForm.h"
#import "GetRechargeInfoForm.h"
#import "GetRechargeInfoResponse_Alipay.h"
#import "GetRechargeInfoResponse_Unionpay.h"
#import "GetPaywayListResponse.h"
#import "GetPaywayListForm.h"
#import "PayOrderByWalletForm.h"

@interface PayServices:NSObject
-(GetPaywayListResponse*)getPaywayList;
-(GetPayInfoByOrderIdResponse_Alipay*)getPayInfoByOrderId_Alipay:(NSString*)order_id ;
-(GetPayInfoByOrderIdResponse_Unionpay*)getPayInfoByOrderId_Unionpay:(NSString*)order_id;
-(GetRechargeInfoResponse_Alipay*)getRechargeInfo_Alipay:(float)amount payway_id:(int)payway_id ;
-(DefaultResponse*)payOrderByWallet:(NSString*)order_id;
-(GetRechargeInfoResponse_Unionpay*)getRechargeInfo_Unionpay:(float)amount payway_id:(int)payway_id ;
+(PayServices*)instance;

@end