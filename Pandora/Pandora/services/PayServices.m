#import "PayServices.h"


@implementation PayServices
static PayServices* services = nil;
static dispatch_once_t pred = 0;
+(PayServices *)instance{
    dispatch_once( &pred, ^{
        services = [[self alloc] init];
    });
    return services;
}
-(GetPayInfoByOrderIdResponse_Alipay*)getPayInfoByOrderId_Alipay:(NSString*)order_id {
    GetPayInfoByOrderIdForm* form = [[GetPayInfoByOrderIdForm alloc] init];
    form.order_id = order_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetPayInfoByOrderIdResponse_Alipay* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetPayInfoByOrderIdResponse_Alipay alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}

-(GetPayInfoByOrderIdResponse_Unionpay*)getPayInfoByOrderId_Unionpay:(NSString*)order_id {
    GetPayInfoByOrderIdForm* form = [[GetPayInfoByOrderIdForm alloc] init];
    form.order_id = order_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetPayInfoByOrderIdResponse_Unionpay* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetPayInfoByOrderIdResponse_Unionpay alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
    
}

-(GetRechargeInfoResponse_Alipay*)getRechargeInfo_Alipay:(float)amount payway_id:(int)payway_id {
    GetRechargeInfoForm* form = [[GetRechargeInfoForm alloc] init];
    form.amount = amount;
    form.payway_id = payway_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetRechargeInfoResponse_Alipay* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetRechargeInfoResponse_Alipay alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}

-(GetRechargeInfoResponse_Unionpay*)getRechargeInfo_Unionpay:(float)amount payway_id:(int)payway_id {
    GetRechargeInfoForm* form = [[GetRechargeInfoForm alloc] init];
    form.amount = amount;
    form.payway_id = payway_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetRechargeInfoResponse_Unionpay* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetRechargeInfoResponse_Unionpay alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
    
}
-(DefaultResponse *)payOrderByWallet:(NSString *)order_id{
    PayOrderByWalletForm* form = [[PayOrderByWalletForm alloc] init];
    form.order_id = order_id;
    NSString* baseurl = [[API shareInstance] BASE_API];
    __block DefaultResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[DefaultResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

-(GetPaywayListResponse*)getPaywayList {
    GetPaywayListForm* form = [[GetPaywayListForm alloc]init];
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetPaywayListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetPaywayListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}


@end