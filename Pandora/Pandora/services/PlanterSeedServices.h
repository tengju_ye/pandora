#import <Foundation/Foundation.h>
#import "EditPlanterSeedInfoForm.h"
#import "EditPlanterSeedInfoResponse.h"
#import "GetPlanterSeedInfoForm.h"
#import "GetPlanterSeedInfoResponse.h"
#import "GetPlanterSeedListForm.h"
#import "GetPlanterSeedListResponse.h"
#import "GetPlanterSeedLogListForm.h"
#import "GetPlanterSeedLogListResponse.h"
#import "GetPlanterSeedStateForm.h"
#import "GetPlanterSeedStateResponse.h"
#import "PlantNewSeedForm.h"
#import "PlantNewSeedResponse.h"
#import "SetPlanterSeedStateForm.h"
#import "SetPlanterSeedStateResponse.h"
#import "RecoverPlanterSeedStateResponse.h"

@interface PlanterSeedServices:NSObject
-(EditPlanterSeedInfoResponse*)editPlanterSeedInfo:(int)planter_seed_id state:(int)state seed_id:(int)seed_id seed_state_id:(int)seed_state_id t_num:(int)t_num h_num:(int)h_num i_num:(int)i_num ;
-(GetPlanterSeedInfoResponse*)getPlanterSeedInfo:(int)planter_seed_id ;
-(GetPlanterSeedListResponse*)getPlanterSeedList:(PlanterState)state firstRow:(int)firstRow ;
-(GetPlanterSeedLogListResponse*)getPlanterSeedLogList:(int)planter_seed_id ;
-(GetPlanterSeedStateResponse*)getPlanterSeedState:(int)planter_seed_id seed_state_id:(int)seed_state_id ;
-(PlantNewSeedResponse*)plantNewSeed:(int)seed_id seed_state_id:(int)seed_state_id state:(int)state ;
-(SetPlanterSeedStateResponse*)setPlanterSeedState:(int)planter_seed_id state:(int)state seed_id:(int)seed_id ;
-(RecoverPlanterSeedStateResponse*)recoverPlanterSeedState:(int)planter_seed_id state:(int)state seed_id:(int)seed_id;

+(PlanterSeedServices*)instance;

@end