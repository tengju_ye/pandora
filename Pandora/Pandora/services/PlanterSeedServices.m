#import "PlanterSeedServices.h"
#import "RecoverPlanterSeedStateForm.h"

@implementation PlanterSeedServices
static PlanterSeedServices* services = nil;
static dispatch_once_t pred = 0;
+(PlanterSeedServices *)instance{
    dispatch_once( &pred, ^{
        services = [[self alloc] init];
    });
    return services;
}
-(EditPlanterSeedInfoResponse*)editPlanterSeedInfo:(int)planter_seed_id state:(int)state seed_id:(int)seed_id seed_state_id:(int)seed_state_id t_num:(int)t_num h_num:(int)h_num i_num:(int)i_num {
    EditPlanterSeedInfoForm* form = [[EditPlanterSeedInfoForm alloc] init];
    form.planter_seed_id = planter_seed_id;
    form.state = state;
    form.seed_id = seed_id;
    form.seed_state_id = seed_state_id;
    form.t_num = t_num;
    form.h_num = h_num;
    form.i_num = i_num;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block EditPlanterSeedInfoResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result,     NSURLResponse *fullResponse) {
        NSError* error;
        response = [[EditPlanterSeedInfoResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetPlanterSeedInfoResponse*)getPlanterSeedInfo:(int)planter_seed_id {
    GetPlanterSeedInfoForm* form = [[GetPlanterSeedInfoForm alloc] init];
    form.planter_seed_id = planter_seed_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetPlanterSeedInfoResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetPlanterSeedInfoResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetPlanterSeedListResponse*)getPlanterSeedList:(PlanterState)state firstRow:(int)firstRow {
    GetPlanterSeedListForm* form = [[GetPlanterSeedListForm alloc] init];
    form.state = state;
    form.firstRow = firstRow;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetPlanterSeedListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetPlanterSeedListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetPlanterSeedLogListResponse*)getPlanterSeedLogList:(int)planter_seed_id {
    GetPlanterSeedLogListForm* form = [[GetPlanterSeedLogListForm alloc] init];
    form.planter_seed_id = planter_seed_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetPlanterSeedLogListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetPlanterSeedLogListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetPlanterSeedStateResponse*)getPlanterSeedState:(int)planter_seed_id seed_state_id:(int)seed_state_id {
    GetPlanterSeedStateForm* form = [[GetPlanterSeedStateForm alloc] init];
    form.planter_seed_id = planter_seed_id;
    form.seed_state_id = seed_state_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetPlanterSeedStateResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetPlanterSeedStateResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(PlantNewSeedResponse*)plantNewSeed:(int)seed_id seed_state_id:(int)seed_state_id state:(int)state {
    PlantNewSeedForm* form = [[PlantNewSeedForm alloc] init];
    form.seed_id = seed_id;
    form.seed_state_id = seed_state_id;
    form.state = state;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block PlantNewSeedResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[PlantNewSeedResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(SetPlanterSeedStateResponse*)setPlanterSeedState:(int)planter_seed_id state:(int)state seed_id:(int)seed_id {
    SetPlanterSeedStateForm* form = [[SetPlanterSeedStateForm alloc] init];
    form.planter_seed_id = planter_seed_id;
    form.state = state;
    form.seed_id = seed_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block SetPlanterSeedStateResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[SetPlanterSeedStateResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}

-(RecoverPlanterSeedStateResponse *)recoverPlanterSeedState:(int)planter_seed_id state:(int)state seed_id:(int)seed_id{
    RecoverPlanterSeedStateForm* form = [RecoverPlanterSeedStateForm new];
    form.planter_seed_id = @(planter_seed_id).stringValue;
    form.state = @(state).stringValue;
    form.seed_id = @(seed_id).stringValue;
    NSString* baseurl = [[API shareInstance] BASE_API];
    __block RecoverPlanterSeedStateResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[RecoverPlanterSeedStateResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}




@end