#import <Foundation/Foundation.h>
#import "BindNewPlanterForm.h"
#import "BindNewPlanterResponse.h"
#import "ChangeCurrentPlanterForm.h"
#import "ChangeCurrentPlanterResponse.h"
#import "GetUserPlanterListForm.h"
#import "GetUserPlanterListResponse.h"
#import "SetBoxStateForm.h"
#import "SetBoxStateResponse.h"
#import "SetPlanterNameForm.h"
#import "SetPlanterNameResponse.h"
#import "GetPlanterInfoResponse.h"
#import "GetPlanterInfoForm.h"
#import "SetModeForm.h"
#import "SetModeResponse.h"
#import "SetWaterTimeForm.h"
#import "SetWaterTimeResponse.h"
#import "GetOwnedPlanterListForm.h"
#import "GetOwnedPlanterListResponse.h"
#import "GetPlanterShareInfoForm.h"
#import "GetPlanterShareInfoResponse.h"
#import "GetPlanterAuthListForm.h"
#import "GetPlanterAuthListResponse.h"
@interface PlanterServices:NSObject
-(BindNewPlanterResponse*)bindNewPlanter:(NSString*)serial_num ;
-(ChangeCurrentPlanterResponse*)changeCurrentPlanter:(int)planter_id ;
-(GetUserPlanterListResponse*)getUserPlanterList;
-(SetBoxStateResponse*)setBoxState:(int)serial state:(int)state ;
-(SetPlanterNameResponse*)setPlanterName:(int)planter_id planter_name:(NSString*)planter_name ;
-(GetPlanterInfoResponse*)getPlanterInfo;
-(SetModeResponse*)setMode:(NSString*)mode;
-(SetWaterTimeResponse*)setWaterTime:(NSString*)ton toff:(NSString*)toff;
-(GetOwnedPlanterListResponse*)getOwnedPlanterList;
-(GetPlanterShareInfoResponse*)getPlanterShareInfo:(NSString*)planter_id;
-(GetPlanterAuthListResponse*)getPlanterAuthList:(NSString*)planter_id;
+(PlanterServices*)instance;
@end