#import "PlanterServices.h"


@implementation PlanterServices
static PlanterServices* services = nil;
static dispatch_once_t pred = 0;
+(PlanterServices *)instance{
    dispatch_once( &pred, ^{
        services = [[self alloc] init];
    });
    return services;
}
-(BindNewPlanterResponse*)bindNewPlanter:(NSString*)serial_num {
    BindNewPlanterForm* form = [[BindNewPlanterForm alloc] init];
    form.serial_num = serial_num;
    NSString* baseurl = [[API shareInstance] BASE_API];
    __block BindNewPlanterResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[BindNewPlanterResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(ChangeCurrentPlanterResponse*)changeCurrentPlanter:(int)planter_id {
    ChangeCurrentPlanterForm* form = [[ChangeCurrentPlanterForm alloc] init];
    form.planter_id = planter_id;
    NSString* baseurl = [[API shareInstance] BASE_API];
    __block ChangeCurrentPlanterResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[ChangeCurrentPlanterResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetUserPlanterListResponse*)getUserPlanterList{
    GetUserPlanterListForm* form = [[GetUserPlanterListForm alloc] init];
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetUserPlanterListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetUserPlanterListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(SetBoxStateResponse*)setBoxState:(int)serial state:(int)state {
    SetBoxStateForm* form = [[SetBoxStateForm alloc] init];
    form.serial = serial;
    form.state = state;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block SetBoxStateResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result,     NSURLResponse *fullResponse) {
        NSError* error;
        response = [[SetBoxStateResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(SetPlanterNameResponse*)setPlanterName:(int)planter_id planter_name:(NSString*)planter_name {
    SetPlanterNameForm* form = [[SetPlanterNameForm alloc] init];
    form.planter_id = planter_id;
    form.planter_name = planter_name;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block SetPlanterNameResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[SetPlanterNameResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}

-(GetPlanterInfoResponse*)getPlanterInfo {
    GetPlanterInfoForm* form = [[GetPlanterInfoForm alloc]init];
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetPlanterInfoResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetPlanterInfoResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

-(SetModeResponse*)setMode:(NSString*)mode {
    SetModeForm* form = [[SetModeForm alloc] init];
    form.mode = mode;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block SetModeResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[SetModeResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

-(SetWaterTimeResponse*)setWaterTime:(NSString*)ton toff:(NSString*)toff {
    SetWaterTimeForm* form = [[SetWaterTimeForm alloc] init];
    form.ton = ton;
    form.toff = toff;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block SetWaterTimeResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[SetWaterTimeResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

-(GetOwnedPlanterListResponse *)getOwnedPlanterList{
    GetOwnedPlanterListForm* form = [[GetOwnedPlanterListForm alloc] init];

    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetOwnedPlanterListResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetOwnedPlanterListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}

-(GetPlanterShareInfoResponse *)getPlanterShareInfo:(NSString *)planter_id{
    GetPlanterShareInfoForm* form = [[GetPlanterShareInfoForm alloc] init];
    form.planter_id = planter_id;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetPlanterShareInfoResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetPlanterShareInfoResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

-(GetPlanterAuthListResponse *)getPlanterAuthList:(NSString *)planter_id{
    GetPlanterAuthListForm* form = [[GetPlanterAuthListForm alloc] init];
    form.planter_id = planter_id;
    NSString* baseurl = [[API shareInstance] BASE_API];
    __block GetPlanterAuthListResponse* response = nil;
    
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetPlanterAuthListResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

@end