#import <Foundation/Foundation.h>
#import "CheckMobileRegisteredForm.h"
#import "CheckMobileRegisteredResponse.h"
#import "EditUserInfoForm.h"
#import "EditUserInfoResponse.h"
#import "GetUserInfoForm.h"
#import "GetUserInfoResponse.h"
#import "LoginForm.h"
#import "LoginResponse.h"
#import "RegisterForm.h"
#import "RegisterResponse.h"
#import "SendVerifyCodeForm.h"
#import "SendVerifyCodeResponse.h"
#import "FindPassForm.h"
#import "FindPassResponse.h"
#import "SetPasswordResponse.h"
#import "SetPasswordForm.h"
@interface UserServices:NSObject
-(CheckMobileRegisteredResponse*)checkMobileRegistered:(NSString*)mobile ;
-(EditUserInfoResponse*)editUserInfo:(NSString*)mobile realname:(NSString*)realname nickName:(NSString*)nickname ;
-(GetUserInfoResponse*)getUserInfo;
-(LoginResponse*)login:(NSString*)mobile password:(NSString*)password verify_code:(NSString*)verify_code;
-(RegisterResponse*)Register:(NSString*)mobile verify_code:(NSString*)verify_code ;
-(SendVerifyCodeResponse*)sendVerifyCode:(NSString*)mobile ;
-(FindPassResponse*) findPassword:(NSString*)newPass verifyCode:(NSString*)verifyCode;
-(SetPasswordResponse*) setPassword:(NSString*)newPass oldPass:(NSString*)oldPassword;
-(DefaultResponse*)logout;
+(UserServices*)instance;
@end