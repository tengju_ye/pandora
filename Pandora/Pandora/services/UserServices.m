#import "UserServices.h"


@implementation UserServices
static UserServices* services = nil;
static dispatch_once_t pred = 0;
+(UserServices *)instance{
    dispatch_once( &pred, ^{
        services = [[self alloc] init];
    });
    return services;
}

-(CheckMobileRegisteredResponse*)checkMobileRegistered:(NSString*)mobile {
    CheckMobileRegisteredForm* form = [[CheckMobileRegisteredForm alloc] init];
    form.mobile = mobile;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block CheckMobileRegisteredResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result,     NSURLResponse *fullResponse) {
        NSError* error;
        response = [[CheckMobileRegisteredResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(EditUserInfoResponse*)editUserInfo:(NSString*)mobile realname:(NSString*)realname nickName:(NSString*)nickname{
    EditUserInfoForm* form = [[EditUserInfoForm alloc] init];
    form.mobile = mobile;
    form.realname = realname;
    form.nickname = nickname;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block EditUserInfoResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[EditUserInfoResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(GetUserInfoResponse*)getUserInfo{
    GetUserInfoForm* form = [[GetUserInfoForm alloc] init];
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block GetUserInfoResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result,     NSURLResponse *fullResponse) {
        NSError* error;
        response = [[GetUserInfoResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}


-(LoginResponse *)login:(NSString *)mobile password:(NSString *)password verify_code:(NSString *)verify_code{
    LoginForm* form = [[LoginForm alloc] init];
    form.mobile = mobile;
    form.verify_code = verify_code;
    form.password = password;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block LoginResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[LoginResponse alloc] initWithDictionary:result error:&error];
        NSHTTPURLResponse* httpresponse = (NSHTTPURLResponse*)fullResponse;
        NSDictionary*headers = [httpresponse allHeaderFields];
        NSArray* cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:headers forURL:[NSURL URLWithString:@""]];
        for (NSHTTPCookie* cookie in cookies) {
            if ([cookie.name isEqualToString:@"PHPSESSID"]) {
                response.phpsessid = cookie.value;
            }
        }
        
    } sync:YES];
    return response;
}



-(FindPassResponse *)findPassword:(NSString *)newPass verifyCode:(NSString *)verifyCode{
    FindPassForm* form = [[FindPassForm alloc] init];
    __block FindPassResponse* response = nil;
    form.password = newPass;
    form.verify_code = verifyCode;
    [[APP instance] setSESSIONID:nil];
    [[HttpManager defaultManager] postSecretRequest:[[API shareInstance] BASE_API] model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[FindPassResponse alloc] initWithDictionary:result error:&error];
    } sync:YES ];
    
    
    return response;
}

-(SetPasswordResponse *)setPassword:(NSString *)newPass oldPass:(NSString *)oldPassword{
    SetPasswordForm* form = [[SetPasswordForm alloc] init];
    __block SetPasswordResponse* response = nil;
    form.password = newPass;
    form.old_password = oldPassword;
    [[HttpManager defaultManager] postSecretRequest:[[API shareInstance] BASE_API] model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[SetPasswordResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}

-(DefaultResponse *)logout{
    DefaultForm* form = [[DefaultForm alloc] init];
    form.api_name = @"plant.user.logout";
    __block DefaultResponse* response = nil;

    [[HttpManager defaultManager] postSecretRequest:[[API shareInstance] BASE_API] model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[DefaultResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;
}


-(RegisterResponse *)Register:(NSString *)mobile verify_code:(NSString *)verify_code{
    RegisterForm* form = [[RegisterForm alloc] init];
    form.mobile = mobile;
    form.verify_code = verify_code;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block RegisterResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[RegisterResponse alloc] initWithDictionary:result error:&error];
        NSHTTPURLResponse* httpresponse = (NSHTTPURLResponse*)fullResponse;
        NSDictionary*headers = [httpresponse allHeaderFields];
        [response setPhpsessid:headers[@"PHPSESSID"]];
    } sync:YES];
    return response;
}


-(SendVerifyCodeResponse*)sendVerifyCode:(NSString*)mobile {
    SendVerifyCodeForm* form = [[SendVerifyCodeForm alloc] init];
    form.mobile = mobile;
    NSString* baseurl = [[API shareInstance] BASE_API]
    ;__block SendVerifyCodeResponse* response = nil;
    [[HttpManager defaultManager] postSecretRequest:baseurl model:form.toDictionary complete:^(BOOL succeed, id result, NSURLResponse *fullResponse) {
        NSError* error;
        response = [[SendVerifyCodeResponse alloc] initWithDictionary:result error:&error];
    } sync:YES];
    return response;

}



@end