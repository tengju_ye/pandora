
#import <Foundation/Foundation.h>
#import "UIViewController+ShowRight.h"
@interface tools : NSObject
+(tools *)instance;

+ (int)checkWifiStatus;

+(void)hudNotification:(NSString *)text andView:(UIView *)view andDelay:(float)time withCustomView:(UIView*)cview;


+(NSString*)filterStr:(NSString*)string withReplacedString:(NSString*)replacedString withPlaceString:(NSString*)replaceString;
+(void)addHandTapRecognizer:(int)tapNum withView:(id)view Target:(id)target Selector:(SEL)selector;

//正则判断
+ (BOOL)isMobileNumber:(NSString *)mobileNum;
+ (BOOL)isValidEmail:(NSString*)email;
+ (BOOL) validateNumber: (NSString *) number;

//给textfield 添加 padding
+(void)addPaddingToTF:(UITextField*)tf left:(CGRect)lr right:(CGRect)rr;

//把NSNull 的 转成nil ， 如果不是NSNull 的 返回它本身
+(id)ifNullToNil:(id)objc;

//生成一个为type类型文件名（根据时间生产）
+(NSString*)generateFilenameByType:(NSString*)type;

+(BOOL)isSameDay:(NSDate*)date1 date2:(NSDate*)date2;

+(void)addRightBarItem:(UIViewController*)controller;

+(void)changeSearchCancelText:(UISearchBar*)searchBar;


@end
