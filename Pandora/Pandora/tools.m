
#import "tools.h"
#import <QuartzCore/QuartzCore.h>
#import "Reachability.h"
#import <AVFoundation/AVFoundation.h>
#import "MBProgressHUD.h"
@interface tools()
@property(nonatomic,assign) UIViewController* temController;
@end
static NSMutableSet* _JOBS;
@implementation tools
static tools* singleTon = nil;
static dispatch_once_t pred = 0;
+(tools *)instance{
    dispatch_once( &pred, ^{
        singleTon = [[self alloc] init];
    });
    return singleTon;
}

+ (NSString *)getFilePathInDocument:(NSString *)name
{
    return [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:name];
}

+ (BOOL)isFileExistInDocument:(NSString *)name
{
    NSFileManager *file_manager = [NSFileManager defaultManager];
    return [file_manager fileExistsAtPath:[tools getFilePathInDocument:name]];
}

+(void)hudNotification:(NSString *)text andView:(UIView *)view andDelay:(float)time withCustomView:(UIView*)cview
{
    MBProgressHUD* hud = [[MBProgressHUD alloc]initWithView:view];
    [hud setMode:MBProgressHUDModeText];
    [hud setLabelText:text];
    [hud setRemoveFromSuperViewOnHide:YES];
    [view    addSubview:hud];
    if (view) {
        [hud setMode:MBProgressHUDModeCustomView];
        hud.customView = cview;
    }
    [hud show:YES];
    [hud hide:YES afterDelay:time];
}



//检查wifi状态
+ (int)checkWifiStatus
{
    if([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus] != NotReachable)
    {
        return ReachableViaWiFi;
    }
    else if([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable){
        return ReachableViaWWAN;
    }
    else {
        return NotReachable;
    }
}



+(NSString*)filterStr:(NSString*)string withReplacedString:(NSString*)replacedString withPlaceString:(NSString*)replaceString{
    NSMutableString *dString=[NSMutableString stringWithString:string];
    [dString replaceOccurrencesOfString:replacedString withString:replaceString options:NSCaseInsensitiveSearch range:(NSRange){0,dString.length}];
    return dString;
}
+(void)addHandTapRecognizer:(int)tapNum withView:(id)view Target:(id)target Selector:(SEL)selector{
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:target action:selector];
    tapRecognizer.numberOfTapsRequired = tapNum;
    [view addGestureRecognizer:tapRecognizer];
}

+ (NSDate *)NSStringDateToNSDate:(NSString *)string
{
    NSDateFormatter *f = [NSDateFormatter new];
    [f setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * d = [f dateFromString:string];
    return d;
}
+ (NSString *)TimeIntervalToNSDate:(NSTimeInterval)string{
    NSDateFormatter *f = [NSDateFormatter new];
    [f setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * d = [f stringFromDate:[NSDate dateWithTimeIntervalSince1970:string]];
    return d;
}
+ (NSString *)getCurrentDateToString{
    NSDate *date=[NSDate date];
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateWithNewFormate=[f stringFromDate:date];
    return dateWithNewFormate;
}


+ (NSString *)cutCurrentDateToString:(NSString*)timeFormatStr{
    NSDate *date=[NSDate date];
    NSDateFormatter *f = [NSDateFormatter new];
    [f setDateFormat:timeFormatStr];
    NSString *dateWithNewFormate=[f stringFromDate:date];
    return dateWithNewFormate;
}
+(UIViewController*)viewControllerOfSuperView:(UIView *)subView
{
    for (UIView *next = [subView superview]; next; next = next.superview)
    {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]])
        {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}
+ (CGFloat)getWebViewHeight:(UIWebView *)webView
{
    UIScrollView *scrollView = (UIScrollView *)[[webView subviews] objectAtIndex:0];
    CGFloat webViewHeight = [scrollView contentSize].height;
    return webViewHeight;
}
+(NSString *)stringByStrippingHTML:(NSString*)str {
    NSRange r;
    NSString *s = str;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}
+(void)clearWebViewBg:(UIWebView*)webView{
    if ([[webView subviews] count] > 0) {
        for (UIView* shadowView in [[[webView subviews] objectAtIndex:0] subviews]) {
            [shadowView setHidden:YES];
        }
        [[[[[webView subviews] objectAtIndex:0] subviews] lastObject] setHidden:NO];
    }
    webView.backgroundColor = [UIColor whiteColor];
}
+(NSString*)replaceStr:(NSString*)regexPattern withReplacedStr:(NSMutableString*)str withPlaceStr:(NSString*)pstr{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexPattern options:0 error:nil];
    [regex replaceMatchesInString:str options:0 range:NSMakeRange(0, [str length]) withTemplate:pstr];
    return str;
}

#pragma mark - Regular expression
+(BOOL)isValidEmail:(NSString*)email{
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilterString ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (BOOL) validateNumber: (NSString *) number{
    NSString *reg = @"^[0-9]*$";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", reg];
    return [numberTest evaluateWithObject:number];
}

+ (BOOL)isMobileNumber:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}



+(void)addPaddingToTF:(UITextField *)tf left:(CGRect)lr right:(CGRect)rr
{
    UIView *paddingView = [[UIView alloc] initWithFrame:rr];
    [paddingView setBackgroundColor:[UIColor clearColor]];
    tf.rightView = paddingView;
    tf.rightViewMode = UITextFieldViewModeAlways;
    
    paddingView = [[UIView alloc] initWithFrame:lr];
    [tf setBackgroundColor:[UIColor clearColor]];
    tf.leftView = paddingView;
    tf.leftViewMode = UITextFieldViewModeAlways;
}

+(id)ifNullToNil:(id)objc
{
    if ([objc isKindOfClass:[NSNull class]]) {
        return nil;
    }
    
    return objc;
}



+(NSString*)generateFilenameByType:(NSString*)type
{
    //设置文件名
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddhhmmss"];
    NSDate *date = [NSDate date];
    NSString * s = [dateFormatter stringFromDate:date];
    int x = arc4random() % 10;
    s = [NSString stringWithFormat:@"%@%d",s,x];
    NSString* name = [s stringByAppendingPathExtension:type];
    return name;
}



+(BOOL)isSameDay:(NSDate *)date1 date2:(NSDate *)date2
{
    
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    
    [comp1 month] == [comp2 month] &&
    
    [comp1 year]  == [comp2 year];
    
    
}

+(void)addRightBarItem:(UIViewController *)controller{
    UIImage* image = [UIImage imageNamed:@"menu_icon"];
    UIButton* btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width/2, image.size.height/2)];
    [btn addTarget:controller action:@selector(showRight:) forControlEvents:UIControlEventTouchUpInside];
    [btn setImage:image forState:UIControlStateNormal];
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [[controller navigationItem] setRightBarButtonItem:item];
    
}


+(void)changeSearchCancelText:(UISearchBar *)searchBar{
    for (UIView *subView in searchBar.subviews){
        if([subView isKindOfClass:[UIButton class]]){
            [(UIButton*)subView setTitle:@"取消" forState:UIControlStateNormal];
        }
    }
}

@end
